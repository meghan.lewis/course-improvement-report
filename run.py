import sys
import argparse
#import datetime
import os
import re
import pandas as pd
import numpy as np
from pathlib import Path
# from bs4 import UnicodeDammit
import json
from urllib.parse import urlparse

# from pandas.io.formats.format import DataFrameFormatter

from dotenv import load_dotenv   #pip install python-dotenv



## Custom Modules
from utilities import utils
from utilities.custom_data import course_processor
from utilities.custom_data import canvas_helpers
from utilities import excel_report_writer as exl_writer

from utilities.database_access import OLD_database_access as database_access
from utilities.create_logger import initialize_logger

# pd.set_option('display.max_colwidth', None)
# pd.set_option('display.max_rows', 999)

ENV = 'dev'
DEFAULT_INCLUDE_NON_CONTENT_COURSES = False

TERM_FILTER_QUERY = 'term_filter'

COURSES_CTE_NAME = "CTE_courses"

QUERIES = {
    'courses': 'courses',
    'course_agg': 'course_aggs',
    'content': ['content_list', 'quiz_question_list'],
    'files': 'file_list'
}



numeric_types = ['int16', 'int32', 'int64', 'float16', 'float32', 'float64']

dict_sort_content = {
    'pages': 0,
    'assignments': 1,
    'discussions': 2,
    'quizzes': 3,
    'syllabus': 4,
    'modules': 5,
    'announcements': 6,
    'quizzes/questions': 7,
    'quizzes/answers': 8,
    'quizzes/question_comments': 9,
    'quizzes/answer_comments': 10
}



### --------------------------------------- TO DO --------------------------------------------- ###
    # YES         ---- 1. Check for WYSIWYG links in MODULE ITEMS
    # YES? check? ---- 2. Confirm checking external URLs in Module Items  (only external tools - what happened to external URLs?)
    # YES...      ---- 3. Further testing of youtube links
    # YES         ---- 4. Fill NA - zeroes
    # YES  Broken links to new report ??
    # 6. FOR CONTENT LINKED IN MULTIPLE PLACES -- (just for content report?)
    #       SORT BY "content_state_with_link" & "content_type_with_link" -- AND SELECT TOP MATCH ONLY ???
### --------------------------------------- ----- --------------------------------------------- ###



##  --------------------------------------------------------
#  FOR QUIZZES CONTENT ITEMS -- 
        # APPEND NUMBER OF QUESTIONS TO content_text 
        # SO CHECKING FOR "has_content" WILL BE ACCURATE, 
        # EVEN IF THERE IS NO DESCRIPTION FOR THE QUIZ
# MAYBE CHECK OTHER OCNTENT ITEMS ???
##  --------------------------------------------------------




# ----------- FUNCTIONS -----------

def script_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-e", "--env", help="run app in environment")
    parser.add_argument("-db", "--database_name", help="name of database to query")
    parser.add_argument("-t", "--term_id", help="SIS id of term to include data from")
    parser.add_argument("-i", "--include_no_content_courses", help="boolean - include courses without content items")
    return parser.parse_args()


def prep_column_format_for_report(df, report_column_map, custom_types_map):
    column_formats = {}
    column_renames = {}
    for k, v in report_column_map.items():
        if v.get('display_name') in custom_types_map.keys():
            column_formats[v.get('display_name')] = custom_types_map.get(v.get('display_name'))
        elif v.get('data_name') and v.get('data_name').find('total') > -1:
            column_formats[v.get('display_name')] = 'black_integers'
        elif v.get('data_name') and v.get('data_name').find('percent') > -1:
            column_formats[v.get('display_name')] = 'percent'
        # elif v.get('data_name') == '_IGNORE_':
        #     column_formats[v.get('display_name')] = 'gray_background'
        else:
            column_formats[v.get('display_name')] = 'general'
        
        if v.get('data_name') and v.get('data_name') in df.columns:
            column_renames[v.get('data_name')] = v.get('display_name')
        else:
            df[v.get('display_name')] = None

    df.rename(columns=column_renames, inplace=True)
    return df, column_formats




def get_sql_query_from_file(dir, name):
    sql_file_path = os.path.join(dir, f"{name}.sql")
    if not utils.check_file_exists(sql_file_path):
        logger.info(f"SQL file does not exist: {sql_file_path}")
        return None
    return utils.get_file_text(sql_file_path)


def get_db_data(conn, query, parameters={}):
    df = pd.DataFrame()
    #logger.info(f"Executing Database Query")
    try:
        df = pd.read_sql_query(query, db_conn, params=parameters)
        logger.info(f" {len(df)} data rows retrieved")
    except Exception:
        logger.exception("Error querying database", exc_info=True)
    return df


def get_terms_list(db_conn, sql_dir, query_name):
    logger.info("Get terms data")
    query_name = TERM_FILTER_QUERY
    logger.info(f"Get data for: {query_name}")
    query_text = get_sql_query_from_file(sql_dir, query_name)
    query_params = {'days_from_start': -84, # Term start date is within next 12 weeks (or in the past)
                    'days_from_end': -28}  # Term end date is not more than 4 weeks from now
    terms_df = get_db_data(db_conn, query_text, parameters=query_params)
    logger.info(f"{len(terms_df)} terms returned")
    return list(terms_df['sis_source_id'].unique())




def extract_text(text, regex_list=[]):
    matches = []
    if len(regex_list) == 0:
        print("Must provide list of regular expression(s)")
    for reg in regex_list:
        matches += re.findall(reg, text, re.IGNORECASE)
    return matches


# Files Calculations
def parse_file_extension(file_name):
    regex_extension = r'(\.\w{1,5})$'
    matched = re.search(regex_extension, file_name.lower())
    return matched.group(1) if matched else None

def parse_file_category(file_type):
    category_regexes = {
        'pdf': 'pdf',
        'image': 'image/',
        'video': 'video/',
        'flash': 'flash'
    }
    for cat, reg in category_regexes.items():
        if re.search(reg, file_type.lower()):
            return cat
    return None


def check_url_http_syntax(url):
    pattern_good_http = 'https?://[^\s]'
    pattern_malformed_http = 'https?\:?/*\s*'

    REG_http_multi_match = '(' + pattern_malformed_http + '){2,}'
    REG_http_extract = '((?:' + pattern_malformed_http + ')+)'

    has_error = False
    # Starts with 1+ http
    if re.match(REG_http_extract, url):
        # Starts with multiple http(s) or malformed http(s)
        if re.match(REG_http_multi_match, url) or not re.match(pattern_good_http, url):
            has_error = True
        if 'https' in re.match(REG_http_extract, url).group(1):
            prefix = 'https://'
        else:
            prefix = 'http://'
        return has_error, re.sub(REG_http_extract, prefix, url)
    # Starts with // or contains web address (i.e. NOT internal path)
    elif re.match('//', url) or re.search('(www\.|\.(com|org|edu|gov))', url):
        return has_error, 'http://' + url.lstrip('/')
    else:
        return has_error, url

def parse_tool_redirect_link(link):
    # starts with '/courses/' but contains 'http' - because it has a redirect from external tool
    if re.match('/?courses/\d+/external_tools', link) and re.search('http', link):
        redirect_link = re.search('(http.+)', link).group(1)
        return redirect_link
    else:
        return link

def process_text_url(url):
    BROKEN_LINK_REGEX_LIST = [ 'LINK\.PLACEHOLDER',
                               '(?:(?:\$|%24)[A-Z_]+(?:\$|%24))',
                               '.*\$CANVAS_OBJECT_REFERENCE\$']
    VALID_SCHEMES = ['http', 'https', 'ftp']
    internal_first_paths = [
        'appointment_groups', 'assessment_questions',  'assignments', 'calendar', 'conversations', 
        'courses', 'enroll', 'eportfolios', 'equation_images', 'files', 'groups',  'images',
        'login', 'media_objects', 'media_objects_iframe',  'pages', 'profile', 'search', 'users',

        'modules_items', 'external_tools', 'quizzes', 'grades', 'gradebook', 
        'collaborations', 'conferences', 'discussion_topics',
    ]
    internal_paths_regex = '^((/?api/v1)|(#))'

    if not url:
        return pd.Series([None, True, None, None])
    elif any([re.match(REG_broken, url) for REG_broken in BROKEN_LINK_REGEX_LIST]):
        return pd.Series([True, True, None, None])

    
    process_url = url.encode('ascii', errors='ignore').decode('ascii').lower()
    has_error, process_url = check_url_http_syntax(process_url)

    is_internal = None
    parts = urlparse(process_url, allow_fragments=False)
    parsed_path = parts.path.strip().strip('/')
    if re.search('#', parsed_path) and not re.match('#', parsed_path):
        parsed_groups = re.search('([^#]+)(#.*)', parsed_path).groups()
        parsed_path = parsed_groups[0]
        parsed_fragment = parsed_groups[1]

    path_split = [x for x in parsed_path.split('/') if len(x)>0] if parsed_path else []
    if parts.scheme and parts.scheme not in VALID_SCHEMES:
        has_error = True
    if not parts.netloc and len(path_split) == 0:
        #link_type = None
        has_error = True
    if parts.netloc and parts.netloc != 'usu.instructure.com':
        is_internal = False
    elif not parts.netloc and len(path_split) > 0:
        is_internal = True
        if re.search('(www\.|\.(com|org|edu|gov))', parsed_path):
            is_internal = False
            has_error = True
        elif any([True if x.find(' ')>-1 else False for x in path_split]) and path_split[0] not in ['courses','files']:
            ## if url link contains a space (and is not a course file)
            has_error = True
        elif not any([re.match(internal_paths_regex, parsed_path), path_split[0] in internal_first_paths]):
            has_error = True        

    return pd.Series([is_internal, 
                        has_error, 
                        (parts.netloc.strip() or None), 
                        (parts.path.strip() or parts.fragment.strip() or None) 
                    ])
                        # parts.params.strip() or None, 
                        # parts.query.strip() or None, 
                        # parsed_fragment,
                        # parts.scheme.strip() or None, 




### CUSTOM DATA PROCESSORS

def data_processor_parse_url_from_content(input_content_df):
    """Parse URLs from content text/html
    input_content_df = all Canvas content items"""

    logger.info(f"Parse links from content")
    ## COPY ALL CONTENT THAT HAS "content_text"
    filt_content_df = input_content_df.loc[input_content_df['content_text'].notnull(), 
                                    ['course_id', 'content_type', 'content_id', 'content_text', 'content_state']].copy(deep=True)

    ## PARSE ALL URLS FROM "content_text"
    filt_content_df['contained_links'] = filt_content_df['content_text'].apply(lambda x: canvas_helpers.parse_links_from_content(x, include_raw_text_links=False))
    # content_text_df['contained_links'].head()

    ## PIVOT URLS PARSED FROM "content_text" -- ONE URL PER ROW
    links_content_df = pd.DataFrame(filt_content_df.contained_links.tolist(),
                                    index=[filt_content_df.course_id, filt_content_df.content_type, 
                                            filt_content_df.content_id, filt_content_df.content_state,]
                                ).stack()
    # Reset index and set column names
    links_content_df = links_content_df.reset_index()[[0,'course_id', 'content_type','content_id','content_state',]] 
    links_content_df.columns = ['url_from_text', 'course_id', 'content_type', 'content_id', 'content_state',]

    # Remove reows without any parsed URLs
    links_content_df = links_content_df.loc[links_content_df['url_from_text'].str.len()>0].drop_duplicates().copy(deep=True)


    logger.info(f"Clean/split URLs in content")
    links_content_df[['is_internal_url', 'has_url_error', 'netloc', 'path']] = links_content_df['url_from_text'].apply(lambda x: process_text_url(x))

    return links_content_df


def data_processor_find_content_linked_in_content(input_content_df, input_links_content_df):
    """Join Canvas content items data with links to that item that exist in other course content
        input_content_df = all Canvas content items
        input_links_content_df = Canvas content items and URLs that were parsed from the HTML/text of the item
    Returns ALL content -- and designates whether a link to content was found in other course content
    """
    
    ##  --------------------------------------------------------
    #  FOR QUIZZES CONTENT ITEMS -- 
    # APPEND NUMBER OF QUESTIONS TO content_text 
    # SO CHECKING FOR "has_content" WILL BE ACCURATE, 
    # EVEN IF THERE IS NO DESCRIPTION FOR THE QUIZ
    ##  --------------------------------------------------------
    logger.info(f"Match links to WYSIWYG content")

    ### CHECK FOR LINKS TO CANVAS WYSIWYG ITEMS WITHIN CONTENT

    ######################################################################################################################################
    ######################################################################################################################################
    ######################################################################################################################################
    ######################################################################################################################################
    ######################################################################################################################################
    ## Temp modified WYSIWYG_CONTENT_TYPES to also include quizzes/questions & quizzes/answers ??????????????????????????????????
    ######################################################################################################################################
    ######################################################################################################################################
    ######################################################################################################################################
    ######################################################################################################################################
    #WYSIWYG_CONTENT_TYPES = [t for t in input_content_df.content_type.unique() if t.find('/')==-1 and t != 'modules']
    WYSIWYG_CONTENT_TYPES = [t for t in input_content_df.content_type.unique() if t != 'modules']


    REGEX_CONTENT_TYPES = '/?(?:' + '|'.join(WYSIWYG_CONTENT_TYPES) + ')/[^/#&\?]+' 
    REGEX_EXTRACT = '(/(?:' + '|'.join(WYSIWYG_CONTENT_TYPES) + ')/(?:syllabus|[\d]+|(?<=pages/)[^/#&\?]+))(?:[/#&\?]|$)' 
    #REGEX_EXTRACT = '((?:/?courses/\d+)?' + REGEX_CONTENT_TYPES + ')'

    #non_wysiwyg_content_df = content_df.loc[~content_df['content_type'].isin(WYSIWYG_CONTENT_TYPES)].copy(deep=True)

    ## DataFrame with all Canvas WYSIWYG content items
    wysiwyg_content_df = input_content_df.loc[input_content_df['content_type'].isin(WYSIWYG_CONTENT_TYPES)].copy(deep=True)

    # DataFrame with all LINKS parsed from content, that could be an internal Canvas WYSIWYG item
    wysiwyg_links_df = input_links_content_df.loc[(input_links_content_df['is_internal_url']==True) 
                                                    & (input_links_content_df["path"].str.contains(REGEX_CONTENT_TYPES, regex=True))
                                                ].copy(deep=True).reset_index(drop=True)


    # Extract URL found in content & split it into path parts 
    split_wysiwyg_links_df = pd.DataFrame(wysiwyg_links_df['path'].apply(lambda x: [] if not re.search(REGEX_EXTRACT, x) else \
                                                                        re.search(REGEX_EXTRACT, x).group(1).strip('/').split('/')
                                                            ).tolist(), 
                                        index=wysiwyg_links_df.index)
    split_wysiwyg_links_df.rename(columns={c:f"link_path_{c}" for c in split_wysiwyg_links_df.columns}, inplace=True)


    ## Re-join split URLs that were parsed from 'wysiwyg_links_df' back to original 'wysiwyg_links_df'
    wysiwyg_links_df = pd.merge(wysiwyg_links_df, 
                                    split_wysiwyg_links_df, 
                                    left_index=True, 
                                    right_index=True)
    wysiwyg_links_df.rename(columns={"content_type": "content_type_with_link",
                                        "content_id":	"content_id_with_link",
                                        "content_state": "content_state_with_link"}, inplace=True)
    #wysiwyg_links_df.info()


    ## CHECK IF ANY LINKS DIDN'T PARSE FROM REGEX
    parse_error = wysiwyg_links_df.loc[wysiwyg_links_df['link_path_0'].isnull(), ['url_from_text']]
    if len(parse_error) > 0:
        logger.info(f"Error processing URLs/links in WYSIWYG content -- {len(parse_error)} urls could not be parsed")
        print(parse_error)
    
    ## combine content data with parsed links data, to determin which content items have links referenced in other content
    merged_wysiwyg_df = pd.merge(wysiwyg_content_df,
                                wysiwyg_links_df,
                                how='left',
                                left_on=['course_id','content_type','content_id'],
                                right_on=['course_id','link_path_0','link_path_1']
            )

    merged_wysiwyg_df['content_is_linked'] = merged_wysiwyg_df['link_path_0'].apply(lambda x: 1 if pd.notnull(x) else 0)
    #merged_wysiwyg_df.info()
    ### ------------------------------ TEST NEW -------------------------------

    merged_wysiwyg_df['course_canvas_id'] = merged_wysiwyg_df['course_id'].apply(lambda x: None if pd.isnull(x) else str(canvas_helpers.convert_canvas_id(x, output_id_type='local')) )

    merged_wysiwyg_df['joinable_content_with_link'] = merged_wysiwyg_df['content_type_with_link'].apply(lambda x: None if pd.isnull(x) else x[:x.find('/')] if x.find('/')>-1 else x)

    merged_wysiwyg_df['content_url_with_link'] = merged_wysiwyg_df.apply(lambda x: None if x['content_is_linked']==0 else \
                                                                            utils.build_canvas_url(
                                                                                context_type= 'courses',
                                                                                context_id= x['course_canvas_id'], 
                                                                                content_type=x['joinable_content_with_link'], 
                                                                                content_id=x['content_id_with_link'],
                                                                                canvas_domain=None), axis=1)

    linked_wysiwyg = merged_wysiwyg_df.loc[merged_wysiwyg_df['content_is_linked']==1].copy(deep=True)
    linked_wysiwyg_summary = linked_wysiwyg.groupby(['course_id', 'content_type', 'content_id',], as_index=False).agg(
                                                                                        content_is_linked=('content_is_linked', max),
                                                                                        content_types_with_links=('content_type_with_link','unique'),
                                                                                        content_urls_with_links=('content_url_with_link','unique'),
                                                                                    )
    linked_wysiwyg_summary['in_modules'] = linked_wysiwyg_summary['content_types_with_links'].apply(lambda x: 'modules' in x)
    linked_wysiwyg_summary['total_links_to_content'] = linked_wysiwyg_summary['content_urls_with_links'].apply(lambda x: 0 if not isinstance(x, (list, np.ndarray)) else len(x))

    all_wysiwyg_df = pd.merge(wysiwyg_content_df,
                                linked_wysiwyg_summary,
                                how='left',
                                left_on=['course_id','content_type','content_id'],
                                right_on=['course_id','content_type','content_id'])

    all_wysiwyg_df['content_is_linked'] = all_wysiwyg_df['content_is_linked'].fillna(0).astype(int)
    all_wysiwyg_df['in_modules'] = all_wysiwyg_df['in_modules'].fillna(False)
    all_wysiwyg_df['has_content'] = all_wysiwyg_df['content_text'].apply(lambda x: pd.notnull(x) and len(x.strip())>0)
    all_wysiwyg_df['total_links_to_content'] = all_wysiwyg_df['total_links_to_content'].fillna(0).astype(int)

    ### ------------------------- TO DO  ??? -------------------------
    # FOR CONTENT LINKED IN MULTIPLE PLACES 
    # -- SORT BY "content_state_with_link" & "content_type_with_link"
    # -- SELECT TOP MATCH ONLY
    ### --------------------------------------------------------------

    # Only keep one reference link per content item
    # Group per content item and keep only one rows (if linked to first)
    # GROUP_COLS = list(wysiwyg_content_df.columns)
    # AGG_COL = 'content_is_linked'

    # merged_wysiwyg_df = merged_wysiwyg_df[GROUP_COLS + [AGG_COL]].fillna('-').groupby(GROUP_COLS, as_index=False)[AGG_COL].max()
    ###---------- ENDING HERE : NEW PROCESSING FIND ALL LINKS IN CONTENT -- WORK IN PROGRESS ----------------
    #merged_wysiwyg_df.info()

    return all_wysiwyg_df


def data_processor_find_content_with_broken_links(input_content_df, input_links_content_df):
    """Find Canvas content items with broken URLs/links
        input_content_df = all Canvas content items
        input_links_content_df = Canvas content items and URLs that were parsed from the HTML/text of the item
    Returns ONLY content with broken links"""

    logger.info(f"Find Broken Links in Content")
    broken_links_content_df = input_links_content_df[(input_links_content_df['has_url_error']==True)].copy(deep=True)

    ## Summarize broken links PER content item
    broken_links_df = broken_links_content_df.groupby(['course_id', 'content_type', 
                                                    'content_id', 'content_state'], 
                                            as_index=False).agg(link_list=('url_from_text',list))

    broken_links_df['total_broken_links'] = broken_links_df['link_list'].apply(lambda x: len(x or []))
    ## Parse/fix 'content_type' field, to allow joining to content items info
    broken_links_df['joinable_content'] = broken_links_df['content_type'].apply(lambda x: x[:x.find('/')] if x.find('/')>-1 else x)

    ## Merge broken link(s) data with original content information
    broken_links_df = pd.merge(broken_links_df, 
                                input_content_df[['course_id','content_type','content_id','content_name', 'content_created_at', 'content_updated_at']].drop_duplicates(), 
                                left_on=['course_id','joinable_content','content_id'],
                                right_on=['course_id','content_type','content_id'],
                                suffixes=['','_y'])
    return broken_links_df


def data_processor_find_content_with_kaltura_links(input_content_df, input_links_content_df):
    """Find Canvas content items with links to Kaltura
        input_content_df = all Canvas content items
        input_links_content_df = Canvas content items and URLs that were parsed from the HTML/text of the item
    Returns ONLY content with Kaltura links"""

    logger.info(f"Parse Links to Kaltura from Content")

    kaltura_links_content_df = input_links_content_df.loc[(input_links_content_df['url_from_text'].str.lower().str.contains('kaltura|mymedia.usu.edu'))].copy(deep=True)
    kaltura_links_content_df['url_from_text'] = kaltura_links_content_df['url_from_text'].apply(lambda x: parse_tool_redirect_link(x))


    ## Summarize kaltura links PER content item
    kaltura_links_df = kaltura_links_content_df.groupby(['course_id', 'content_type', 
                                                        'content_id', 'content_state'], 
                                                as_index=False).agg(link_list=('url_from_text',list))
    kaltura_links_df['total_kaltura_links'] = kaltura_links_df['link_list'].apply(lambda x: len(x or []))

    kaltura_links_df = pd.json_normalize(json.loads(kaltura_links_df.apply(lambda x: {**x.to_dict(), 
                                                                                    **parse_kaltura_ids_from_url_list(x['link_list'])}, 
                                                                            axis=1).to_json(orient="records")) )

    ## Parse/fix 'content_type' field, to allow joining to content items info
    kaltura_links_df['joinable_content'] = kaltura_links_df['content_type'].apply(lambda x: x[:x.find('/')] if x.find('/')>-1 else x)

    ## Merge broken link(s) data with original content information
    kaltura_links_df = pd.merge(kaltura_links_df, 
                                input_content_df[['course_id','content_type','content_id','content_name', 'content_created_at', 'content_updated_at']].drop_duplicates(), 
                                left_on=['course_id','joinable_content','content_id'],
                                right_on=['course_id','content_type','content_id'],
                                suffixes=['','_y'])
    return kaltura_links_df


def data_processor_find_content_with_youtube_links(input_content_df, input_links_content_df):
    """Find Canvas content items with links to YouTube
        input_content_df = all Canvas content items
        input_links_content_df = Canvas content items and URLs that were parsed from the HTML/text of the item
    Returns ONLY content with YouTube links"""
    

    logger.info(f"Parse Links to YouTube from Content")
    MATCH_REGEX_YOUTUBE = r'youtu\.?be'
    EXTRACT_REGEX_LIST_YOUTUBE = [r'(.*youtu\.?be.*)']
    
    youtube_links_content_df = input_links_content_df.loc[(input_links_content_df['is_internal_url']==False) 
                                                    & (input_links_content_df['netloc'].notnull())
                                                    & (input_links_content_df['url_from_text'].str.lower().str.contains(MATCH_REGEX_YOUTUBE))
                                                    ].copy(deep=True)


    # youtube_content_df['total_youtube_links'] = youtube_content_df['netloc'].apply(lambda x: len(extract_text(x, YOUTUBE_REGEX_LIST)))
    # youtube_content_df = youtube_content_df.loc[youtube_content_df['total_youtube_links']>0].copy(deep=True)
    # youtube_content_df.shape


    ## Summarize kaltura links PER content item
    youtube_links_df = youtube_links_content_df.groupby(['course_id', 'content_type', 
                                                        'content_id', 'content_state'], 
                                                as_index=False).agg(link_list=('url_from_text',list))
    youtube_links_df['total_youtube_links'] = youtube_links_df['link_list'].apply(lambda x: len(x or []))


    ## Parse/fix 'content_type' field, to allow joining to content items info
    youtube_links_df['joinable_content'] = youtube_links_df['content_type'].apply(lambda x: x[:x.find('/')] if x.find('/')>-1 else x)

    ## Merge broken link(s) data with original content information
    youtube_links_df = pd.merge(youtube_links_df, 
                                input_content_df[['course_id','content_type','content_id','content_name', 'content_created_at', 'content_updated_at']].drop_duplicates(), 
                                left_on=['course_id','joinable_content','content_id'],
                                right_on=['course_id','content_type','content_id'],
                                suffixes=['','_y'])
    return youtube_links_df


def data_processor_find_files_linked_in_content(input_files_df, input_links_content_df):
    """Find Canvas content items with links to Canvas files
        input_content_df = all Canvas content items
        input_links_content_df = Canvas content items and URLs that were parsed from the HTML/text of the item
        input_files_df = all Canvas Files items
    Returns ALL files -- and designates whether a link to content was found in other course content
    """
    # PARSING OF FILES LINKED IN CONTENT
    # POSSIBLY REPLACE THIS WITH FULL PARSING OF LINKS IN CONTENT (IN-PROGRESS "WIP - SUB STEP 1" AT BOTTOM)

    #file_links_content_df['files'] = file_links_content_df['path'].apply(lambda x: extract_text(x, FILE_REGEX_LIST) if pd.notnull(x) else [])

    logger.info(f"Parse Links to Files from Content")
    FILE_REGEX = 'files/(\d+)(?:[/#&\?]|$)'
    file_links_content_df = input_links_content_df.loc[(input_links_content_df['path'].notnull()) 
                                                        & (input_links_content_df['path'].str.contains('files'))].copy(deep=True)
    # Parse file id from link/URL found in content
    file_links_content_df['linked_file_id'] = file_links_content_df['path'].apply(lambda x: re.search(FILE_REGEX, x).group(1) if pd.notnull(x) and re.search(FILE_REGEX, x) else None)

    f_columns = input_files_df.columns.tolist()
    f_links_columns = [c for c in file_links_content_df.columns if c not in ['course_id','linked_file_id']]

    # Merge files with links in content
    merged_files_links_df = pd.merge(input_files_df, 
                                    file_links_content_df, 
                                    how='left', 
                                    left_on=['file_id'],
                                    right_on=['linked_file_id'],
                                    suffixes=['','_y'],
                                    indicator=True)

    ## Files without match in links data AND with a 'replacement_id'
    unmatched_replacement_df = merged_files_links_df.loc[(merged_files_links_df['_merge']=='left_only')
                                            & (merged_files_links_df['replacement_file_id'].notnull())
                                        , f_columns].copy(deep=True)
    ## join links to 'replacement_id'
    merged_replacement_df = pd.merge(unmatched_replacement_df, 
                                    file_links_content_df, 
                                    how='left',
                                    left_on=['replacement_file_id'], 
                                    right_on=['linked_file_id'],
                                    suffixes=['','_y'],
                                    indicator=True)

    ## Files with match in links data OR without a 'replacement_id'
    merged_files_df = merged_files_links_df.loc[(merged_files_links_df['_merge']=='both')
                                                | (merged_files_links_df['replacement_file_id'].isnull())
                                    ].copy(deep=True)


    ## Combine data from matches on 'file_id' AND 'replacement_id' PLUS data that was not matched
    ## This contains one row per LINK found in content
    full_files_and_links_df = pd.concat([merged_files_df[f_columns+f_links_columns], 
                                            merged_replacement_df[f_columns+f_links_columns]]).drop_duplicates()

    # Create summaries of links PER FILE

    # calculate link counts -- per FILE
    links_total_summary = pd.pivot_table(full_files_and_links_df, 
                                        index=['file_id'], 
                                        columns=['content_type'], 
                                        values='content_id',
                                        aggfunc='count', 
                                        fill_value=0)

    links_total_summary.columns.name = None
    content_type_list = list(links_total_summary.columns)  ## Save for future use


    links_total_summary['total_links_to_file'] = links_total_summary.sum(axis=1)
    links_total_summary['file_is_linked'] = links_total_summary['total_links_to_file'].apply(lambda x: x>0)
    links_total_summary.reset_index(inplace=True)
    measure_cols = content_type_list + ['total_links_to_file', 'file_is_linked']  ## Save for future use

    # create list of links to all content that contain a link to the file -- per FILE
    links_list_summary = full_files_and_links_df.loc[full_files_and_links_df['content_type'].notnull()].drop_duplicates().copy(deep=True)

    links_list_summary['course_canvas_id'] = links_list_summary['course_id'].apply(lambda x: None if pd.isnull(x) else str(canvas_helpers.convert_canvas_id(x, output_id_type='local')) )
    links_list_summary['joinable_content'] = links_list_summary['content_type'].apply(lambda x: x[:x.find('/')] if x.find('/')>-1 else x)


    links_list_summary['content_with_link'] = links_list_summary.apply(lambda x: utils.build_canvas_url(
                                                                            context_type='courses',
                                                                            context_id= x['course_canvas_id'], 
                                                                            content_type=x['joinable_content'], 
                                                                            content_id=x['content_id'],
                                                                            canvas_domain=None), axis=1)
    links_list_summary = links_list_summary.groupby(['file_id'], as_index=False).agg(link_list=('content_with_link',list))

    # Combine summary counts and link list
    links_summary = pd.merge(links_total_summary,
                                links_list_summary,
                                how='outer',
                                on=['file_id'])

    # Add links summary data to original file list -- one row per FILE
    logger.info(f"Join File links to Files data")
    all_files_plus_links_df = pd.merge(input_files_df, 
                                        links_summary, 
                                        how='left', 
                                        left_on=['file_id'],
                                        right_on=['file_id'])

    #all_files_plus_links_df.info()
    for c in measure_cols:
        all_files_plus_links_df[c].fillna(0, inplace=True)

    all_files_plus_links_df['file_is_linked'] = all_files_plus_links_df['file_is_linked'].astype(np.int64)
    all_files_plus_links_df[content_type_list] = all_files_plus_links_df[content_type_list].astype(np.int64)
    all_files_plus_links_df['total_links_to_file'] = all_files_plus_links_df['total_links_to_file'].astype(np.int64)

    return all_files_plus_links_df


def parse_kaltura_entries_from_links(link_list):
    ID_categories = ['entry_id', 'playlist_id', 'widget_id', 'tiny', 'OTHER']

    KALTURA_ID_REGEX_LIST = [
        ## _MYMEDIA_PATTERN must be listed/checked *before* _ID_PATTERN
        r'mymedia.usu.edu/(?P<key>playlist|media)[/=](?:[^/]+[/=])?(?P<value>\d\_[^/\&\?\=]+)',
        r"[/\?\&](?P<key>[^/\&\?\=]+)[/=](?P<value>\d\_[^/\&\?\=]+)",
        r'kaltura.com/(?P<key>tiny)/(?P<value>[^/\?]+)'
    ]

    match_pivot = {c: None for c in ID_categories}
    for link_text in link_list:
        for REGEX in KALTURA_ID_REGEX_LIST:
            if re.search(REGEX, link_text, re.IGNORECASE):
                for m in re.finditer(REGEX, link_text, re.IGNORECASE):
                    match_key, match_value = m.groupdict().get('key'), m.groupdict().get('value')
                    key = 'entry_id' if match_key.lower() in ['entry_id','entryid','media'] else \
                            'widget_id' if match_key.lower() in ['wid','widget_id'] else \
                            'playlist_id' if re.search('playlist', match_key.lower()) else \
                            match_key.lower() if match_key.lower() in ['tiny'] else 'OTHER'

                    match_pivot[key] = list(set( (match_pivot.get(key) or []) + [match_value] ))
                break
    return match_pivot


def parse_kaltura_players_from_links(link_list):
    KALTURA_PLAYER_REGEX_LIST = [
        r'/uiconf[\_]?id/([0-9]+)',
        r'/playerskin/([0-9]+)',
        r'kaltura.com/(tiny)',
        r'=(kalturaviewer)',
        r'(mediaspace)\.kaltura.com'
    ]
    players = []
    for link in link_list:
        for REGEX in KALTURA_PLAYER_REGEX_LIST:
            if re.search(REGEX, link, re.IGNORECASE):
                players.append(re.search(REGEX, link, re.IGNORECASE).group(1))
                break

    return {'players': list(set(players)) }


def parse_kaltura_ids_from_url_list(link_list):
    match_players_dict = parse_kaltura_players_from_links(link_list)
    match_entries_dict = parse_kaltura_entries_from_links(link_list)
    return {**match_players_dict, **match_entries_dict}

     
    # config_dir = os.path.join(current_dir, 'config')
    # data_dir = os.path.join(current_dir, 'data')






def main(config, db_conn, db_params, sql_dir, reports_dir, include_courses_without_content):
    # # parent_dir = os.path.split(current_dir)[0]
    # sql_dir = os.path.join(current_dir, 'sql')
    # reports_dir = os.path.join(current_dir, 'reports/NEW_test_main')

    ## ------------------ QUERYING DATABASE ----------------------
    logger.info("Starting process with settings:\n     "+f"{'    '.join(k+' = '+str(v) for k, v in {**db_params, **{'include_courses_without_content': include_courses_without_content}}.items())}")

    COURSES_CTE = get_sql_query_from_file(sql_dir, COURSES_CTE_NAME)

    ## GET COURSE DATA
    QUERY_NAME = QUERIES.get('courses')
    logger.info(f"Get data for: {QUERY_NAME}")
    query_text = COURSES_CTE + get_sql_query_from_file(sql_dir, QUERY_NAME)
    query_params = db_params
    course_df = get_db_data(db_conn, query_text, parameters=query_params)

    ## PROCESS COURSE DATA
    course_df['course_id'] = course_df['course_id'].apply(lambda x: None if pd.isnull(x) else str(int(x)))
    course_df['course_name'] = course_df['course_name'].apply(lambda x: utils.clean_ascii_string(x))
    course_df['delivery_method'] = course_df['course_name'].apply(lambda x: course_processor.CourseProcessor(course_name=x).get_delivery_method())
    #course_df.head()
    logger.info(f"\t {len(course_df)} data rows")

    ## GET COURSE AGG DATA
    QUERY_NAME = QUERIES.get('course_agg')
    logger.info(f"Get data for: {QUERY_NAME}")
    query_text = COURSES_CTE + get_sql_query_from_file(sql_dir, QUERY_NAME)
    query_params = db_params
    course_agg_df = get_db_data(db_conn, query_text, parameters=query_params)
    #course_agg_df.info()

    ## PROCESS COURSE AGG DATA
    course_agg_df['course_id'] = course_agg_df['course_id'].apply(lambda x: None if pd.isnull(x) else str(int(x)))
    #  course_agg_df.head()
    logger.info(f"\t {len(course_agg_df)} data rows")


    ## GET FILES DATA
    QUERY_NAME = QUERIES.get('files')
    logger.info(f"Get data for: {QUERY_NAME}")
    query_text = COURSES_CTE + get_sql_query_from_file(sql_dir, QUERY_NAME)
    query_params = db_params
    files_df = get_db_data(db_conn, query_text, parameters=query_params)
    #files_df.info()

    ## PROCESS FILES DATA
    files_df['course_id'] = files_df['course_id'].apply(lambda x: None if pd.isnull(x) else str(int(x)))
    files_df['file_id'] = files_df['file_id'].apply(lambda x: None if pd.isnull(x) else str(int(x)))
    files_df['replacement_file_id'] = files_df['replacement_file_id'].apply(lambda x: None if pd.isnull(x) else str(canvas_helpers.convert_canvas_id(x, output_id_type='local')) )


    files_df['file_name'] = files_df['file_name'].apply(lambda x: utils.clean_ascii_string(x))
    files_df['file_type'] = files_df['file_type'].apply(lambda x: utils.clean_ascii_string(x))

    files_df['file_extension'] = files_df['file_name'].apply(lambda x: parse_file_extension(x))
    files_df['file_category'] = files_df['file_type'].apply(lambda x: parse_file_category(x))

    files_df['file_created_at'] = pd.Series(files_df['file_created_at'], dtype='datetime64[ns]')
    files_df['file_updated_at'] = pd.Series(files_df['file_updated_at'], dtype='datetime64[ns]')

    #files_df.head()
    logger.info(f"\t {len(files_df)} data rows")

    ## GET CONTENT DATA
    content_df_list = [] 
    for QUERY_NAME in QUERIES.get('content'): 
        logger.info(f"Get data for: {QUERY_NAME}")
        query_text = COURSES_CTE + get_sql_query_from_file(sql_dir, QUERY_NAME)
        query_params = db_params
        df = get_db_data(db_conn, query_text, parameters=query_params)
        content_df_list.append(df)

    content_df = pd.concat(content_df_list).reset_index(drop=True)
    #content_df.info()

    ## PROCESS CONTENT DATA
    content_df['course_id'] = content_df['course_id'].apply(lambda x: None if pd.isnull(x) else str(int(x)))
    content_df['content_text'] = content_df['content_text'].apply(lambda x: canvas_helpers.clean_canvas_html_content(x))
    content_df['content_created_at'] = pd.Series(content_df['content_created_at'], dtype='datetime64[ns]')
    content_df['content_updated_at'] = pd.Series(content_df['content_updated_at'], dtype='datetime64[ns]')

    #content_df.head()
    logger.info(f"\t {len(content_df)} data rows")

    ### See bottom of script file for code that is in process
    ### ------------------
    ### WIP - SUB STEP 1 
    ### ------------------


    ## ------------------ PROCESSING NEW DATA ----------------------

    ## was 'content_links_df' now 'links_in_content_df'
    links_in_content_df = data_processor_parse_url_from_content(content_df)
    ## was 'merged_wysiwyg_df' 

    files_plus_linked_to_df = data_processor_find_files_linked_in_content(files_df, links_in_content_df )

    wysiwyg_plus_linked_to_df = data_processor_find_content_linked_in_content(content_df, links_in_content_df)


    content_with_broken_links_df = data_processor_find_content_with_broken_links(content_df, links_in_content_df)
    content_with_kaltura_links_df = data_processor_find_content_with_kaltura_links(content_df, links_in_content_df)
    content_with_youtube_links_df = data_processor_find_content_with_youtube_links(content_df, links_in_content_df)




    ## ---------------- SUMMARY CALCULATIONS ----------------------

    logger.info(f"Total Links to Content Items")
    content_totals = wysiwyg_plus_linked_to_df.groupby(['course_id']).agg(total_canvas=('content_id','count'), 
                                                                        total_canvas_with_link=('content_is_linked','sum')).reset_index()

    content_totals['percent_canvas_with_link'] = content_totals.apply(lambda x:  x['total_canvas_with_link']/x['total_canvas'] if x['total_canvas']>0 else np.nan, axis=1 )

    logger.info(f"Total Broken Links in Content")
    broken_totals = content_with_broken_links_df.groupby('course_id').agg(total_broken=('total_broken_links','sum')).reset_index()

    ## TEMP
    broken_totals_CHECK = links_in_content_df[(links_in_content_df['has_url_error']==True)].groupby('course_id').agg(total_broken=('url_from_text','count')).reset_index()
    logger.info(f"Check broken content totals:  {broken_totals['total_broken'].sum() == broken_totals_CHECK['total_broken'].sum()}")


    ### FOR Kaltura total in 'Course Summary'
    logger.info(f"Total Links to Kaltura")
    kaltura_totals = content_with_kaltura_links_df.groupby('course_id', as_index=False)['total_kaltura_links'].sum()

    ### FOR YouTube total in 'Course Summary'
    logger.info(f"Total Links to YouTube")
    youtube_totals = content_with_youtube_links_df.groupby('course_id', as_index=False)['total_youtube_links'].sum()


    ### FOR various Files totals in 'Course Summary'
    logger.info(f"Total Files and Links")

    logger.info(f"\t parse number of Files of various types")
    df_list = []
    for file_category in files_plus_linked_to_df.loc[files_plus_linked_to_df['file_category'].notnull()]['file_category'].unique():
        category_df = files_plus_linked_to_df.loc[files_plus_linked_to_df['file_category']==file_category].copy(deep=True)
        if len(category_df) > 0:
            category_df = category_df.groupby(['course_id']).agg(total=('file_id','nunique'), 
                                                        with_link=('file_is_linked','sum')).reset_index()
            category_df['percent_with_link'] = category_df.apply(lambda x: (x['with_link']/x['total']) if x['total']>0 else np.nan, axis=1 )

            category_df.rename(columns={"total": f"total_{file_category}",
                                    "with_link": f"total_{file_category}_with_link",
                                    "percent_with_link": f"percent_{file_category}_with_link"
                                    }, inplace=True)

            df_list.append(category_df)


    file_totals = files_plus_linked_to_df.groupby(['course_id']).agg(total_files=('file_id','nunique'), 
                                                                    total_files_with_link=('file_is_linked','sum')).reset_index()

    file_totals['percent_files_with_link'] = file_totals.apply(lambda x:  x['total_files_with_link']/x['total_files'] if x['total_files']>0 else np.nan, axis=1 )


    ## combine overall file totals AND per file type
    for file_category_df in df_list:
        file_totals = pd.merge(file_totals, 
                                file_category_df, 
                                how='left', 
                                on='course_id')







    ### ------------------------------------------------------------------------------------------------------
    ###                                             Write report
    ### ------------------------------------------------------------------------------------------------------
    ### ------------------------------------------- COURSE SUMMARY -------------------------------------------
    ### ------------------------------------------------------------------------------------------------------

    logger.info("------- Creating Course Summary Report -------")

    TOOL_LINK_MAP = {
        'TidyUp': {'content_name': 'TidyUp',  'content_type': 'external_tools', 'content_id': '28805'},
        'Accessibility Report': { 'content_name': 'A11y Report', 'content_type': 'external_tools', 'content_id': '29768'},
    }

    COURSE_SUMMARY_COLUMN_MAP = {
        0: { 'display_name': 'Course Id', 'data_name': 'course_canvas_id', },
        1: { 'display_name': 'Course', 'data_name': 'course_name', },
        2: { 'display_name': 'College', 'data_name': 'parent_account_name', },
        3: { 'display_name': 'Department', 'data_name': 'account_name', },
        4: { 'display_name': 'Delivery Method', 'data_name': 'delivery_method',},
        5: { 'display_name': 'New Course', 'data_name': '_IGNORE_',}, # Ignore
        6: { 'display_name': 'Term', 'data_name': 'term_name', },
        7: { 'display_name': 'Instructors', 'data_name': 'instructors', },
        8: { 'display_name': 'TidyUp', 'data_name': 'TidyUp',},
        9: { 'display_name': 'Accessibility Report', 'data_name': 'Accessibility Report',},
        10: { 'display_name': 'Number of Students', 'data_name': 'total_students', },
        11: { 'display_name': 'Files Navigation Visible?', 'data_name': 'files_navigation', },
        12: { 'display_name': 'Navigation Items', 'data_name': 'total_navigation_items', },
        13: { 'display_name': 'Files', 'data_name': 'total_files', },
        14: { 'display_name': 'Files with Link', 'data_name': 'total_files_with_link', },
        15: { 'display_name': '% Files in Use', 'data_name': 'percent_files_with_link', },
        16: { 'display_name': 'Files A11y Score', 'data_name': '_IGNORE_',},  # Ignore
        17: { 'display_name': 'WYSIWYG Content', 'data_name': 'total_canvas', },
        18: { 'display_name': 'WYSIWYG Content with Link', 'data_name': 'total_canvas_with_link', },
        19: { 'display_name': '% Canvas Content in Use', 'data_name': 'percent_canvas_with_link', },
        20: { 'display_name': 'WYSIWYG A11y Score', 'data_name': '_IGNORE_',}, # Ignore
        21: { 'display_name': 'Overall Accessibility Score', 'data_name': '_IGNORE_',}, # Ignore
        22: { 'display_name': 'Instructor Communication', 'data_name': '_IGNORE_',}, # Ignore
        23: { 'display_name': 'Notes', 'data_name': '_IGNORE_',}, # Ignore
        24: { 'display_name': 'PDF Files', 'data_name': 'total_pdf',},
        25: { 'display_name': 'PDF Files in Use', 'data_name': 'percent_pdf_with_link',}, 
        26: { 'display_name': "PDF Scanned not OCR'd", 'data_name': '_IGNORE_',}, # Ignore
        27: { 'display_name': 'PDF to HTML Status', 'data_name': '_IGNORE_',}, # Ignore
        28: { 'display_name': 'Images', 'data_name': 'total_image', },
        29: { 'display_name': 'Images in Use', 'data_name': 'percent_image_with_link', },
        30: { 'display_name': 'Images without Alt Text', 'data_name': '_IGNORE_',}, # Ignore
        31: { 'display_name': '% Images Needing Alt Text', 'data_name': '_IGNORE_',}, # Ignore
        32: { 'display_name': 'Images Status', 'data_name': '_IGNORE_',}, # Ignore
        33: { 'display_name': 'Kaltura Videos', 'data_name': 'total_kaltura_links',},
        34: { 'display_name': 'Videos', 'data_name': 'total_video',}, 
        35: { 'display_name': 'YouTube Videos', 'data_name': 'total_youtube_links',},
        36: { 'display_name': 'Caption Status', 'data_name': '_IGNORE_',}, # Ignore
        37: { 'display_name': 'Flash Content', 'data_name': 'total_flash',}, 

        38: { 'display_name': 'Potential Broken Links', 'data_name': 'total_broken', },   ######### TEMPORARY
    }

    if include_courses_without_content:
        WITH_EMPTY_COURSES_COLUMN_MAP = {}

        for k, v in COURSE_SUMMARY_COLUMN_MAP.items():
            if k < 7:
                WITH_EMPTY_COURSES_COLUMN_MAP[k] = v
            elif k == 7:
                WITH_EMPTY_COURSES_COLUMN_MAP[k] = { 'display_name': 'Number of Instructors', 'data_name': 'total_instructors', }
                WITH_EMPTY_COURSES_COLUMN_MAP[k+1] = v
            else:
                WITH_EMPTY_COURSES_COLUMN_MAP[k+1] = v

        COURSE_SUMMARY_COLUMN_MAP = WITH_EMPTY_COURSES_COLUMN_MAP

    COURSE_SUMMARY_COLUMN_ORDER = [v.get('display_name') for k, v in COURSE_SUMMARY_COLUMN_MAP.items()]

    hyperlink_cols = ['Course'] + list(TOOL_LINK_MAP.keys())
    COURSE_SUMMARY_CUSTOM_COLUMN_TYPES = {k: 'hyperlink' for k in hyperlink_cols}


    ## Create DataFrame
    course_summary_report_df = course_df.copy(deep=True)


    course_summary_measure_dfs = [
        course_agg_df,
        content_totals,
        file_totals,
        kaltura_totals,
        youtube_totals,
        broken_totals
    ]

    course_summary_merge_fields = 'course_id'

    for measure_df in course_summary_measure_dfs:
        course_summary_report_df = pd.merge(course_summary_report_df, 
                                                measure_df, 
                                                on='course_id', 
                                                how='left')
        
        for col in measure_df.select_dtypes(include=['int64', 'float64']).columns:
            course_summary_report_df[col] = course_summary_report_df[col].fillna(0)
            

    if not include_courses_without_content:
        course_summary_report_df = course_summary_report_df.loc[(course_summary_report_df['total_canvas']>0) 
                                                            | (course_summary_report_df['total_files']>0)].copy(deep=True)



    ## Format DataFrame
    course_summary_report_df, column_formats = prep_column_format_for_report(course_summary_report_df, 
                                                                                report_column_map=COURSE_SUMMARY_COLUMN_MAP, 
                                                                                custom_types_map=COURSE_SUMMARY_CUSTOM_COLUMN_TYPES)



    course_summary_report_df['Course'] = course_summary_report_df.apply(lambda x: exl_writer.format_excel_hyperlink(
                                                    utils.build_canvas_url('courses',
                                                                        x['Course Id'])
                                                , x['Course']), axis=1)


    for col, link_map in TOOL_LINK_MAP.items():
        course_summary_report_df[col] = course_summary_report_df.apply(lambda x: exl_writer.format_excel_hyperlink(
                                                    utils.build_canvas_url('courses', 
                                                                        x['Course Id'], 
                                                                        link_map.get('content_type'),
                                                                        link_map.get('content_id')),
                                                    link_map.get('content_name')
                                                ), axis=1)


    ## Save DataFrame to report
    SUB_NAME_TERMS = '_'.join(db_params.get('term_sis_list')) if len(db_params.get('term_sis_list')) <= 2 else 'Multiple'
    ### MOD FOR NEAL (all courses -- including courses without content)
    if include_courses_without_content:
        SUB_NAME_TERMS = 'Including Empty Courses - ' + SUB_NAME_TERMS
    REPORT_NAME = os.path.join(reports_dir, 'Course Summary - {sub_name} - {date}.xlsx')

    exl_writer.save_custom_xls(REPORT_NAME.format(sub_name=SUB_NAME_TERMS, date=utils.current_date()), 
                                list_dfs=[course_summary_report_df[COURSE_SUMMARY_COLUMN_ORDER]],
                                list_nms=['Courses'],
                                column_formats=column_formats)





    ### ------------------------------------------------------------------------------------------------------
    ###                                             Write report
    ### ------------------------------------------------------------------------------------------------------
    ### ----------------------------------------------- KALTURA ----------------------------------------------
    ### ------------------------------------------------------------------------------------------------------
    logger.info("------- Creating Kaltura Report -------")
    ### WORK IN PROGRESS
    KALTURA_COLUMN_MAP = {
        0: { 'display_name': 'Course Id', 'data_name': 'course_canvas_id' },
        1: { 'display_name': 'Course', 'data_name': 'course_name' },
        2: { 'display_name': 'Term', 'data_name': 'term_name' },

        3: { 'display_name': 'Content Name', 'data_name': 'content_name' },
        4: { 'display_name': 'Content Type', 'data_name': 'content_type' },
        5: { 'display_name': 'Content Id', 'data_name': 'content_id' },
        6: { 'display_name': 'Content State', 'data_name': 'content_state' },

        7: { 'display_name': 'Content Created', 'data_name': 'content_created_at' },
        8: { 'display_name': 'Content Updated', 'data_name': 'content_updated_at' },

        9: { 'display_name': 'Total Kaltura Links', 'data_name': 'total_kaltura_links' },

        10: {'display_name': 'Player Ids', 'data_name': 'players'},

        11: {'display_name': 'Entry Ids', 'data_name': 'entry_id'},
        12: {'display_name': 'Playlist Ids', 'data_name': 'playlist_id'},
        13: {'display_name': 'Widget Ids', 'data_name': 'widget_id'},
        14: {'display_name': 'Tiny Ids', 'data_name': 'tiny'},

        15: { 'display_name': 'Kaltura Video Links', 'data_name': 'link_list' },
    }

    KALTURA_COLUMN_ORDER = [v.get('display_name') for k, v in KALTURA_COLUMN_MAP.items()]

    hyperlink_cols = ['Course', 'Content Name']
    multiline_cols = ['Kaltura Video Links'] 

    KALTURA_CUSTOM_COLUMN_TYPES = {
        **{k: 'hyperlink' for k in hyperlink_cols},
        **{k: 'general_multiline' for k in multiline_cols}
    }

    ## Create DataFrame
    kaltura_report_df = pd.merge(course_df[['course_id','course_canvas_id','course_name','term_name']],
                                    content_with_kaltura_links_df,
                                    on=['course_id'])
    for col in ['content_created_at', 'content_updated_at']:
        kaltura_report_df[col] = kaltura_report_df[col].apply(lambda x: x if pd.isnull(x) else x.strftime('%Y-%m-%d'))


    ## Format DataFrame
    kaltura_report_df, column_formats = prep_column_format_for_report(kaltura_report_df, 
                                                                    report_column_map=KALTURA_COLUMN_MAP, 
                                                                    custom_types_map=KALTURA_CUSTOM_COLUMN_TYPES)


    kaltura_report_df['Course'] = kaltura_report_df.apply(lambda x: exl_writer.format_excel_hyperlink(
                                                        utils.build_canvas_url('courses',
                                                                            x['Course Id'])
                                                    , x['Course']), axis=1)


    kaltura_report_df['Content Name'] = kaltura_report_df.apply(lambda x: exl_writer.format_excel_hyperlink(
                                                        utils.build_canvas_url('courses',
                                                                            x['Course Id'],
                                                                            content_type=(x['Content Type'] if x['Content Type'].find('/')<0 else x['Content Type'][:x['Content Type'].find('/')]),
                                                                            content_id=x['Content Id']
                                                                            )
                                                    , x['Content Name']), axis=1)

    ## Save DataFrame to report
    SUB_NAME_TERMS = '_'.join(db_params.get('term_sis_list')) if len(db_params.get('term_sis_list')) <= 2 else 'Multiple'
    REPORT_NAME = os.path.join(reports_dir, 'Kaltura - {sub_name} - {date}.xlsx')

    exl_writer.save_custom_xls(REPORT_NAME.format(sub_name=SUB_NAME_TERMS, date=utils.current_date()),
                                list_dfs=[kaltura_report_df[KALTURA_COLUMN_ORDER]],
                                list_nms=['Kaltura'],
                                column_formats=column_formats)



    ### ------------------------------------------------------------------------------------------------------
    ###                                             Write report
    ### ------------------------------------------------------------------------------------------------------
    ### -------------------------------------------- BROKEN LINKS --------------------------------------------
    ### ------------------------------------------------------------------------------------------------------

    logger.info("------- Creating Broken Links Report -------")

    BROKEN_LINKS_COLUMN_MAP = {
        0: { 'display_name': 'Course Id', 'data_name': 'course_canvas_id' },
        1: { 'display_name': 'Course', 'data_name': 'course_name' },
        2: { 'display_name': 'Term', 'data_name': 'term_name' },
        3: { 'display_name': 'Content Name', 'data_name': 'content_name' },
        4: { 'display_name': 'Content Type', 'data_name': 'content_type' },
        5: { 'display_name': 'Content Id', 'data_name': 'content_id' },
        6: { 'display_name': 'Content State', 'data_name': 'content_state' },

        7: { 'display_name': 'Content Created', 'data_name': 'content_created_at' },
        8: { 'display_name': 'Content Updated', 'data_name': 'content_updated_at' },

        9: { 'display_name': 'Total Broken Links', 'data_name': 'total_broken_links' },
        10: { 'display_name': 'Potential Broken Links', 'data_name': 'link_list' },
    }
    BROKEN_LINKS_COLUMN_ORDER = [v.get('display_name') for k, v in BROKEN_LINKS_COLUMN_MAP.items()]

    hyperlink_cols = ['Course', 'Content Name']
    multiline_cols = ['Potential Broken Links'] 

    BROKEN_LINKS_CUSTOM_COLUMN_TYPES = {
        **{k: 'hyperlink' for k in hyperlink_cols},
        **{k: 'general_multiline' for k in multiline_cols}
    }

    ## Create DataFrame
    broken_links_report_df = pd.merge(course_df[['course_id','course_canvas_id','course_name','term_name']],
                                content_with_broken_links_df,
                                on=['course_id'])

    for col in ['content_created_at', 'content_updated_at']:
        broken_links_report_df[col] = broken_links_report_df[col].apply(lambda x: x if pd.isnull(x) else x.strftime('%Y-%m-%d'))


    ## Format DataFrame
    broken_links_report_df, column_formats = prep_column_format_for_report(broken_links_report_df, 
                                                                    report_column_map=BROKEN_LINKS_COLUMN_MAP, 
                                                                    custom_types_map=BROKEN_LINKS_CUSTOM_COLUMN_TYPES)


    broken_links_report_df['Course'] = broken_links_report_df.apply(lambda x: exl_writer.format_excel_hyperlink(
                                                        utils.build_canvas_url('courses',
                                                                            x['Course Id'])
                                                    , x['Course']), axis=1)


    broken_links_report_df['Content Name'] = broken_links_report_df.apply(lambda x: exl_writer.format_excel_hyperlink(
                                                        utils.build_canvas_url('courses',
                                                                            x['Course Id'],
                                                                            content_type=(x['Content Type'] if x['Content Type'].find('/')<0 else x['Content Type'][:x['Content Type'].find('/')]),
                                                                            content_id=x['Content Id']
                                                                            )
                                                    , x['Content Name']), axis=1)

    ## Save DataFrame to report
    SUB_NAME_TERMS = '_'.join(db_params.get('term_sis_list')) if len(db_params.get('term_sis_list')) <= 2 else 'Multiple'
    REPORT_NAME = os.path.join(reports_dir, 'Broken Links - {sub_name} - {date}.xlsx')

    exl_writer.save_custom_xls(REPORT_NAME.format(sub_name=SUB_NAME_TERMS, date=utils.current_date()),
                            list_dfs=[broken_links_report_df[BROKEN_LINKS_COLUMN_ORDER]],
                            list_nms=['Broken Links'],
                            column_formats=column_formats)






    ### ------------------------------------------------------------------------------------------------------
    ###                                             Write report
    ### ------------------------------------------------------------------------------------------------------
    ### ---------------------------------------------- ALL FILES ---------------------------------------------
    ### ------------------------------------------------------------------------------------------------------

    logger.info("------- Creating All Files Report -------")

    ALL_FILES_COLUMN_MAP = {
        0: { 'display_name': 'Course Id', 'data_name': 'course_canvas_id' },
        1: { 'display_name': 'Course', 'data_name': 'course_name' },
        2: { 'display_name': 'Term', 'data_name': 'term_name' },
        3: { 'display_name': 'File Name', 'data_name': 'file_name' },
        4: { 'display_name': 'File Id', 'data_name': 'file_id' },
        5: { 'display_name': 'File State', 'data_name': 'file_state' },
        6: { 'display_name': 'File Type', 'data_name': 'file_type' },
        7: { 'display_name': 'File Extension', 'data_name': 'file_extension' },
        8: { 'display_name': 'File Created', 'data_name': 'file_created_at' },
        9: { 'display_name': 'File Updated', 'data_name': 'file_updated_at' },
        10: { 'display_name': 'File Linked', 'data_name': 'file_is_linked' },
        11: { 'display_name': 'Total Links to File', 'data_name': 'total_links_to_file' },
        12: { 'display_name': 'announcements', 'data_name': 'announcements' },
        13: { 'display_name': 'assignments', 'data_name': 'assignments' },
        14: { 'display_name': 'discussions', 'data_name': 'discussions' },
        15: { 'display_name': 'modules', 'data_name': 'modules' },
        16: { 'display_name': 'pages', 'data_name': 'pages' },
        17: { 'display_name': 'quizzes', 'data_name': 'quizzes' },
        18: { 'display_name': 'quizzes/answers', 'data_name': 'quizzes/answers' },
        19: { 'display_name': 'quizzes/questions', 'data_name': 'quizzes/questions' },
        20: { 'display_name': 'syllabus', 'data_name': 'syllabus' },
        21: { 'display_name': 'Links to Content', 'data_name': 'link_list' },
    }
    ALL_FILES_COLUMN_ORDER = [v.get('display_name') for k, v in ALL_FILES_COLUMN_MAP.items()]


    hyperlink_cols = ['Course']
    multiline_cols = ['Links to Content'] 

    ALL_FILES_CUSTOM_COLUMN_TYPES = {
        **{k: 'hyperlink' for k in hyperlink_cols},
        **{k: 'general_multiline' for k in multiline_cols}
    }
    ALL_FILES_CUSTOM_COLUMN_TYPES = {
        **ALL_FILES_CUSTOM_COLUMN_TYPES,
        **{k: 'black_integers' for k in dict_sort_content.keys()}
    }



    ## Create DataFrame
    all_files_report_df = pd.merge(course_df[['course_id','course_canvas_id','course_name','term_name']],
                                    files_plus_linked_to_df,
                                    on=['course_id'])
    for col in ['file_created_at', 'file_updated_at']:
        all_files_report_df[col] = all_files_report_df[col].apply(lambda x: x if pd.isnull(x) else x.strftime('%Y-%m-%d'))

    all_files_report_df['file_is_linked'] = all_files_report_df['file_is_linked'].astype(bool)


    ## Format DataFrame
    all_files_report_df, column_formats = prep_column_format_for_report(all_files_report_df, 
                                                                    report_column_map=ALL_FILES_COLUMN_MAP, 
                                                                    custom_types_map=ALL_FILES_CUSTOM_COLUMN_TYPES)

    all_files_report_df['Course'] = all_files_report_df.apply(lambda x: exl_writer.format_excel_hyperlink(
                                                        utils.build_canvas_url('courses',
                                                                            x['Course Id'])
                                                    , x['Course']), axis=1)

    ## Save DataFrame to report
    SUB_NAME_TERMS = '_'.join(db_params.get('term_sis_list')) if len(db_params.get('term_sis_list')) <= 2 else 'Multiple'
    REPORT_NAME = os.path.join(reports_dir, 'All Files - {sub_name} - {date}.xlsx')

    exl_writer.save_custom_xls(REPORT_NAME.format(sub_name=SUB_NAME_TERMS, date=utils.current_date()),
                            list_dfs=[all_files_report_df[ALL_FILES_COLUMN_ORDER]],
                            list_nms=['Files'],
                            column_formats=column_formats)







    ### ------------------------------------------------------------------------------------------------------
    ###                                             Write report
    ### ------------------------------------------------------------------------------------------------------
    ### ----------------------------------------------- WYSIWYG ----------------------------------------------
    ### ------------------------------------------------------------------------------------------------------

    ### WORK IN PROGRESS
    ##  --------------------------------------------------------
    #  FOR QUIZZES CONTENT ITEMS -- 
    # APPEND NUMBER OF QUESTIONS TO content_text 
    # SO CHECKING FOR "has_content" WILL BE ACCURATE, 
    # EVEN IF THERE IS NO DESCRIPTION FOR THE QUIZ
    ##  --------------------------------------------------------

    logger.info("------- Creating WYSIWYG Report -------")
    WYSIWYG_COLUMN_MAP = {
        0: { 'display_name': 'Course Id', 'data_name': 'course_canvas_id'},
        1: { 'display_name': 'Course', 'data_name': 'course_name'},
        2: { 'display_name': 'Term', 'data_name': 'term_name'},
        3: { 'display_name': 'WYSIWYG Name', 'data_name': 'content_name'},
        4: { 'display_name': 'WYSIWYG Id', 'data_name': 'content_id'},
        5: { 'display_name': 'WYSIWYG Type', 'data_name': 'content_type'},
        6: { 'display_name': 'WYSIWYG Status', 'data_name': 'content_state'},
        7: { 'display_name': 'In Modules', 'data_name': 'in_modules'},
        8: { 'display_name': 'Has Content', 'data_name': 'has_content'},
        9: { 'display_name': 'Kaltura Video Links', 'data_name': 'total_kaltura_links'},
        10: { 'display_name': 'WYSIWYG Created', 'data_name': 'content_created_at'},
        11: { 'display_name': 'WYSIWYG Updated', 'data_name': 'content_updated_at'},
        12: { 'display_name': 'WYSIWYG Linked', 'data_name': 'content_is_linked'},
        13: { 'display_name': 'Total Links to WYSIWYG', 'data_name': 'total_links_to_content'},
        14: { 'display_name': 'Links to Content', 'data_name': 'content_urls_with_links'},
    }


    WYSIWYG_COLUMN_ORDER = [v.get('display_name') for k, v in WYSIWYG_COLUMN_MAP.items()]

    hyperlink_cols = ['Course', 'WYSIWYG Name']
    multiline_cols = ['Links to Content'] 

    WYSIWYG_CUSTOM_COLUMN_TYPES = {
        **{k: 'hyperlink' for k in hyperlink_cols},
        **{k: 'general_multiline' for k in multiline_cols}
    }


    ## Create DataFrame
    wysiwyg_report_df = pd.merge(course_df[['course_id','course_canvas_id','course_name','term_name']],
                                    wysiwyg_plus_linked_to_df,
                                    on=['course_id'])


    wysiwyg_report_df = pd.merge(wysiwyg_report_df,
                                    content_with_kaltura_links_df[['course_id','content_type','content_id','total_kaltura_links']],
                                    how='left',
                                    on=['course_id','content_type','content_id'])

    wysiwyg_report_df['total_kaltura_links'] = wysiwyg_report_df['total_kaltura_links'].fillna(0)

    wysiwyg_report_df['content_is_linked'] = wysiwyg_report_df['content_is_linked'].astype(bool)

    for col in ['content_created_at', 'content_updated_at']:
        wysiwyg_report_df[col] = wysiwyg_report_df[col].astype('datetime64[s]').apply(lambda x: x if pd.isnull(x) else x.strftime('%Y-%m-%d'))

    wysiwyg_report_df['content_state'] = wysiwyg_report_df['content_state'].apply(lambda x: canvas_helpers.clean_canvas_workflow_states(x))

    ## Format DataFrame
    wysiwyg_report_df, column_formats = prep_column_format_for_report(wysiwyg_report_df, 
                                                                    report_column_map=WYSIWYG_COLUMN_MAP, 
                                                                    custom_types_map=WYSIWYG_CUSTOM_COLUMN_TYPES)


    wysiwyg_report_df['Course'] = wysiwyg_report_df.apply(lambda x: exl_writer.format_excel_hyperlink(
                                                        utils.build_canvas_url('courses',
                                                                            x['Course Id'])
                                                    , x['Course']), axis=1)


    wysiwyg_report_df['WYSIWYG Name'] = wysiwyg_report_df.apply(lambda x: exl_writer.format_excel_hyperlink(
                                                        utils.build_canvas_url('courses',
                                                                            x['Course Id'],
                                                                            content_type=(x['WYSIWYG Type'] if x['WYSIWYG Type'].find('/')<0 else x['WYSIWYG Type'][:x['WYSIWYG Type'].find('/')]),
                                                                            content_id=x['WYSIWYG Id']
                                                                            )
                                                    , x['WYSIWYG Name']), axis=1)

    ## Save DataFrame to report
    SUB_NAME_TERMS = '_'.join(db_params.get('term_sis_list')) if len(db_params.get('term_sis_list')) <= 2 else 'Multiple'
    REPORT_NAME = os.path.join(reports_dir, 'WYSIWYG Content - {sub_name} - {date}.xlsx')

    exl_writer.save_custom_xls(REPORT_NAME.format(sub_name=SUB_NAME_TERMS, date=utils.current_date()),
                                list_dfs=[wysiwyg_report_df[WYSIWYG_COLUMN_ORDER]],
                                list_nms=['WYSIWYG'],
                                column_formats=column_formats)


# version = 14  ## new version -- executable script after mods -- 2021-12-06
# if __name__ == 'main':
global env, args, working_dir, app_config, logger

working_dir = os.path.dirname(os.path.abspath(__file__))

load_dotenv()

args = script_args()
env = args.env or ENV or 'dev'

logger = initialize_logger(location=['STDOUT', working_dir+'/logs/'], script_name='run') 
logger.info("created")
full_config = utils.load_yaml_config(config_path=os.path.join(working_dir,'config'), config_file='config.yaml')
app_config = full_config['environments'][env]

database_name = args.database_name or app_config['database_name']
db_config = full_config['databases'][database_name]

if os.getenv('DB_USER'):
    db_config['db_user'] = os.getenv('DB_USER')
if os.getenv('DB_PASSWORD'):
    db_config['db_password'] = os.getenv('DB_PASSWORD')
if os.getenv('SSH_PRIVATE_KEY'):
    db_config['ssh_private_key'] = os.getenv('SSH_PRIVATE_KEY')

if args.include_no_content_courses is not None:
    include_no_content_courses = args.include_no_content_courses 
elif app_config.get('include_no_content_courses') is not None:
    include_no_content_courses = app_config['include_no_content_courses']
else:
    include_no_content_courses = DEFAULT_INCLUDE_NON_CONTENT_COURSES



print(f"Include courses without content?  {include_no_content_courses}")

sql_directory = utils.create_directory(working_dir, app_config['sql_directory'])
reports_directory = utils.create_directory(working_dir, app_config['report_directory'])

# create DatabaseAccess object from custom module
dba = database_access.DatabaseAccess()
dba.set_db_config(settings=db_config)
#ssh_server, db_conn = dba.get_db_connection()
db_conn = dba.get_db_connection()


TERM_ID_LIST = args.term_id or get_terms_list(db_conn, sql_directory, TERM_FILTER_QUERY)
if not isinstance(TERM_ID_LIST, list):
    TERM_ID_LIST = [TERM_ID_LIST]

logger.info(f"-- Execute reports process -- ")
for term in TERM_ID_LIST:
    
    PARAMS = {
        'term_sis_list': [term]
    }
    main(config=app_config, 
            db_conn=db_conn, 
            db_params=PARAMS, 
            sql_dir=sql_directory, 
            reports_dir=reports_directory, 
            include_courses_without_content=include_no_content_courses)

logger.info("...Done...")

### -----------------------------------------------------------------------------------------------------------------------
### ------------------
### WIP - SUB STEP 1 
### ------------------
# --------------------------- STARTING HERE : NEW PROCESSING FIND ALL LINKS IN CONTENT -- WORK IN PROGRESS -------------------------------------------------
# def convert_to_unicode(text):
#     if text:
#         converted = UnicodeDammit(text)
#     # if not converted.unicode_markup:
#     #     raise UnicodeDecodeError('Failed to detect encoding, tried [%s]')
#     # else:
#         return converted
#     else:
#         return None
#     #converted.original_encoding
# content_df['content_text'] = content_df['content_text'].apply(lambda x: convert_to_unicode(x))
### -----------------------------------------------------------------------------------------------------------------------

