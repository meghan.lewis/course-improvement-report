import os
import sys

import logging
import logging.config

import smtplib

# For guessing MIME type based on file name extension
import mimetypes

from email import encoders
from email.message import Message
from email.mime.audio import MIMEAudio
from email.mime.base import MIMEBase
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

logger = logging.getLogger(__name__)

COMMASPACE = ', '


# class EmailMessage:
#     def __init__(self, sender='', recipient=[], subject="Automated Message", email_content="", email_html_content="", bcc=[]):
#         self._sender = sender
#         self._recipient = recipient
#         self.subject = subject
#         self.email_content = email_content
#         self.email_html_content = email_html_content
#         self.bcc = bcc

#     @property
#     def sender(self):
#         return self.__sender

#     @sender.setter
#     def sender(self, a:str):
#         self.__sender = a

#     @property
#     def recipient(self):
#         return self.__recipient

#     @recipient.setter
#     def recipient(self, a):
#         self.__recipient = a



def configure_email(sender='', recipients=[], directory=None, file_list=[], subject="Automated Message", email_content="", email_html_content="", bcc=[]):

    if not sender or not recipients:
        logger.info('Must provide sender email address and recipient email address(es)')
        sys.exit(1)
    # if not directory:
    #     logger.info('No directory provided. Using current directory.')

    logger.info('Configure email')
    # Create the enclosing (outer) message
    outer = MIMEMultipart('mixed')
    outer['Subject'] = subject
    outer['To'] = COMMASPACE.join(recipients) if type(recipients)==type(list()) else recipients
    outer['From'] = sender

    # If html text content should be embedded in email
    if len(email_html_content)>0:
        html_text = '<p>' + email_content + '</p><br>' + email_html_content
        msgHTML = MIMEText(html_text,'html')
        outer.attach(msgHTML)
    else:
        msgBody = MIMEText(email_content)
        outer.attach(msgBody)

    if directory:
        logger.info('Get Files...')
        for file_name in os.listdir(directory):
            # if 'file_list' is specified, only include file if it IS IN LIST
            # otherwise, include all files in directory
            if len(file_list or []) > 0 and file_name not in file_list:
                pass
            else:
                path = os.path.join(directory, file_name)
                if not os.path.isfile(path):
                    continue
                # Guess the content type based on the file's extension.  Encoding
                # will be ignored, although we should check for simple things like
                # gzip'd or compressed files.
                ctype, encoding = mimetypes.guess_type(path)
                if ctype is None or encoding is not None:
                    # No guess could be made, or the file is encoded (compressed), so
                    # use a generic bag-of-bits type.
                    ctype = 'application/octet-stream'
                maintype, subtype = ctype.split('/', 1)
                #print( file_name, maintype, subtype)

                if maintype == 'text':
                    fp = open(path)
                    # Note: we should handle calculating the charset
                    msg = MIMEText(fp.read(), _subtype=subtype)
                    fp.close()
                elif maintype == 'image':
                    fp = open(path, 'rb')
                    msg = MIMEImage(fp.read(), _subtype=subtype)
                    fp.close()
                elif maintype == 'audio':
                    fp = open(path, 'rb')
                    msg = MIMEAudio(fp.read(), _subtype=subtype)
                    fp.close()
                else:
                    fp = open(path, 'rb')
                    msg = MIMEBase(maintype, subtype)
                    msg.set_payload(fp.read())
                    fp.close()
                    # Encode the payload using Base64
                    encoders.encode_base64(msg)

                try:
                    logger.info('attach file: %s' % file_name)
                    # Set the filename parameter
                    msg.add_header('Content-Disposition', 'attachment', filename=file_name)
                    outer.attach(msg)
                except Exception:
                    logger.exception("error", exc_info=True)

    # Now send or store the message
    composed = outer.as_string()

    # BCC the sender address
    if bcc:
        recipients+=bcc
    if sender not in recipients:
        recipients.append(sender)


    logger.info('Sending email...')

    try:
        send_email(composed, recipients, sender)
        return
    except Exception:
        logger.exception("error", exc_info=True)


def send_email(message, recipient, sender):
    try:
        server = smtplib.SMTP('mail.usu.edu')
        # server.starttls()
        server.sendmail(sender, recipient, message)
        server.quit()
        logger.info('Complete')
        return
    except Exception:
        logger.exception("Error sending mail", exc_info=True)

