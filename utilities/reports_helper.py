# import os
# import datetime

import pandas as pd
import numpy as np
from pandas import ExcelWriter
from xlsxwriter.utility import xl_range

import logging

logger = logging.getLogger(__name__)


numeric_types = ['int16', 'int32', 'int64', 'float16', 'float32', 'float64']


def format_columns(workbook, sheets, column_lists):

    left_fmt = workbook.add_format({'align': 'left'})
    center_fmt = workbook.add_format({'align': 'center', 'num_format': '#,##0'})
    total_fmt = workbook.add_format({'align': 'center','bold': True, 'font_size': 12, 'num_format': '#,##0'})

    format_dict = {'left': left_fmt,
                   'center': center_fmt,
                  'total': total_fmt}

    for i, sheet in enumerate(sheets):
        for key, value in column_lists[i].items():
            sheet.set_column(int(key), int(key), value[0], format_dict.get(value[1]))
    return


def df_list_as_tables(table_dfs, table_nms, sheet_name, header_list, workbook_obj):
    col_formats = {}
    row_offset = 0
    worksheet = workbook_obj.add_worksheet(sheet_name)
    table_head_fmt = workbook_obj.add_format({'align': 'center',
                                              'bold': True,
                                              'font_size': 12,
                                              'num_format': '#,##0'})

    # header rows at top of sheet
    if header_list['general']:
        sheet_header = header_list['general']
        row_offset = len(sheet_header)
        for x in range(0, row_offset):
            worksheet.write_string(x, 0, sheet_header[x])
        row_offset+=2


    #for each dataframe ---
    for i, df in enumerate(table_dfs):
        row_len = len(df.index)
        col_len = len(df.columns)

        # Add Chart Titles
        chart_titles = header_list[table_nms[i]]
        title_len = len(chart_titles)
        for x in range(0, title_len):
            cell_range = xl_range( (row_offset+x), 0,
                                   (row_offset+x), (col_len-1) )
            worksheet.merge_range(cell_range, chart_titles[x], table_head_fmt)

        # Add columns to list for table headers
        column_names = []
        for idx, col in enumerate(df):  # loop through all columns
            column_names.append({"header": col})

            # IF first dataframe, Create column format list
            if i==0:
                #resize worksheet columns
                series = df[col]
                max_len = max((
                        series.astype(str).map(len).max(),  # len of largest item
                        len(str(series.name))  # len of column name/header
                    )) + 2

                if col == 'Total':
                    col_formats[str(idx)] = (10, 'total')
                elif df[col].dtypes in numeric_types:
                    col_formats[str(idx)] = (max_len, 'center')
                else:
                    col_formats[str(idx)] = (max_len, 'left')

        # Cell range for table
        cell_range = xl_range(row_offset+title_len, 0,
                              row_offset+title_len+row_len, col_len-1)
        # Add table to worksheet
        worksheet.add_table(cell_range, {'data': df.as_matrix(),
                                        'columns': column_names,
                                        'autofilter': False})
        # Format bottom table row
        worksheet.set_row(row_offset+title_len+row_len, 16, table_head_fmt)
        # Update offset value
        row_offset = row_offset+title_len+row_len+3

    return (worksheet, col_formats)


def save_xls(xls_path, list_dfs=[], list_nms=[], table_dfs=[], table_nms=[], table_header_dict={}, table_sheet_name="Summary"):
    """
    Exports pandas dataframes to excel using ExcelWriter.
        list_dfs - A list of dataframes to export to individual excel sheets.
        list_nms - A list of names to give to each sheet created from
                    the dataframes in "list_dfs".
        table_dfs - list of dataframes to export/format as tables into
                    a single excel sheet.
        table_nms - A list of names to give to each table created from
                    the dataframes in "table_dfs".
        table_headers - Dictionary of headers to append between the table names
                    and tables created from "table_dfs" and "table_nms". Dictionary
                    items should be given the same name as the corresponding
                    name in "table_nms".
                    *** If a dictionary item is provided with the name "general",
                        it will be appened to the sheet BEFORE any of the tables.
        table_sheet_name - The name of the excel sheet containing the tables
                    created from "table_dfs".

    """
    format_sheets_list=[]
    format_cols_list=[]

    writer = ExcelWriter(xls_path, engine='xlsxwriter')
    workbook = writer.book

    # Summary Sheet
    if len(table_dfs)>0:
        sheet, col_formats = df_list_as_tables(table_dfs, table_nms, table_sheet_name, table_header_dict, workbook)
        # If column formats returned -- add to list to be formatted
        if len(col_formats)>0:
            format_sheets_list.append(sheet)
            format_cols_list.append(col_formats)

    # Individual Sheets
    for i, df in enumerate(list_dfs):
        col_formats = {}
        df.to_excel(writer, list_nms[i], index=False)
        worksheet = writer.sheets[list_nms[i]]

        # Create column format list
        for idx, col in enumerate(df):  # loop through all columns
            #resize worksheet columns
            series = df[col]
            max_len = max((
                    series.astype(str).map(len).max(),  # len of largest item
                    len(str(series.name))  # len of column name/header
                )) + 2  # adding a little extra space

            if df[col].dtypes in numeric_types:
                col_formats[str(idx)] = (max_len, 'center')
            else:
                col_formats[str(idx)] = (max_len, 'left')
        # Add to list to be formatted
        format_sheets_list.append(worksheet)
        format_cols_list.append(col_formats)

    # Format all sheets/columns in lists
    format_columns(workbook, format_sheets_list, format_cols_list)

    writer.save()

    return "complete"


# if __name__ == '__main__':
#     main()

