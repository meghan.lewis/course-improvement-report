# import html ## ADDED 2020-05-06
# import sys
# import json

import os
import re
import yaml
import datetime
from dateutil.relativedelta import relativedelta
import pytz
import logging
import chardet

import pandas as pd

logger = logging.getLogger(__name__)

def load_yaml_config(config_path='config', config_file='config.yaml'):
    """Load configuration settings from YAML file(s)"""
    with open(os.path.join(config_path, config_file), 'r') as file:
        config = yaml.safe_load(file) or {}

    return config


def current_date():
    """ Returns current date/time as string """
    return datetime.datetime.now().strftime("%Y-%m-%d")


def current_time():
    """ Returns current time as string """
    return datetime.datetime.now().strftime("%I:%M:%S")
    

def current_datetime():
    """ Returns current date/time as string """
    return datetime.datetime.now().strftime("%Y-%m-%d %I:%M:%S")

def date_add(date,date_part,part_count):

    delta_dict = {
        'seconds': datetime.timedelta(seconds=part_count),
        'minutes': datetime.timedelta(minutes=part_count),
        'hours': datetime.timedelta(hours=part_count),
        'days': datetime.timedelta(days=part_count),
        'weeks': datetime.timedelta(weeks=part_count)
    }

    new_date = datetime.datetime.strptime(str(date), '%Y-%m-%d') + delta_dict.get(date_part)
    return new_date


def convert_timezones(timestamp, new_tz='US/Mountain', date_format="%Y-%m-%d %H:%M:%S"):
    if not timestamp:
        return None
    try:
        timestamp_string = str(timestamp).replace('T', ' ')[:19]

        ts_local = datetime.datetime.strptime(timestamp_string, "%Y-%m-%d %H:%M:%S") \
                                .replace(tzinfo=pytz.UTC) \
                                .astimezone(pytz.timezone(new_tz))
            
        formatted_local = datetime.datetime.strptime(str(ts_local), date_format)

    except Exception:
        logger.exception("Error converting timezone for: " + timestamp_string, exc_info=True)
        raise
    return formatted_local


def time_diff(start, end):
    """Returns a string with the amount of time passed between two datetime variables """
    try:
        tdif = relativedelta(end, start)
    except Exception:
        logger.exception("Error calculating time difference", exc_info=True)
        raise
    return '{h}h {m}m {s}s'.format(h=tdif.hours, m=tdif.minutes, s=tdif.seconds)



def list_string_param_names(input_string):
    regex_python_params = '\{([^{}/]+)\}'
    regex_psycopg_params = '\%\(([^\)]+)\)s'
    param_names = []
    param_names += re.findall(regex_python_params, input_string)
    param_names += re.findall(regex_psycopg_params, input_string)
    return param_names


def get_file_text(file_location):
    """
        Reads content of text file
    """
    with open(file_location, "r") as file:
        file_text = file.read()
    return file_text


def check_file_exists(file_location):
    return os.path.exists(file_location)


def create_empty_file(file_location):
    try:
        f = open(file_location, "w+")
        f.close()
        return
    except Exception:
        logger.exception('Error creating file %s' % file_location,
                               exc_info=True)
        raise


def create_directory(parent_path, directory_name):
    """Creates directory in the parent_path, if directory does not exists
        (by setting exist_ok as True, error caused by existing 
        directory can be suppressed)
        """
    try:
        full_path = os.path.join(parent_path, directory_name)
        os.makedirs(full_path, exist_ok = True)
        print("Directory '%s' created successfully" % directory_name)
        return full_path
    except OSError as error:
        print("Directory '%s' can not be created" % directory_name)


def delete_file_if_exists(directory, file_name):
    file_path = os.path.join(directory, file_name)
    if check_file_exists(file_path):
        try:
            logger.info(f"Deleting file: '{file_name}'")
            os.unlink(file_path)   
        except Exception:
            logger.exception('Error deleting files', exc_info=True)
            raise
    return



def remove_all_files(directory, move_directory=None, delete=True):
    """
        Deletes *OR MOVES* all files from the provided directory.
    """
    if delete:
        logger.info(f'DELETING All Files from: "{directory}"')
    else:
        if not move_directory:
            raise("Error must provide new directory or set delete=True")
        else:
            logger.info(f'MOVING All Files from "{directory}" to "{move_directory}"')
    try:
        for file in os.scandir(directory):
            if file.is_file():
                if delete:
                    os.unlink(file.path)
                else:
                    try:
                        os.rename(f'{directory}/{file.name}',
                                  f'{move_directory}/{file.name}')
                    except FileExistsError:
                        logger.info(f"File already exists in {move_directory}. {file.name} will be deleted instead.")
                        os.unlink(file.path)
        # os.rename('/Users/billy/d1/xfile.txt', '/Users/billy/d2/xfile.txt')
    except Exception:
        logger.exception('Error deleting files', exc_info=True)
        raise
    return



def truncate_text_file(file_name, search_string, empty_file=False):
    """
    Searches for a string within a text file and
    removes any rows that appear BEFORE that string
    in the text file.
    If the string is not found, AND
    - empty_file is "True" - delete everything in the file
    - empty_file is "False" - do not change file
    """
    lines=0
    string_found=False

    if os.path.isfile(file_name):
        try:
            with open(file_name, 'r') as rfile:
                for line in rfile:
                    if search_string in line:
                        string_found=True
                        break
                    lines += 1
                if string_found==True:
                    rfile.seek(0)
                    data="".join(rfile.readlines()[(lines):-1])
                    if len(data)>0:
                        with open(file_name,"w") as wfile:
                            wfile.write(data)
                elif string_found==False and empty_file==True:
                    open(file_name, 'w').close()
        except Exception:
            logger.exception('error truncating file')
            raise



# def clean_html_content(input_html):
#     """
#     Remove text carriage returns from HTML

#     *** 2020-05-06 -- Added html.unescape ***
#     """
#     if input_html is None:
#         return ''
#     else:
#         return html.unescape(re.sub(r'(\\r\\n|\\r|\\n)', '', input_html))

def clean_ascii_string(input_string, replace_char=''):
    """
    Remove non-ascii characters from input string
    """
    if input_string:
        return ''.join(i if ord(i)<128 else replace_char for i in input_string )
    else:
        return input_string


def convert_encoding(data, new_coding='latin1'):
    """
    Accepts a str or bytes object and converts to provided encoding.
    """
    if not data or not isinstance(data, (str, bytes)):
        pass
        #logger.info("'%s' object cannot be encoded." % type(data).__name__)
    else:
        try:
            if isinstance(data, bytes):
                encoding = chardet.detect(data)
                if encoding['encoding']:
                    data = data.decode(encoding['encoding'],'ignore')
            data = data.encode(new_coding,'ignore')
        except Exception:
            logger.exception("Error encoding: " + data, exc_info=True)
            raise
    return data

# def convert_encoding(data, new_coding='latin1'):

#     try:
#         if data is not None:
#             try:
#                 if isinstance(data, str):
#                     data = data.encode(new_coding,'ignore')
#                 elif isinstance(data, bytes):
#                     encoding = chardet.detect(data)
#                     data = data.decode(encoding['encoding'],'ignore').encode(new_coding,'ignore')
#             except Exception:
#                 ogger.exception("Error converting character encoding " + data, exc_info=True)
#                raise
#     except Exception:
#         logger.exception("Error with: " + data, exc_info=True)
#        raise
#     return data





def build_canvas_url(context_type, context_id, content_type=None, content_id=None, canvas_domain='usu.instructure.com'):
    url_parts = []

    if canvas_domain:
        url_parts.append('https://' + canvas_domain)
    url_parts.append(context_type)
    url_parts.append(str(context_id))
    if content_type:
        if content_type == 'syllabus':
            url_parts.append('assignments')
        elif content_type.find('/')<0:
            url_parts.append(content_type)
        else:
            url_parts.append(content_type[:content_type.find('/')])
    if content_id:
         url_parts.append(str(content_id))

    return '/'.join(url_parts)


def split_dataframe_list(df, target_column, separator='array'):
    '''
        df = dataframe to split,
        target_column = the column containing the values to split
        separator = the symbol used to perform the split  ('array' if target_column is a list)

        returns: a dataframe with each entry for the target column separated, with each element moved into a new row.
        The values in the other columns are duplicated across the newly divided rows.
    '''
    row_accumulator = []

    def splitListToRows(row, separator):
        try:
            if separator=='array':
                split_row = row[target_column]
            else:
                split_row = row[target_column].split(separator)
            for s in split_row:
                new_row = row.to_dict()
                new_row[target_column] = s
                row_accumulator.append(new_row)
        except Exception:
            logger.exception("Error splitting DataFrame", exc_info=True)
            raise

    df.apply(splitListToRows, axis=1, args = (separator, ))
    new_df = pd.DataFrame(row_accumulator)


    return new_df


