import logging
import re
from bs4 import BeautifulSoup
import html
import urllib

import warnings
## Suppress warnings about bs4 find
warnings.filterwarnings("ignore", category=UserWarning, message='.*looks like a (filename|URL).*', module='bs4')

logger = logging.getLogger(__name__)

# WORKFLOW_STATE_MAPPING = {
#     'deleted': ['deleted'],
#     'inactive': ['unpublished', 'created', 'claimed', 'broken', 'hidden', 'errored', 'duplicating', 'failed_to_duplicate'],
#     'published': ['active', 'available', 'completed', 'published', 'post_delayed', 'locked']
# }
WORKFLOW_STATE_MAPPING = {
    'deleted': ['deleted'],
    'unpublished': ['unpublished', 'created', 'claimed',  'hidden'],
    'published': ['active', 'available', 'completed', 'published', 'post_delayed', 'locked']
}

#  'broken',  'errored', 'duplicating', 'failed_to_duplicate'

def convert_canvas_id(input_id, output_id_type='local', shard_id=1009):
    try:
        input_id = int(input_id)
    except Exception as e:
        logger.error(f"Error converting ID - {e}")
    offset = (shard_id*10000000000000)
    if output_id_type == 'global' and input_id < offset:
        return input_id + offset
    elif output_id_type == 'local' and input_id > offset:
        return input_id - offset
    else:
        return input_id


def clean_canvas_workflow_states(workflow_state, state_map=WORKFLOW_STATE_MAPPING):
    for new_state, options in state_map.items():
        if workflow_state in options:
            return new_state
    return workflow_state


def clean_canvas_html_content(html_content):
    if html_content:
        html_content = re.sub(r'(\\r\\n|\\r|\\n)', ' ', html_content)
        html_content = html.unescape(html_content)
        html_content = urllib.parse.unquote(html_content)
    return html_content


def parse_links_from_content(html_content, include_raw_text_links=False, include_mail_tel_links=False):
    """Parses HTML tags containing urls/links from the HTML of Canvas content items
    Arguments:
        html_content: string containing the HTML body from a Canvas content item
    Returns:
        links: list of url/links found in the content HTML
    """
    RAW_LINK_REGEX = re.compile(r'https?://[^\s,\)”\"\(\']{4,}')
    if not html_content:
        return []
    try:
        all_links = []
        # Parse HTML content
        soup = BeautifulSoup(html_content, 'lxml')

        # Search for all hyperlinks and iframe links
        all_tags = []
        all_tags += soup.findAll('a', href=True) #))
        all_tags += soup.findAll('iframe', src=True)
        all_tags += soup.findAll('img', src=True)

        for t in all_tags:
            if t.has_attr('src'):
                all_links.append(t.get('src'))
            elif t.has_attr('href'):
                if not include_mail_tel_links and re.search('(mailto|tel):', t.get('href')):
                    pass
                else:
                    all_links.append(t.get('href'))

        if include_raw_text_links:
            text_links = RAW_LINK_REGEX.findall(soup.getText())
            text_links = list(set([x.strip('.') for x in text_links if x.strip('.') not in all_links and len(x.strip('.'))>8]))
            all_links += text_links

        return all_links

    except Exception:
        logger.exception("Error parsing links from content.", exc_info=True)


def parse_tags_from_content(html_content, include_raw_text_links=False, include_mail_tel_links=False):
    """Parses HTML tags containing urls/links from the HTML of Canvas content items
    Arguments:
        html_content: string containing the HTML body from a Canvas content item
    Returns:
        links: list of url/links found in the content HTML
    """
    # LINK_REGEX = re.compile(r'https?://[^\s,\)”\"\(\']{4,}')
    if not html_content:
        return []
    try:
        all_links = []
        # Parse HTML content
        soup = BeautifulSoup(html_content, 'lxml')

        # Search for all hyperlinks and iframe links
        all_tags = []
        all_tags += soup.findAll('a', href=True) #))
        all_tags += soup.findAll('iframe', src=True)
        all_tags += soup.findAll('img', src=True)

        return all_tags

    except Exception:
        logger.exception("Error parsing links from content.", exc_info=True)