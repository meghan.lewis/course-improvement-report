import re

import logging
logger = logging.getLogger(__name__)

COURSE_REGEX = r'(?P<term>(?:Fa|Su|Sp)(?:\w+\s\d{2})?\d{2})\s(?P<subject>\w{2,4})-(?P<number>\d{4})-(?P<section>\w{3})\s?(?P<is_xlist>XL)?'
SECTION_NUMERIC_RANGES = [
        {'start': 101, 'end': 130, 'method': 'Lab', 'campus': 'USU Eastern'},
        {'start': 201, 'end': 207, 'method': 'Online', 'campus': 'USU Eastern'},
        {'start': 350, 'end': 361, 'method': 'Broadcast', 'campus': 'USU Eastern'},
        {'start': 401, 'end': 434, 'method': 'Concurrent', 'campus': 'USU Eastern'},
        {'start': 501, 'end': 550, 'method': 'Lab', 'campus': 'Logan'},
        {'start': 570, 'end': 599, 'method': 'Study Abroad', 'campus': 'Logan'},
        {'start': 601, 'end': 650, 'method': 'Broadcast', 'campus': 'Logan'},
        {'start': 670, 'end': 699, 'method': 'Online', 'campus': 'Logan'},
        {'start': 801, 'end': 850, 'method': 'Broadcast', 'campus': 'USU Eastern'},
        {'start': 901, 'end': 950, 'method': 'Concurrent', 'campus': 'USU Eastern'}
    ]
MODALITY_LETTER_CODES = {
    'O': 'Online',
    'C': 'Concurrent',
    'B': 'Broadcast',
    'S': 'Supervised',
    'L': 'Lab',
    'W': 'Web Broadcast',
    'H': 'Hybrid Face-to-Face',
    'T': 'Face-to-Face'
}
CAMPUS_LETTER_CODES = {
    'A': 'Blanding',
    'B': 'Brigham City',
    'C': 'South West (Ephraim)',
    'E': 'Moab',
    'K': 'Kaysville',
    'L': 'Logan',
    'N': 'North Central (Orem)',
    'P': 'Price',
    'S': 'Salt Lake City',
    'T': 'Tooele',
    'U': 'Uintah Basin (Vernal / Roosevelt)',
    'X': 'Special Programs',
    'Y': 'China Program',
    'Z': 'Out-of-State',
}

class CourseProcessor:

    def __init__(self, course_name):
        self.course_name = course_name
        self.REGEX_name_parts = re.compile(COURSE_REGEX)
        self.numeric_section_codes = SECTION_NUMERIC_RANGES
        self.letter_modality_codes = MODALITY_LETTER_CODES
        self.letter_campus_codes = CAMPUS_LETTER_CODES
        self.course_term = None
        self.course_subject = None
        self.course_number = None
        self.course_section = None
        self.course_is_xlist = False
        self.delivery_method = None
        self.campus = None

        self.parse_course_name_parts(self.course_name)

    def parse_course_name_parts(self, course_name):
        m = self.REGEX_name_parts.search(course_name)
        if m:
            parts = m.groupdict()
            if parts.get('term'): self.course_term = parts.get('term')
            if parts.get('subject'): self.course_subject = parts.get('subject')
            if parts.get('number'): self.course_number = parts.get('number')
            if parts.get('section'): self.course_section = parts.get('section')
            if parts.get('is_xlist'): self.course_is_xlist = True
            return
        else:
            return

    def get_delivery_method(self):
        if not self.course_section:
            self.delivery_method = 'Unknown'
            return self.delivery_method
        section_code = self.course_section
        if re.match('\d{3}',section_code):
            section_code = int(section_code)
            for ncode in self.numeric_section_codes:
                if ncode.get('start') <= section_code <= ncode.get('end'):
                    self.delivery_method = ncode.get('method')
                    return self.delivery_method
            self.delivery_method = 'Face-to-Face'
            return self.delivery_method
        else:
            if section_code[1] in self.letter_modality_codes.keys():
                self.delivery_method = self.letter_modality_codes.get(section_code[1])
                return self.delivery_method
            else:
                self.delivery_method = 'Face-to-Face'
                return self.delivery_method


    def get_campus(self, section_code):
        if not self.course_section:
            self.campus = 'Unknown'
            return self.campus
        section_code = self.course_section
        if re.match(r'\d{3}', section_code):
            section_code = int(section_code)
            for ncode in self.numeric_section_codes:
                if ncode.get('start') <= section_code <= ncode.get('end'):
                    self.campus = ncode.get('campus')
                    return self.campus
            self.campus = 'Logan'
            return self.campus
        elif re.match(r'[PS][1-5]\d',section_code):
            self.campus = 'USU Eastern'
            return self.campus

        else:
            if section_code[0] in self.letter_campus_codes.keys():
                self.campus = self.letter_campus_codes.get(section_code[0])
                return self.campus
            else:
                self.campus = 'Unknown'
                return self.campus
