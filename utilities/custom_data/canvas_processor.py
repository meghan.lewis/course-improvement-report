import re

import logging
logger = logging.getLogger(__name__)

class DataProcessor:
    def __init__(self):

        self.REGEX_name_parts = re.compile(r'(?P<term>(?:Fa|Su|Sp)(?:\w+\s\d{2})?\d{2})\s(?P<subject>\w{2,4})-(?P<number>\d{4})-(?P<section>\w{3})\s?(?P<xlist>XL)?')

        self.numeric_section_codes = [
            {'start': 101, 'end': 130, 'method': 'Lab', 'campus': 'USU Eastern'},
            {'start': 201, 'end': 207, 'method': 'Online', 'campus': 'USU Eastern'},
            {'start': 350, 'end': 361, 'method': 'Broadcast', 'campus': 'USU Eastern'},
            {'start': 401, 'end': 434, 'method': 'Concurrent', 'campus': 'USU Eastern'},
            {'start': 501, 'end': 550, 'method': 'Lab', 'campus': 'Logan'},
            {'start': 570, 'end': 599, 'method': 'Study Abroad', 'campus': 'Logan'},
            {'start': 601, 'end': 650, 'method': 'Broadcast', 'campus': 'Logan'},
            {'start': 670, 'end': 699, 'method': 'Online', 'campus': 'Logan'},
            {'start': 801, 'end': 850, 'method': 'Broadcast', 'campus': 'USU Eastern'},
            {'start': 901, 'end': 950, 'method': 'Concurrent', 'campus': 'USU Eastern'}
        ]

        self.letter_section_codes = {
            'O': 'Online',
            'C': 'Concurrent',
            'B': 'Broadcast',
            'S': 'Supervised',
            'L': 'Lab',
            'W': 'Web Broadcast',
            'H': 'Hybrid Face-to-Face',
            'T': 'Face-to-Face'
        }

        self.letter_campus_codes = {
            'A': 'Blanding',
            'B': 'Brigham City',
            'C': 'South West (Ephraim)',
            'E': 'Moab',
            'K': 'Kaysville',
            'L': 'Logan',
            'N': 'North Central (Orem)',
            'P': 'Price',
            'S': 'Salt Lake City',
            'T': 'Tooele',
            'U': 'Uintah Basin (Vernal / Roosevelt)',
            'X': 'Special Programs',
            'Y': 'China Program',
            'Z': 'Out-of-State',
        }
        
    def parse_course_name_parts(self, course_name):
        m = self.REGEX_name_parts.search(course_name)
        if m: return m.groupdict()
        else: return {}

    def delivery_method_from_section(self, section_code):
        if not section_code:
            return 'Unknown'
        elif re.match('\d{3}',section_code):
            section_code = int(section_code)
            for ncode in self.numeric_section_codes:
                if ncode.get('start') <= section_code <= ncode.get('end'):
                    return ncode.get('method')
            return 'Face-to-Face'
        else:
            if section_code[1] in self.letter_section_codes.keys():
                return self.letter_section_codes.get(section_code[1])
            else:
                return 'Face-to-Face'
            
            
    def campus_from_section(self, section_code):
        if not section_code:
            return 'Unknown'
        elif re.match('\d{3}',section_code):
            section_code = int(section_code)
            for ncode in self.numeric_section_codes:
                if ncode.get('start') <= section_code <= ncode.get('end'):
                    return ncode.get('campus')
            return 'Logan'
        elif re.match('[PS][1-5]\d',section_code):
            return 'USU Eastern'
        else:
            if section_code[0] in self.letter_campus_codes.keys():
                return self.letter_campus_codes.get(section_code[0])
            else:
                return 'Unknown'
