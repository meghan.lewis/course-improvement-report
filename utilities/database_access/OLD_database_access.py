# import os
# import logging.config
# import time

import logging
import json
from sshtunnel import SSHTunnelForwarder
import sqlalchemy
from getpass import getpass

import urllib.parse as urlparse

logger = logging.getLogger(__name__)

DEFAULT_CONFIG_FILE_PATH = None

class DatabaseAccess(object):
    def __init__(self):
        self.db_config = None
        self.ssh_server = None
        self.db_connection_engine = None
        self.db_user = None
        self.db_password = None

    def set_db_config(self, settings=None):
        if settings and isinstance(settings, dict) and len(settings)>0:
            self.db_config = settings
        elif settings and isinstance(settings, str):
            logger.info("Reading database settings from provided file path: {}".format(settings))
            self.db_config = self._read_json_file(settings)
        elif DEFAULT_CONFIG_FILE_PATH:
            logger.info("Database settings have not been provided. \n Retrieving default database settings from: {}".format(DEFAULT_CONFIG_FILE_PATH))
            self.db_config = self._read_json_file(DEFAULT_CONFIG_FILE_PATH)
        else:
            logger.error("Database settings have not been provided.")

        if not all([x in self.db_config.keys() for x in ['host', 'database']]):
            self.db_config = None
            logger.exception("The database settings JSON does not contain all required parameters ('host','database').")
        return self.db_config

    def set_db_user(self, user):
        self._db_user = user

    def set_db_password(self, password):
        self._db_password = password

    def get_db_connection(self):
        if self.db_connection_engine:
            pass
        elif not self.db_connection_engine and self.db_config:
            self.create_db_connection()
        else:
            logger.info("A database connection has not been established.")
        return self.db_connection_engine

    def get_ssh_server(self):
        if not self.ssh_server:
            logger.info("SSH server connection does not exist")
        return self.ssh_server

    def create_db_connection(self, db_settings=None, username=None, password=None):
        if self.db_config and (not db_settings or len(db_settings)==0):
            db_config = self.db_config.copy()
        else:
            db_config = self.set_db_config(db_settings)
            self.db_config = db_config.copy()

        # if not username and db_config['db_user']:
        #     user_name = db_config['db_user']
        # if not password and db_config['db_password']:
        #     password = db_config['db_password']

        if username:
            self.set_db_password(username) 
        if password:
            self.set_db_password(password) 

        if "ssh_host" in db_config.keys():
            ## returns 2 objects -- ssh_server, db_conn
            return self._connect_database_with_ssh(db_config)
        else:
            ## returns 1 object -- db_conn
            return self._connect_database(db_config)

    def _connect_database(self, config):
        logger.info("Connecting to database")
        try:
            conn_string = self._create_conn_string(config)
            engine = sqlalchemy.create_engine(conn_string)
            self.db_connection_engine = engine
            return self.db_connection_engine
        except Exception:
            logger.exception("Could not connect to database.", exc_info=True)

    def _connect_database_with_ssh(self, config):
        logger.info("Creating SSH Tunnel")
        logger.info(config)
        try:
            ssh_server = SSHTunnelForwarder(
                (config.get('ssh_host'), int(config.get('ssh_port') or 22)),
                ssh_username=config.get('ssh_user'),
                ssh_pkey=config.get('ssh_private_key'),
                remote_bind_address=(config.get("host"),
                                     int(config.get("port"))))
            ssh_server.start()
            self.ssh_server = ssh_server
            config["host"] = config["localhost"]
            config["port"] = ssh_server.local_bind_port
            return self.ssh_server, self._connect_database(config)
        except Exception as e:
            logger.exception("Could not create SSH Tunnel.", exc_info=True)
            raise e

    def _create_conn_string(self, config):
        if all([x in config.keys() for x in ['driver_version', 'windows_auth']]):
            return self._conn_string_mssql_windows_auth(config)
        elif all([x in config.keys() for x in ['driver_version']]):
            return self._conn_string_mssql(config)
        else:
            return self._conn_string_general(config)

    def _conn_string_general(self, config):
        config = self._get_db_credentials(config)
        template = '{dialect}://{db_user}:{db_password}@{host}:{port}/{database}'
        return template.format(**config)

    def _conn_string_mssql_windows_auth(self, config):
        template = 'mssql+pyodbc:///?odbc_connect={}'
        param_string = 'DRIVER={driver_version}; \
                        SERVER={host}; \
                        DATABASE={database}; \
                        trusted_connection=yes'.format(**config)

        return template.format(urlparse.quote_plus(param_string))

    def _conn_string_mssql(self, config):
        config = self._get_db_credentials(config)
        template = 'mssql+pyodbc:///?odbc_connect={}'
        param_string = 'DRIVER={driver_version}; \
                     SERVER={host}; \
                     DATABASE={database}; \
                     UID={db_user}; \
                     PWD={db_password}'.format(**config)
        return template.format(urlparse.quote_plus(param_string))

    def _get_db_credentials(self, config):
        db_user, db_password = None, None
        if 'db_user' in config.keys():
            db_user = config['db_user']
        elif self._db_user:
            db_user = self._db_user
        else:
            db_user = self._prompt_login_username()
        if 'db_password' in config.keys():
            db_password = config['db_password']
        elif self._db_password:
            db_password = self._db_password
        else:
            db_password = self._prompt_login_password(db_user)
        self._db_user = db_user
        self._db_password = db_password
        return {**config, **{'db_user': db_user, 'db_password': db_password}}


    def _prompt_login_username(self):
        return input("  **Login credentials required** -- Please enter the DB username: ")

    def _prompt_login_password(self, user):
        return getpass(f"  **Login credential required** -- Please enter the password for user '{user}': ")

    def _read_json_file(self, file_path):
        if not file_path.endswith(".json"):
            logger.exception("Error: provided file does not have '.json' extension -- {}".format(file_path))
        with open(file_path, 'r') as f:
            contents = json.load(f)
            f.close()
        return contents

























