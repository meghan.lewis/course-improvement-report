# -*- coding: utf-8 -*-
import logging
from logging.handlers import RotatingFileHandler


#class Logger(object):
#    def __init__(self, level=None, location=None, format=None):
#        self.level = level
#        self.location = location
#        self.format = format


def initialize_logger(level='INFO', location=['STDOUT'], format=None, script_name='log_file'):
    default_format='%(asctime)s [%(levelname)s]  %(message)s \t {%(filename)s:%(lineno)d}' #'(%(name)s)'
    date_format = '%Y-%m-%d %H:%M:%S'
    # Configure the root logger and library loggers as desired.
#    loggers = [ logging.getLogger(),]

    # Capture runtime warnings so that we'll see them.
    logging.captureWarnings(True)

    logger = logging.getLogger()
    handlers = []
    if isinstance(location, str):
        location = [location]
    for locate in location:
        # If location is configured as "STDOUT", don't create a new log file".
        if locate == 'STDOUT':
             handlers.append(logging.StreamHandler()) #logger.handlers
        else:
            locate = '_'.join([locate, script_name]) + '.log'
            file_handler = RotatingFileHandler(locate,
                                               mode='a',
                                               maxBytes=1024 * 1024 * 5,
                                               backupCount=5)
            handlers.append(file_handler)

    for handler in handlers:
        handler.setLevel(level)
        formatter = logging.Formatter(format or default_format, datefmt=date_format)
        handler.setFormatter(formatter)

        logger.addHandler(handler)
    logger.setLevel(level)

    return logger
