import logging

import numpy as np
import re
import pandas as pd
from pandas import ExcelWriter
from xlsxwriter.utility import xl_range

logger = logging.getLogger()

DATE_FORMAT = "YYYY-MM-DD"
DATETIME_FORMAT = "YYYY-MM-DD HH:MM:SS"

DEFAULT_EXCEL_FORMATS = {
    'general': {
        'align': 'left',
        'font_size': 10,
        'valign' : 'vcenter'
    },
    'general_multiline': {
        'align': 'left',
        'font_size': 10,
        'valign' : 'vcenter',
        'text_wrap': True
    },
    'gray_background': {
        'bg_color': '#F0F0F0',
        'font_size': 10,
        'valign' : 'vcenter',
    },
    'hyperlink': {
        'font_color': 'blue',
        'font_size': 10,
        'underline': 1,
        'valign' : 'vcenter'
    },
    'percent': {
        'align': 'center',
        'font_size': 10,
        'num_format': '0.0%',
        'valign' : 'vcenter'
    },
    'black_integers': {
        'align': 'center',
        'font_size': 10,
        'num_format': '#,##0',
        'valign' : 'vcenter'
    },
    'italic_black_integers': {
        'align': 'center',
        'font_size': 10,
        'italic': True,
        'num_format': '#,##0',
        'valign' : 'vcenter'
    },
    'blue_integers': {
        'align': 'center',
        'font_size': 10,
        'num_format': '[Blue]#,##0;(#,##0);_(* "-"_);_(@_)',
        'valign' : 'vcenter'
    },
    'red_integers': {
        'align': 'center',
        'font_size': 10,
        'num_format': '[Red]#,##0;(#,##0);_(* "-"_);_(@_)',
        'valign' : 'vcenter'
    },
    'subtotal_integers': {
        'align': 'center',
        'bg_color': '#FBF6EF',
        'font_size': 11,
        'num_format': '#,##0',
        'valign' : 'vcenter'
    },
    'total_integers': {
        'align': 'center',
        'bg_color': '#FFFDF1',
        'bold': True,
        'font_size': 11,
        'num_format': '#,##0',
        'valign' : 'vcenter'
    }
}

def format_excel_hyperlink(full_url, display_text=''):
    hyperlink_str = '=HYPERLINK("{url_link}", "{url_name}")'
    if len(display_text)==0:
        display_text = full_url
    else:
        display_text = display_text.replace('"',"'")
    return hyperlink_str.format(url_link=full_url, url_name=display_text)

def calculate_column_width(series):
    def hyperlink_display_text(x):
        regex_string = r'=HYPERLINK\(\"[^\"]+\"\,\s?\"([^\"]+)\"\)'
        if not x or not re.search(regex_string, x): return ''
        return re.search(regex_string, x).group(1)
    def max_len_in_list(x):
        max_len_str = ''
        if x is not None and isinstance(x, (list, np.ndarray)) and len(x) > 0:
            for y in x:
                if len(y or '') >= len(max_len_str):
                    max_len_str = y
        return max_len_str

    is_list_column = any([isinstance(x, (list, np.ndarray)) for x in series])
    is_hyperlink = any([re.match('=HYPERLINK', x) for x in series.astype(str)])

    if is_list_column:
        logger.info(f"Is List: {series.name}")
        max_len = max([
            series.apply(max_len_in_list).map(len).max(),  # -------------------
            len(str(series.name))  # len of column name/header
        ])
    if is_hyperlink:
        max_len = max([
            series.astype(str).apply(hyperlink_display_text).map(len).max(),  # len of largest DISPLAY for hyperlink
            len(str(series.name))  # len of column name/header
        ])
    else:
        max_len = max([
            series.astype(str).map(len).max(),  # len of largest item
            len(str(series.name))  # len of column name/header
        ]) 
    return max_len + 2  # adding a little extra space

def save_custom_xls(xls_path, list_dfs=[], list_nms=[], column_formats={}, conditional_formats={}, custom_sheets=[]):
    """
        Generates an Excel file

        xls_path: path/name of the output Excel file
        list_dfs: list of Pandas DataFrames - data to write to Excel File, each as an Excel Sheet
        list_nms: list of strings - names to give to Excel sheets created from 'list_dfs'
                 (index-based with 'list_dfs' -- if names are not provided, sheets will be named 'Sheet #')
        column_formats: dictionary - custom columns formats for the Excel file
                        (key= column name, value= dictionary with custom format mappings)

    """
    # logger.info(f"Column Formats: {column_formats}")
    try:
        # Create ExcelWriter object and workbook
        writer = ExcelWriter(xls_path, 
                                engine='xlsxwriter',
                                date_format=DATE_FORMAT,
                                datetime_format=DATETIME_FORMAT)

        if len(custom_sheets)>0:
            writer = build_custom_sheets(custom_sheets, writer)

        workbook = writer.book
        # Writes individual sheets to workbook
        for i, df in enumerate(list_dfs):
            if list_nms[i]:
                sheet_name = list_nms[i]
            else:
                sheet_name = 'Sheet %d' % i

            df.to_excel(writer, sheet_name, index=False)
            worksheet = writer.sheets[sheet_name]
            logger.info(f"Sheet: {sheet_name}")
            # Format worksheet columns
            for idx, col in enumerate(df):  # loop through all columns in sheet
                default_format = workbook.add_format({'align': 'left', 'font_size': 10})
                logger.info(f"Column: {col}")
                #resize columns
                #series = df[col]

                col_width = calculate_column_width(df[col])

                if column_formats.get(col):
                    if isinstance(column_formats.get(col), str):
                        logger.info(f"\tFormat: {column_formats.get(col)}")
                        if column_formats.get(col) in DEFAULT_EXCEL_FORMATS.keys():
                            if column_formats.get(col).find('multiline') > -1:
                                worksheet.set_column(int(idx), int(idx), min(col_width, 150), workbook.add_format(DEFAULT_EXCEL_FORMATS[column_formats.get(col)]))
                            else:
                                worksheet.set_column(int(idx), int(idx), min(col_width, 50), workbook.add_format(DEFAULT_EXCEL_FORMATS[column_formats.get(col)]))
                        else:
                            logger.info(f"\tFormat '{column_formats.get(col)}' not found in default formats. Using 'General' format.")
                            worksheet.set_column(int(idx), int(idx), min(col_width, 50), default_format)
                    elif isinstance(column_formats.get(col), dict):
                        logger.info(f"\tFormat: *custom dict*")
                        worksheet.set_column(int(idx), int(idx), min(col_width, 50), workbook.add_format(column_formats.get(col)))
                else:
                    logger.info(f"\t No Format - Using 'General' format.")
                    worksheet.set_column(int(idx), int(idx), min(col_width, 50), default_format)

            for col, cond_format in conditional_formats.items():
                logger.info('CONDITIONAL FORMAT')
                col_idxs = {col: i for i, col in enumerate(df.columns) }
                col_letters = [chr(i) for i in range(65, 91)]

                idx = col_idxs.get(col)
                letter = col_letters[idx]
                r_start = 2
                r_end = len(df)+1

                col_notation = f'{letter}{r_start}:{letter}{r_end}'

                logger.info(f'\tApply to: {col_notation} (column: {col})')
#                c_format = workbook.add_format(cond_format.get('format'))
#                cond_format['format'] = c_format

                if cond_format.get('type')=='formula':
                    criteria_parts = cond_format.get('criteria_parts')
                    fcol_idx = col_idxs.get(criteria_parts.get('col'))
                    fcol_letter = col_letters[fcol_idx]
                    fcol_remainder = criteria_parts.get('remainder')
                    formula_criteria = f'=${fcol_letter}{r_start}{fcol_remainder}'
                    logger.info(f'\t {cond_format.get("type").upper()} -- Criteria: {formula_criteria} (on column: {criteria_parts.get("col")})')

                    worksheet.conditional_format(col_notation,  #'A1:A4',
                            {'type': cond_format.get('type'),
                             'criteria': formula_criteria,
                             'format': workbook.add_format(cond_format.get('format'))})
                else:
                    logger.info(f'\t {cond_format.get("type").upper()} -- Criteria: {cond_format.get("criteria")} {cond_format.get("value")}')

                    worksheet.conditional_format(col_notation,  #'A1:A4',
                            {'type': cond_format.get('type'),
                             'criteria': cond_format.get('criteria'),
                             'value': cond_format.get('value'),
                             'format': workbook.add_format(cond_format.get('format'))})
        writer.save()
    except Exception:
        logger.exception("Error generating Excel file", exc_info=True)
        raise
    return "complete"





def build_custom_sheets(sheet_maps, writer):
    def write_header_section(worksheet, start_row, header_list, header_formats={}, default_merge=None, default_format=None):
        for x in range(0, len(header_list)):
            height, format_def, merge  = header_formats.get(x) or (None, None, None)
            if not format_def and default_format:
                format_def = default_format
            if not merge and default_merge:
                merge = default_merge
            if merge:
                merge_cols = merge
                cell_range = xl_range( (start_row+x), merge_cols[0],
                                       (start_row+x), merge_cols[1])

                worksheet.merge_range(cell_range,
                                      data=header_list[x],
                                      cell_format=format_def)
            else:
                worksheet.write_string(row=(start_row+x),
                                       col=0,
                                       string=header_list[x],
                                       cell_format=format_def)
            if height:
                worksheet.set_row(row=(start_row+x),
                                  height=height)
        return worksheet

    def define_header_formats(workbook, format_specs=[]):
        header_row_formats = {}
        for spec in format_specs:
            if spec.get('format'):
                fmt_object = workbook.add_format(spec.get('format'))
            else:
                fmt_object = None
            for i in spec.get('rows'):
                header_row_formats[i] = (spec.get('height'),
                                          fmt_object,
                                          spec.get('merges'))
        return workbook, header_row_formats

    # ----- START -----
    workbook_obj = writer.book
    table_head_fmt = workbook_obj.add_format({'align': 'center',
                                              'bold': True,
                                              'font_size': 12,
                                              'border': 1})
    table_row_fmt = workbook_obj.add_format({
                        'align': 'left',
                        'valign': 'vcenter',
                        'font_size': 11,
                        'text_wrap': True
                    })


    for sheet_i, custom_sheet_map in enumerate(sheet_maps):
        column_widths = {}

        row_offset = 0

        sheet_name = custom_sheet_map.get('sheet_name') or ("Summary" if len(sheet_maps)==1 else 'Summary_{}'.format(sheet_i+1))
        table_dfs = custom_sheet_map.get('table_df_list') or []
        #table_nms = custom_sheet_map.get('table_name_list') or []
        table_headers = custom_sheet_map.get('table_headers_list') or []
        table_formats = custom_sheet_map.get('table_formats') or []
        sheet_headers = custom_sheet_map.get('sheet_headers') or {}
        header_formats = custom_sheet_map.get('header_formats') or []

        start_loc = 0
        if len(sheet_headers)>0:
           start_loc += len(sheet_headers) + 2

        ## ADD DATAFRAMES AS TABLES
        for df, headers in zip(table_dfs, table_headers):
            for idx, col in enumerate(df):  # loop through all columns
                if not column_widths.get(str(idx)):
                    column_widths[str(idx)] = calculate_column_width(df[col])+3
                else:
                    column_widths[str(idx)] = max(calculate_column_width(df[col])+3, column_widths.get(str(idx)))

            start_loc += len(headers)
            #table_locs[table_name] = start_loc
            df.to_excel(writer,
                        sheet_name=sheet_name,
                        startrow=start_loc,
                        header=False,
                        index=False)

            start_loc += len(df.index)+3

        worksheet = writer.sheets[sheet_name]

        # header rows at top of sheet
        if len(sheet_headers)>0:
            workbook_obj, row_formats = define_header_formats(workbook_obj,
                                                            format_specs=header_formats)

            worksheet = write_header_section(worksheet,
                                             start_row=row_offset,
                                             header_list=sheet_headers,
                                             header_formats=row_formats)

            row_offset += len(sheet_headers) + 2



        #For each dataframe --- Write/format headers
        for df, headers, formats in zip(table_dfs, table_headers, table_formats):
            row_len = len(df.index)
            col_len = len(df.columns)

            workbook_obj, row_formats = define_header_formats(workbook_obj,
                                                        format_specs=formats)
            # Add Chart Titles
            worksheet = write_header_section(worksheet,
                                             start_row=row_offset,
                                             header_list=headers,
                                             header_formats=row_formats,
                                             default_merge=(0, col_len-1),
                                             default_format=table_head_fmt)
            row_offset += len(headers)


                        ### added for Quiz Times... is not adaptable
            if table_row_fmt:
                for x in range(row_len):
                    worksheet.set_row(row=(row_offset+x),
                                      height=None,
                                      cell_format=table_row_fmt)

            # Cell range for table
#            cell_range = xl_range(row_offset, 0,
#                                  row_offset+row_len-1, col_len-1)
#            # Add table to worksheet
#            worksheet.add_table(cell_range, {'data': df.values,
##                                             'columns': [{'header': 'test a', 'format': table_head_fmt},
##                                                         {'header': 'test b'},
##                                                ],
#                                             'style': '',
#                                             'header_row': False,
#                                            'autofilter': False,
#                                            #'columns': column_names,
#                                            #'first_column': True,
#                                            })
#

            # Update offset value
            row_offset += (row_len+3)

        for idx, c_width in column_widths.items():
            worksheet.set_column(int(idx), int(idx), min(c_width, 125))
    #print(col_formats)
    return writer #(worksheet, col_formats)


