from setuptools import setup, find_packages

__version__ = '1.0.0'

setup(
    name='course_improvement_report', 
    version=__version__, 
    packages=find_packages(), 
    install_requires=['python-dotenv', 'pytz', 'pandas', 'numpy', 'XlsxWriter',
        'psycopg2', 'SQLAlchemy', 'sshtunnel', 'beautifulsoup4', 'lxml', 'chardet', 'pyyaml']
)
