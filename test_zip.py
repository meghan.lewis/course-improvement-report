import zipfile
import re
import os
from os.path import basename
import argparse

from utilities import utils
from utilities.create_logger import initialize_logger
from utilities import emailmodule as emails

ENV = 'dev'

compression = zipfile.ZIP_DEFLATED


def script_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-e", "--env", help="run app in environment")
    parser.add_argument("-db", "--database_name", help="name of database to query")
    parser.add_argument("-t", "--term_id", help="SIS id of term to include data from")
    parser.add_argument("-i", "--include_no_content_courses", help="boolean - include courses without content items")
    return parser.parse_args()


# if __name__ == 'main':
global env, args, working_dir, app_config, logger

working_dir = os.path.dirname(os.path.abspath(__file__))


args = script_args()
env = args.env or ENV or 'dev'

logger = initialize_logger(location=['STDOUT', working_dir+'/logs/'], script_name='test_zip') 
logger.info("created")


full_config = utils.load_yaml_config(config_path=os.path.join(working_dir,'config'), config_file='config.yaml')
app_config = full_config['environments'][env]




working_dir = os.path.dirname(os.path.abspath(__file__))

sql_directory = utils.create_directory(working_dir, app_config['sql_directory'])
reports_directory = utils.create_directory(working_dir, app_config['report_directory'])
reports_archive_directory = utils.create_directory(working_dir, app_config['report_archive_directory'])



date_match = utils.current_date()


REGEX_report_name = r'\w+\s\-\s(?P<term>(?:\d{6}|Multiple))\s\-\s(?P<date>\d{4}\-\d{2}\-\d{2}).xlsx'


files_to_zip = {}

# Iterate over all the files in directory
for folderName, subfolders, filenames in os.walk(reports_directory):
    for filename in filenames:
        if re.search(REGEX_report_name, filename):
            name_match = re.search(REGEX_report_name, filename)
            if name_match['date'] == date_match:
                files_to_zip[name_match['term']] = (files_to_zip.get(name_match['term']) or []) + [filename]
            #create complete filepath of file in directory
           # filePath = os.path.join(folderName, filename)



for term_id, files_list in files_to_zip.items():
    ZIP_NAME = "Reports - {term} - {date}.zip".format(term=term_id, date=date_match)
    utils.delete_file_if_exists(reports_archive_directory, ZIP_NAME)
    # Create a ZipFile Object
    print(f"Zipping files to '{reports_archive_directory}':")
    with zipfile.ZipFile(os.path.join(reports_archive_directory, ZIP_NAME), 'w') as zipObj:
        for filename in files_list:
            print(f"\t {filename}")
            #create complete filepath of file in directory
            filePath = os.path.join(reports_directory, filename)
            # Add file to zip
            zipObj.write(filePath, basename(filePath), compress_type=compression)
    print(f"Zipped to {reports_archive_directory}/{ZIP_NAME}")

print(app_config['email']['recipients'])

# emails.configure_email(sender='meghan.lewis@usu.edu', 
#                         recipients=app_config['email']['recipients'], 
#                         directory=reports_archive_directory, 
#                         file_list=[ZIP_NAME], 
#                         subject=app_config['email']['subject'], 
#                         email_content=app_config['email']['message_text'])