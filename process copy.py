

### --------------------------------------- TO DO --------------------------------------------- ###
    # YES         ---- 1. Check for WYSIWYG links in MODULE ITEMS
    # YES? check? ---- 2. Confirm checking external URLs in Module Items  (only external tools - what happened to external URLs?)
    # YES...      ---- 3. Further testing of youtube links
    # YES         ---- 4. Fill NA - zeroes
    # 5. Broken links to new report ??
    # 6. FOR CONTENT LINKED IN MULTIPLE PLACES -- (just for content report?)
    #       SORT BY "content_state_with_link" & "content_type_with_link" -- AND SELECT TOP MATCH ONLY ???
### --------------------------------------- ----- --------------------------------------------- ###



# In[]:
# import importlib
# importlib.reload(my_module)


# importlib.reload(exl_writer)
# In[]:
version = 12




# In[]:
import datetime
import re
import pandas as pd
import numpy as np
from pathlib import Path
from bs4 import UnicodeDammit

from urllib.parse import urlparse
import os
# from bs4 import BeautifulSoup
# from pandas import ExcelWriter
# from xlsxwriter.utility import xl_range

## Custom Modules
# import connmanager_two as connman
from utilities import utils
from utilities.custom_data import course_processor
from utilities import excel_report_writer as exl_writer
# In[]:
pd.set_option('display.max_colwidth', None)
pd.set_option('display.max_rows', 999)

# In[]:

current_dir = os.getcwd()
print(current_dir)
# In[]:
###
parent_dir = os.path.split(current_dir)[0]
# config_dir = os.path.join(current_dir, 'config')
# data_dir = os.path.join(current_dir, 'data')
sql_dir = os.path.join(current_dir, 'sql')
reports_dir = os.path.join(current_dir, 'reports')
modules_dir = "c:/Users/A00362809/Documents/Python Scripts/helpers"


# In[]:
import sys
sys.path.append(modules_dir)  # Append to system path
from database_access import OLD_database_access as database_access
from create_logger import initialize_logger
from custom_data import canvas_content_processor as cc


# In[]:

# In[]:
logger = initialize_logger(location=['STDOUT', current_dir+'/'], script_name='process') # Path(__file__).stem)
#logger = initialize_logger(location=['STDOUT', current_dir+'/'], script_name=Path(__file__).stem)

# In[]:
# from dotenv import load_dotenv   #pip install python-dotenv
# load_dotenv()
#####  obZYbHC10Z

# In[]:
# create DatabaseAccess object from custom module
dba = database_access.DatabaseAccess()
ssh_server, db_conn = dba.create_db_connection(username="mlewis", password="obZYbHC10Z")




# In[]:
def get_sql_query_from_file(dir, name):
    sql_file_path = os.path.join(dir, f"{name}.sql")
    if not utils.check_file_exists(sql_file_path):
        logger.info(f"SQL file does not exist: {sql_file_path}")
        return None
    return utils.get_file_text(sql_file_path)

def get_db_data(conn, query, parameters={}):
    df = pd.DataFrame()
    #logger.info(f"Executing Database Query")
    try:
        df = pd.read_sql_query(query, db_conn, params=parameters)
        logger.info(f" {len(df)} data rows retrieved")
    except Exception:
        logger.exception("Error querying database", exc_info=True)
    return df

# In[]:
# Files Calculations

def parse_file_extension(file_name):
    regex_extension = r'(\.\w{1,5})$'
    matched = re.search(regex_extension, file_name.lower())
    return matched.group(1) if matched else None

def parse_file_category(file_type):
    category_regexes = {
        'pdf': 'pdf',
        'image': 'image/',
        'video': 'video/',
        'flash': 'flash'
    }
    for cat, reg in category_regexes.items():
        if re.search(reg, file_type.lower()):
            return cat
    return None



# In[2]:
# DATE_TODAY = datetime.date.today()
# DATE_STR = DATE_TODAY.strftime("%Y-%m-%d")
# In[]:
# SQL_REGEX_LIST = [r'files/[[:digit:]]+']
# PYTHON_REGEX_LIST = [r'files/(?:1009~)?([0-9]+)']
# LINK_REGEX = r'[\'\"]([^\'\"]*/files/(?:1009~)?[0-9]{5,}[^\'\"]*)[\'\"]'
# COURSE_ID_REGEX = r'courses/(?:1009~)?([0-9]+)/files/(?:1009~)?[0-9]{5,}'
# FILE_ID_REGEX = r'/files/(?:1009~)?([0-9]{5,})'

# ## Path to write report
# REPORT_LOC = 'c:/users/a00362809/desktop/'
# REPORT_FILE_PATH = os.path.join(current_dir, REPORT_LOC)

# REPORT_NAME = os.path.join(REPORT_FILE_PATH, 'Test New Files Report - {date}.xlsx')
# In[]:

numeric_types = ['int16', 'int32', 'int64', 'float16', 'float32', 'float64']
# dict_sort_content = {
#     'pages': 0,
#     'assignments': 1,
#     'discussions': 2,
#     'quizzes': 3,
#     'syllabus': 4,
#     'modules': 5,
#     'announcements': 6
# }
dict_sort_content = {
    'pages': 0,
    'assignments': 1,
    'discussions': 2,
    'quizzes': 3,
    'syllabus': 4,
    'modules': 5,
    'announcements': 6,
    'quizzes/questions': 7,
    'quizzes/answers': 8,
    'quizzes/question_comments': 9,
    'quizzes/answer_comments': 10
}

################################################################################################################
################################################################################################################
######################## ------------------------------ QUESTIONS -------------------------------------
## FILL WITH ZEROES OR LEAVE BLANK ?

# In[]:
COLUMN_MAP = {
    0: { 'display_name': 'Course Id', 'data_name': 'course_canvas_id', },
    1: { 'display_name': 'Course', 'data_name': 'course_name', },
    2: { 'display_name': 'College', 'data_name': 'parent_account_name', },
    3: { 'display_name': 'Department', 'data_name': 'account_name', },
    4: { 'display_name': 'Delivery Method', 'data_name': 'delivery_method',},
    5: { 'display_name': 'New Course', 'data_name': '_IGNORE_',}, # Ignore
    6: { 'display_name': 'Term', 'data_name': 'term_name', },
    7: { 'display_name': 'Instructors', 'data_name': 'instructors', },
    8: { 'display_name': 'TidyUp', 'data_name': 'TidyUp',},
    9: { 'display_name': 'Accessibility Report', 'data_name': 'Accessibility Report',},
    10: { 'display_name': 'Number of Students', 'data_name': 'total_students', },
    11: { 'display_name': 'Files Navigation Visible?', 'data_name': 'files_navigation', },
    12: { 'display_name': 'Navigation Items', 'data_name': 'total_navigation_items', },
    13: { 'display_name': 'Files', 'data_name': 'total_files', },
    14: { 'display_name': 'Files with Link', 'data_name': 'total_files_with_link', },
    15: { 'display_name': '% Files in Use', 'data_name': 'percent_files_with_link', },
    16: { 'display_name': 'Files A11y Score', 'data_name': '_IGNORE_',},  # Ignore
    17: { 'display_name': 'WYSIWYG Content', 'data_name': 'total_canvas', },
    18: { 'display_name': 'WYSIWYG Content with Link', 'data_name': 'total_canvas_with_link', },
    19: { 'display_name': '% Canvas Content in Use', 'data_name': 'percent_canvas_with_link', },
    20: { 'display_name': 'WYSIWYG A11y Score', 'data_name': '_IGNORE_',}, # Ignore
    21: { 'display_name': 'Overall Accessibility Score', 'data_name': '_IGNORE_',}, # Ignore
    22: { 'display_name': 'Instructor Communication', 'data_name': '_IGNORE_',}, # Ignore
    23: { 'display_name': 'Notes', 'data_name': '_IGNORE_',}, # Ignore
    24: { 'display_name': 'PDF Files', 'data_name': 'total_pdf',},
    25: { 'display_name': 'PDF Files in Use', 'data_name': 'percent_pdf_with_link',}, 
    26: { 'display_name': "PDF Scanned not OCR'd", 'data_name': '_IGNORE_',}, # Ignore
    27: { 'display_name': 'PDF to HTML Status', 'data_name': '_IGNORE_',}, # Ignore
    28: { 'display_name': 'Images', 'data_name': 'total_image', },
    29: { 'display_name': 'Images in Use', 'data_name': 'percent_image_with_link', },
    30: { 'display_name': 'Images without Alt Text', 'data_name': '_IGNORE_',}, # Ignore
    31: { 'display_name': '% Images Needing Alt Text', 'data_name': '_IGNORE_',}, # Ignore
    32: { 'display_name': 'Images Status', 'data_name': '_IGNORE_',}, # Ignore
    33: { 'display_name': 'Kaltura Videos', 'data_name': 'total_kaltura_links',},
    34: { 'display_name': 'Videos', 'data_name': 'total_video',}, 
    35: { 'display_name': 'YouTube Videos', 'data_name': 'total_youtube_links',},
    36: { 'display_name': 'Caption Status', 'data_name': '_IGNORE_',}, # Ignore
    37: { 'display_name': 'Flash Content', 'data_name': 'total_flash',}, 

    38: { 'display_name': 'Potential Broken Links', 'data_name': 'total_broken', },   ######### TEMPORARY


}

COLUMN_ORDER = [v.get('display_name') for k, v in COLUMN_MAP.items()]

# In[25]:


TOOL_LINK_MAP = {
    'TidyUp': {'content_name': 'TidyUp',  'content_type': 'external_tools', 'content_id': '28805'},
    'Accessibility Report': { 'content_name': 'A11y Report', 'content_type': 'external_tools', 'content_id': '29768'},
}



# df = pd.DataFrame([{v.get('display_name'): 'column_{k}'.format(k=k) for k, v in COLUMN_MAP.items()}])

COURSES_CTE_NAME = "CTE_courses"

QUERIES = {
    'courses': 'courses',
    'course_agg': 'course_aggs',
    'content': ['content_list', 'quiz_question_list'],
    'files': 'file_list'
}

PARAMS = {
    'term_sis_list': ['202140']
}




# In[]:
COURSES_CTE = get_sql_query_from_file(sql_dir, COURSES_CTE_NAME)

# In[25]:
## GET COURSE DATA
QUERY_NAME = QUERIES.get('courses')
logger.info(f"Get data for: {QUERY_NAME}")
query_text = COURSES_CTE + get_sql_query_from_file(sql_dir, QUERY_NAME)
query_params = PARAMS
course_df = get_db_data(db_conn, query_text, parameters=query_params)

# In[25]:
## PROCESS COURSE DATA
course_df['course_id'] = course_df['course_id'].apply(lambda x: None if pd.isnull(x) else str(int(x)))
course_df['course_name'] = course_df['course_name'].apply(lambda x: utils.clean_ascii_string(x))
course_df['delivery_method'] = course_df['course_name'].apply(lambda x: course_processor.CourseProcessor(course_name=x).get_delivery_method())
course_df.head()

# In[10]:
## COURSE AGG DATA
QUERY_NAME = QUERIES.get('course_agg')
logger.info(f"Get data for: {QUERY_NAME}")
query_text = COURSES_CTE + get_sql_query_from_file(sql_dir, QUERY_NAME)
query_params = PARAMS
course_agg_df = get_db_data(db_conn, query_text, parameters=query_params)
course_agg_df.info()

# In[11]:
## PROCESS COURSE AGG DATA
course_agg_df['course_id'] = course_agg_df['course_id'].apply(lambda x: None if pd.isnull(x) else str(int(x)))
course_agg_df.head()

# In[12]:
## GET FILES DATA
QUERY_NAME = QUERIES.get('files')
logger.info(f"Get data for: {QUERY_NAME}")
query_text = COURSES_CTE + get_sql_query_from_file(sql_dir, QUERY_NAME)
query_params = PARAMS
files_df = get_db_data(db_conn, query_text, parameters=query_params)
files_df.info()
# In[13]:
## PROCESS FILES DATA
files_df['course_id'] = files_df['course_id'].apply(lambda x: None if pd.isnull(x) else str(int(x)))
files_df['file_id'] = files_df['file_id'].apply(lambda x: None if pd.isnull(x) else str(int(x)))
files_df['replacement_file_id'] = files_df['replacement_file_id'].apply(lambda x: None if pd.isnull(x) else str(int(x)))

files_df['file_name'] = files_df['file_name'].apply(lambda x: utils.clean_ascii_string(x))
files_df['file_type'] = files_df['file_type'].apply(lambda x: utils.clean_ascii_string(x))

files_df['file_extension'] = files_df['file_name'].apply(lambda x: parse_file_extension(x))
files_df['file_category'] = files_df['file_type'].apply(lambda x: parse_file_category(x))
files_df.head()
#  %%
# files_df.head()

# # %%
# files_df.file_state.value_counts()
# # %%
# files_df.file_category.value_counts()
# %%

# %%
## GET CONTENT DATA

content_df_list = [] 
for QUERY_NAME in QUERIES.get('content'): 
    logger.info(f"Get data for: {QUERY_NAME}")
    query_text = COURSES_CTE + get_sql_query_from_file(sql_dir, QUERY_NAME)
    query_params = PARAMS
    df = get_db_data(db_conn, query_text, parameters=query_params)
    content_df_list.append(df)

content_df = pd.concat(content_df_list).reset_index(drop=True)
content_df.info()

# %%
## PROCESS CONTENT DATA
content_df['course_id'] = content_df['course_id'].apply(lambda x: None if pd.isnull(x) else str(int(x)))
content_df['content_text'] = content_df['content_text'].apply(lambda x: cc.clean_canvas_html_content(x))

content_df.head()

# %%
###--------------------------- STARTING HERE : NEW PROCESSING FIND ALL LINKS IN CONTENT -- WORK IN PROGRESS -------------------------------------------------
# def convert_to_unicode(text):
#     if text:
#         converted = UnicodeDammit(text)
#     # if not converted.unicode_markup:
#     #     raise UnicodeDecodeError('Failed to detect encoding, tried [%s]')
#     # else:
#         return converted
#     else:
#         return None

#     #converted.original_encoding
# %%
# content_df['content_text'] = content_df['content_text'].apply(lambda x: convert_to_unicode(x))

# %%
logger.info(f"Parse links from content")
## COPY ALL CONTENT THAT HAS "content_text"
content_text_df = content_df.loc[content_df['content_text'].notnull(), 
                                ['course_id', 'content_type', 'content_id', 'content_text', 'content_state']].copy(deep=True)
# %%
## PARSE ALL URLS FROM "content_text"
content_text_df['contained_links'] = content_text_df['content_text'].apply(lambda x: cc.parse_links_from_content(x, include_raw_text_links=False))

# %%
content_text_df['contained_links'].head()

# %%
content_text_df.head()
# %%
## PIVOT URLS PARSED FROM "content_text" -- ONE URL PER ROW
content_links_df = pd.DataFrame(content_text_df.contained_links.tolist(),
                                index=[content_text_df.course_id, content_text_df.content_type, 
                                        content_text_df.content_id, content_text_df.content_state,]
                            ).stack()
content_links_df = content_links_df.reset_index()[[0,'course_id', 'content_type','content_id','content_state',]] 

content_links_df.columns = ['url_from_text','course_id','content_type','content_id','content_state',]

content_links_df = content_links_df.loc[content_links_df['url_from_text'].str.len()>0].drop_duplicates().copy(deep=True)
# %%
content_links_df.info()

# %%

# %%
# %%

def check_url_http_syntax(url):
    pattern_good_http = 'https?://[^\s]'
    pattern_malformed_http = 'https?\:?/*\s*'

    REG_http_multi_match = '(' + pattern_malformed_http + '){2,}'
    REG_http_extract = '((?:' + pattern_malformed_http + ')+)'

    has_error = False
    # Starts with 1+ http
    if re.match(REG_http_extract, url):
        # Starts with multiple http(s) or malformed http(s)
        if re.match(REG_http_multi_match, url) or not re.match(pattern_good_http, url):
            has_error = True
        if 'https' in re.match(REG_http_extract, url).group(1):
            prefix = 'https://'
        else:
            prefix = 'http://'
        return has_error, re.sub(REG_http_extract, prefix, url)
    # Starts with // or contains web address (i.e. NOT internal path)
    elif re.match('//', url) or re.search('(www\.|\.(com|org|edu|gov))', url):
        return has_error, 'http://' + url.lstrip('/')
    else:
        return has_error, url



def process_text_url(url):
    BROKEN_LINK_REGEX_LIST = [ 'LINK\.PLACEHOLDER',
                               '(?:(?:\$|%24)[A-Z_]+(?:\$|%24))',
                               '.*\$CANVAS_OBJECT_REFERENCE\$']
    VALID_SCHEMES = ['http', 'https', 'ftp']
    internal_first_paths = [
        'appointment_groups', 'assessment_questions',  'assignments', 'calendar', 'conversations', 
        'courses', 'enroll', 'eportfolios', 'equation_images', 'files', 'groups',  'images',
        'login', 'media_objects', 'media_objects_iframe',  'pages', 'profile', 'search', 'users',

        'modules_items', 'external_tools', 'quizzes', 'grades', 'gradebook', 
        'collaborations', 'conferences', 'discussion_topics',
    ]
    internal_paths_regex = '^((/?api/v1)|(#))'

    if not url:
        return pd.Series([None, True, None, None])
    elif any([re.match(REG_broken, url) for REG_broken in BROKEN_LINK_REGEX_LIST]):
        return pd.Series([True, True, None, None])

    
    process_url = url.encode('ascii', errors='ignore').decode('ascii').lower()
    has_error, process_url = check_url_http_syntax(process_url)

    is_internal = None
    parts = urlparse(process_url, allow_fragments=False)
    parsed_path = parts.path.strip().strip('/')
    if re.search('#', parsed_path) and not re.match('#', parsed_path):
        parsed_groups = re.search('([^#]+)(#.*)', parsed_path).groups()
        parsed_path = parsed_groups[0]
        parsed_fragment = parsed_groups[1]

    path_split = [x for x in parsed_path.split('/') if len(x)>0] if parsed_path else []
    if parts.scheme and parts.scheme not in VALID_SCHEMES:
        has_error = True
    if not parts.netloc and len(path_split) == 0:
        link_type = None
        has_error = True
    if parts.netloc and parts.netloc != 'usu.instructure.com':
        is_internal = False
    elif not parts.netloc and len(path_split) > 0:
        is_internal = True
        if re.search('(www\.|\.(com|org|edu|gov))', parsed_path):
            is_internal = False
            has_error = True
        elif any([True if x.find(' ')>-1 else False for x in path_split]) and path_split[0] not in ['courses','files']:
            ## if url link contains a space (and is not a course file)
            has_error = True
        elif not any([re.match(internal_paths_regex, parsed_path), path_split[0] in internal_first_paths]):
            has_error = True        

    return pd.Series([is_internal, 
                        has_error, 
                        (parts.netloc.strip() or None), 
                        (parts.path.strip() or parts.fragment.strip() or None) 
                    ])
                        # parts.params.strip() or None, 
                        # parts.query.strip() or None, 
                        # parsed_fragment,
                        # parts.scheme.strip() or None, 
# %%
# content_links_df[['is_internal_url', 'has_url_error', 
#                 'scheme', 'netloc', 'path', 'params', 'query', 'fragment']] = content_links_df['url_from_text'].apply(lambda x: process_text_url(x))

logger.info(f"Clean/split URLs in content")
content_links_df[['is_internal_url', 'has_url_error', 'netloc', 'path']] = content_links_df['url_from_text'].apply(lambda x: process_text_url(x))
# %%
content_links_df.head()
# %%

# %% 

# %%

# %%

# %%

logger.info(f"Match links to WYSIWYG content")

### CHECK FOR LINKS TO WYSIWYG ITEMS WITHIN CONTENT
WYSIWYG_CONTENT_TYPES = [t for t in content_df.content_type.unique() if t.find('/')==-1 and t != 'modules']

#non_wysiwyg_content_df = content_df.loc[~content_df['content_type'].isin(WYSIWYG_CONTENT_TYPES)].copy(deep=True)
wysiwyg_content_df = content_df.loc[content_df['content_type'].isin(WYSIWYG_CONTENT_TYPES)].copy(deep=True)


# %%
REGEX_CONTENT_TYPES = '/?(?:' + '|'.join(WYSIWYG_CONTENT_TYPES) + ')/[^/#&\?]+' 
REGEX_CONTENT_TYPES

content_url_paths = content_links_df.loc[(content_links_df['is_internal_url']==True) 
                                        & (content_links_df["path"].str.contains(REGEX_CONTENT_TYPES, regex=True))
                                    ].copy(deep=True).reset_index(drop=True)

content_url_paths.shape


# %%
#REGEX_EXTRACT = '((?:/?courses/\d+)?' + REGEX_CONTENT_TYPES + ')'

REGEX_EXTRACT = '(/(?:' + '|'.join(WYSIWYG_CONTENT_TYPES) + ')/(?:syllabus|[\d]+|(?<=pages/)[^/#&\?]+))(?:[/#&\?]|$)'

split_paths = pd.DataFrame(content_url_paths['path'].apply(lambda x: [] if not re.search(REGEX_EXTRACT, x) else re.search(REGEX_EXTRACT, x).group(1).strip('/').split('/')).tolist(), 
                            index=content_url_paths.index)


split_paths.rename(columns={c:f"link_path_{c}" for c in split_paths.columns}, inplace=True)

split_paths.info()

# %%
#REGEX_EXTRACT = '(' + REGEX_CONTENT_TYPES + ')'






# %%

content_url_paths = pd.merge(content_url_paths, 
                            split_paths, 
                            left_index=True, 
                            right_index=True)

content_url_paths.rename(columns={"content_type": "content_type_with_link",
                                    "content_id":	"content_id_with_link",
                                    "content_state": "content_state_with_link"}, inplace=True)
content_url_paths.info()
# %%
# %%
## CHECK IF ANY LINKS DIDN'T PARSE FROM REGEX
content_url_paths.loc[content_url_paths['link_path_0'].isnull(), ['url_from_text']]
# %%
merged_wysiwyg = pd.merge(wysiwyg_content_df,
                            content_url_paths,
                            how='left',
                            left_on=['course_id','content_type','content_id'],
                            right_on=['course_id','link_path_0','link_path_1']
        )

merged_wysiwyg['content_is_linked'] = merged_wysiwyg['link_path_0'].apply(lambda x: 1 if pd.notnull(x) else 0)
# %%
merged_wysiwyg.info()


### --------------------------------------- TO DO --------------------------------------------- ###
## FOR CONTENT LINKED IN MULTIPLE PLACES -- SORT BY "content_state_with_link" & "content_type_with_link"
### SELECT TOP MATCH ONLY
### --------------------------------------- ----- --------------------------------------------- ###

# %%
merged_wysiwyg.content_type_with_link.value_counts()


# %%
GROUP_COLS = list(wysiwyg_content_df.columns)
AGG_COL = 'content_is_linked'

merged_wysiwyg = merged_wysiwyg[GROUP_COLS + [AGG_COL]].fillna('-').groupby(GROUP_COLS, as_index=False)[AGG_COL].max()
# %%
merged_wysiwyg.info()

# %%
logger.info(f"Total Links to Content Items")
content_totals = merged_wysiwyg.groupby(['course_id']).agg(total_canvas=('content_id','count'), 
                                                            total_canvas_with_link=('content_is_linked','sum')).reset_index()

content_totals['percent_canvas_with_link'] = content_totals.apply(lambda x:  x['total_canvas_with_link']/x['total_canvas'] if x['total_canvas']>0 else np.nan, axis=1 )
content_totals

###--------------------------- ENDING HERE : NEW PROCESSING FIND ALL LINKS IN CONTENT -- WORK IN PROGRESS -------------------------------------------------

# %%

logger.info(f"Total Broken Links")
broken_content_links = content_links_df[(content_links_df['has_url_error']==True)].copy(deep=True)

broken_totals = broken_content_links.groupby('course_id').agg(total_broken=('url_from_text','count')).reset_index()

broken_totals.head(10)


# %%

# %%
broken_content_links[['course_id', 'content_type', 'content_id',]].drop_duplicates()

# %%
broken_content = broken_content_links.groupby(['course_id', 'content_type', 'content_id',
       'content_state'], as_index=False).agg(link_list=('url_from_text',list))

# %%
broken_content['total_broken_links'] = broken_content['link_list'].apply(lambda x: len(x or []))


broken_content.info()
# %%
broken_content['joinable_content'] = broken_content['content_type'].apply(lambda x: x[:x.find('/')] if x.find('/')>-1 else x)


# %%
broken_totals['total_broken'].sum()

# %%

broken_content_links.info()
# %%

# %%
# %%
broken_content = pd.merge(broken_content, 
                            content_df[['course_id','content_type','content_id','content_name', 'content_created_at', 'content_updated_at']].drop_duplicates(), 
                            left_on=['course_id','joinable_content','content_id'],
                            right_on=['course_id','content_type','content_id'],
                            suffixes=['','_y'])

# %%   
broken_content.info()


# %%
def extract_text(text, regex_list=[]):
    files = []
    if len(regex_list) == 0:
        print("Must provide list of regular expression(s)")
    for reg in regex_list:
        files += re.findall(reg, text, re.IGNORECASE)
    return files
 # %%
# %%

# %%
# PARSING OF FILES LINKED IN CONTENT
# POSSIBLY REPLACE THIS WITH FULL PARSING OF LINKS IN CONTENT (IN-PROGRESS ABOVE)
# %%
# FILE_REGEX_LIST = [r'files/(?:1009~)?([0-9]+)']


# file_content_links_df.shape
# %%

#file_content_links_df['files'] = file_content_links_df['path'].apply(lambda x: extract_text(x, FILE_REGEX_LIST) if pd.notnull(x) else [])
# %%
logger.info(f"Parse Links to Files from Content")
FILE_REGEX = 'files/(\d+)(?:[/#&\?]|$)'
file_content_links_df = content_links_df.loc[(content_links_df['path'].notnull()) 
                                            & (content_links_df['path'].str.contains('files'))].copy(deep=True)
file_content_links_df['linked_file_id'] = file_content_links_df['path'].apply(lambda x: re.search(FILE_REGEX, x).group(1) if pd.notnull(x) and re.search(FILE_REGEX, x) else None)
# %%
file_content_links_df.shape
# %%
pivot_files = pd.pivot_table(file_content_links_df, 
                            index=['linked_file_id'], 
                            columns=['content_type'], 
                            values='content_id',
                            aggfunc='count', 
                            fill_value=0)

# %%
#pivot_files.columns = pivot_files.columns.droplevel(0)
pivot_files.columns.name = None
# %%
content_type_list = list(pivot_files.columns)
# for col in :
#     content_type_list.append(col)
print(content_type_list)

# In[]:
pivot_files['total_links_to_file'] = pivot_files.sum(axis=1)
pivot_files['file_is_linked'] = pivot_files['total_links_to_file'].apply(lambda x: x>0)
# In[85]:
pivot_files.reset_index(inplace=True)

# %%
pivot_files
# %%
logger.info(f"Join File links to Files data")

merged_files_data = pd.merge(files_df, 
                            pivot_files, 
                            how='left', 
                            left_on=['file_id'],
                            right_on=['linked_file_id'])

merged_files_data.drop_duplicates(inplace=True)
merged_files_data.info()

# %%
### -------------------- CHECK FOR LINKS TO REPLACEMENT FILE IDS --------------------

merged_replacement_data = pd.merge(files_df, 
                                    pivot_files, 
                                    left_on=['replacement_file_id'], 
                                    right_on=['linked_file_id'])
merged_replacement_data.info()

# In[90]:

measure_cols = content_type_list + ['total_links_to_file', 'file_is_linked']

for c in measure_cols:
    merged_files_data[c].fillna(0, inplace=True)


# %%
#merged_data['file_is_linked'] = merged_data['file_is_linked'].astype(bool)
merged_files_data['file_is_linked'] = merged_files_data['file_is_linked'].astype(np.int64)
merged_files_data[content_type_list] = merged_files_data[content_type_list].astype(np.int64)
merged_files_data['total_links_to_file'] = merged_files_data['total_links_to_file'].astype(np.int64)


# %%
# check for duplicate file_id's
checks = merged_files_data.groupby('file_id')['course_id'].count().reset_index()
checks.loc[checks['course_id']>1]


# %%
logger.info(f"Total Files and Links")
file_totals = merged_files_data.groupby(['course_id']).agg(total_files=('file_id','nunique'), 
                                                            total_files_with_link=('file_is_linked','sum')).reset_index()

file_totals['percent_files_with_link'] = file_totals.apply(lambda x:  x['total_files_with_link']/x['total_files'] if x['total_files']>0 else np.nan, axis=1 )
file_totals


# %%
logger.info(f"Parse number of Files of various types")
df_list = []
for file_category in merged_files_data.loc[merged_files_data['file_category'].notnull()]['file_category'].unique():
    category_df = merged_files_data.loc[merged_files_data['file_category']==file_category].copy(deep=True)
    if len(category_df) > 0:
        category_df = category_df.groupby(['course_id']).agg(total=('file_id','nunique'), 
                                                    with_link=('file_is_linked','sum')).reset_index()
        category_df['percent_with_link'] = category_df.apply(lambda x: (x['with_link']/x['total']) if x['total']>0 else np.nan, axis=1 )

        category_df.rename(columns={"total": f"total_{file_category}",
                                "with_link": f"total_{file_category}_with_link",
                                "percent_with_link": f"percent_{file_category}_with_link"
                                }, inplace=True)

        df_list.append(category_df)

# %%
for category_df in df_list:
    file_totals = pd.merge(file_totals, 
                            category_df, 
                            how='left', 
                            on='course_id')

# %%
file_totals.info()

# %%

# %%

# %%

# In[7]:
### ------------------------------- KALTURA ----------------------------------------------------------

# def parse_kaltura_links(link):
#     """
#         Parses links to kaltura videos and IDs of kaltura player objects
#         from the HTML of Canvas content items

#         Returns:
#             - Total # of kaltura links found in the content text
#             - List of kaltura links
#             - List of player IDs
#     """
#     REGEX_LIST_kaltura_players = [r'/uiconf[\_]?id/([0-9]+)',
#                               r'/playerskin/([0-9]+)',
#                               r'kaltura.com/(tiny)',
#                               r'=(kalturaviewer)',
#                               r'(mediaspace)\.kaltura.com']

#     #REGEX_js_kaltura_settings = '<script>.*kaltura.*</script>'

#     links,players=[],[]
#     try:
#         # Parse HTML content
#         parsed_html = BeautifulSoup(html_text_content, "html.parser")

#         # Search all hyperlinks and iframe links for 'kaltura'
#         for a in parsed_html.findAll('a', href=True):
#             if re.search(r'kaltura', a['href'], re.IGNORECASE):
#                 links.append(a['href'])
#         for i in parsed_html.findAll('iframe', src=True):
#             if re.search(r'kaltura', i['src'], re.IGNORECASE):
#                 links.append(i['src'])

#         # For every 'kaltura' link found, attempt to parse a player ID
#         for link in links:
#             link=link.lower()
#             for REGEX in REGEX_LIST_kaltura_players:
#                 if re.search(REGEX, link, re.IGNORECASE):
#                     players.append(re.search(REGEX, link, re.IGNORECASE).group(1))
#                     break
#     except Exception:
#         logger.exception("Error parsing links from content", exc_info=True)

#     return pd.Series([len(links), links, players])

# %%

KALTURA_REGEX_LIST = [
    r'/uiconf[\_]?id/([0-9]+)',
    r'/playerskin/([0-9]+)',
    r'kaltura.com/(tiny)',
    r'=(kalturaviewer)',
    r'(mediaspace)\.kaltura.com'
]


kaltura_content_df = content_links_df.copy(deep=True)
kaltura_content_df.shape

# %%
kaltura_content_df['players'] = kaltura_content_df['url_from_text'].apply(lambda x: extract_text(x, KALTURA_REGEX_LIST) if pd.notnull(x) else [])

kaltura_content_df = kaltura_content_df[ kaltura_content_df.apply(lambda x: len(x['players'])>0, axis=1)].reset_index(drop=True)
# %%
kaltura_player_df = pd.DataFrame(kaltura_content_df.players.tolist(),
                                index=[kaltura_content_df.course_id, 
                                        kaltura_content_df.content_type, 
                                        kaltura_content_df.content_id]
                                ).stack()

kaltura_player_df = kaltura_player_df.reset_index()[[0,'course_id', 'content_type','content_id']] #'content_type','content_id','content_name','content_state',
kaltura_player_df.columns = ['kaltura_player_id','course_id', 'content_type','content_id']
kaltura_player_df.shape

# %%

# %%



# %%                
# logger.info("Parse Kaltura Players from Content...")
# ## Parse lists of kaltura links and player IDs from HTML content
# data[['total_kaltura_links','links','players']] = data['content_text'].apply(lambda x: parse_kaltura_links(x))

# ## Many content items have multiple players -- unstack data to separate rows
# content_players = utils.split_dataframe_list(data[['course_id','content_id','players']],'players','array')
# logger.info("\t (%d player references found)" % len(content_players))

# %%
## Pivot 'content_players' DataFrame, to set player ID's as columns headers,
## rather than row values AND count number of each player ID per content item
pivot_players = pd.pivot_table(kaltura_player_df,
                               index=['course_id','content_type','content_id'],
                               columns=['kaltura_player_id'],
                               aggfunc=len,
                               fill_value=0)
pivot_players.head()


# %%
pivot_players.info()



# %%
pivot_players.columns.name = None
pivot_players.reset_index(inplace=True)



# %%
### FOR Kaltura total in 'Course Summary'
pivot_players['total_kaltura_links'] = pivot_players.sum(axis=1)

kaltura_totals = pivot_players.groupby('course_id', as_index=False)['total_kaltura_links'].sum()





### ------------------------------- END KALTURA ----------------------------------------------------------

# In[]:
kaltura_totals.head()

# %%
# courses[['term_sis_id', 'term_name', 
#           'course_id', 'course_canvas_id', 'course_sis_id', 'course_name', 
#           'course_state', 'instructors']].drop_duplicates()

# %%
# pivot_players.sum()
# pivot_players.sum().sum()


## -------------------------------------- NOTE -----------------------------------------------
### USE PIVOT PLAYERS FOR KALTURA REPORT
#pivot_players

# Create list with each player ID (later used for formatting)  -- OLD
# PLAYER_LIST = list(pivot_players.columns)
# PLAYER_LIST


# kaltura_per_content = kaltura_content_df.groupby(['course_id','content_type','content_id'], as_index=False)['url_from_text'].agg(list)

# kaltura_per_content = pd.merge(kaltura_per_content,
#                                 pivot_players,
#                                 on=['course_id','content_type','content_id'])

# kaltura_per_content.to_excel("Kaltura Test.xlsx")



# %%


# %%



# %%




# %%

YOUTUBE_REGEX_LIST = [r'(.*youtu\.?be.*)']


youtube_content_df = content_links_df.loc[(content_links_df['is_internal_url']==False) & (content_links_df['netloc'].notnull())].copy(deep=True)
youtube_content_df.shape
# %%
youtube_content_df['total_youtube_links'] = youtube_content_df['netloc'].apply(lambda x: len(extract_text(x, YOUTUBE_REGEX_LIST)))
youtube_content_df = youtube_content_df.loc[youtube_content_df['total_youtube_links']>0].copy(deep=True)
youtube_content_df.shape
# %%
youtube_totals = youtube_content_df.groupby('course_id', as_index=False)['total_youtube_links'].sum()
youtube_totals
# %%

# %%

# %%

# %%

# %%

# %%

# %%

# %%

# %%

# %%

# %%

# %%
# %%
# %%
# %%
# %%
# %%
# %%
# %%
# %%

# %%
courses = pd.merge(course_df, 
                    course_agg_df, 
                    on='course_id', 
                    how='left')
# %%
for col in course_agg_df.select_dtypes(include=['int64', 'float64']).columns:
    print(col)
    courses[col] = courses[col].fillna(0)


# %%
courses = pd.merge(courses, 
                    content_totals, 
                    on='course_id', 
                    how='left')
# %%
for col in content_totals.select_dtypes(include=['int64', 'float64']).columns:
    print(f"Fill NA: {col}")
    courses[col] = courses[col].fillna(0)
    # if col.startswith('total_'):
    #     print(f"Integer: {col}")
    #     courses[col] = courses[col].astype('int64')


# %%
courses = pd.merge(courses, 
                    file_totals, 
                    on='course_id', 
                    how='left')
# %%
for col in file_totals.select_dtypes(include=['int64', 'float64']).columns:
    print(f"Fill NA: {col}")
    courses[col] = courses[col].fillna(0)
    # if col.startswith('total_'):
    #     print(f"\tInteger: {col}")
    #     courses[col] = courses[col].astype('int64')
# %%
courses = pd.merge(courses, 
                    kaltura_totals, 
                    on='course_id', 
                    how='left')
# %%
for col in kaltura_totals.select_dtypes(include=['int64', 'float64']).columns:
    print(f"Fill NA: {col}")
    courses[col] = courses[col].fillna(0)
    # if col.startswith('total_'):
    #     print(f"\tInteger: {col}")
    #     courses[col] = courses[col].astype('int64')

# %%
courses = pd.merge(courses, 
                    youtube_totals, 
                    on='course_id', 
                    how='left')

# %%
for col in youtube_totals.select_dtypes(include=['int64', 'float64']).columns:
    print(f"Fill NA: {col}")
    courses[col] = courses[col].fillna(0)
    # if col.startswith('total_'):
    #     print(f"\tInteger: {col}")
    #     courses[col] = courses[col].astype('int64')


# %%
### ---- MAYBE ADD THIS ??? WOULD NEED TO ADD COLUMN TO MAP AT BEGINNING ------

courses = pd.merge(courses, 
                    broken_totals, 
                    on='course_id', 
                    how='left')

# %%
for col in broken_totals.select_dtypes(include=['int64', 'float64']).columns:
    print(f"Fill NA: {col}")
    courses[col] = courses[col].fillna(0)
    # if col.startswith('total_'):
        # print(f"\tInteger: {col}")
        # courses[col] = courses[col].astype('int64')



# %%
# courses.to_csv("temp_courses.csv", index=False, sep='\t')
# courses = pd.read_csv("temp_courses.csv", sep='\t')

# %%
courses.info()
# %%
courses.head()

# %%


### ------------------------------------------------------------------------------------------------------
### ------------------------------------------- COURSE SUMMARY -------------------------------------------
### ------------------------------------------------------------------------------------------------------


# In[25]:

all_data = courses.loc[(courses['total_canvas']>0) | (courses['total_files']>0)].copy(deep=True)


column_formats = {}
column_renames = {}
for k, v in COLUMN_MAP.items():
    if v.get('display_name') == 'Course' or v.get('display_name') in TOOL_LINK_MAP.keys():
        column_formats[v.get('display_name')] = 'hyperlink'
    elif v.get('data_name') and v.get('data_name').find('total') > -1:
        column_formats[v.get('display_name')] = 'black_integers'
    elif v.get('data_name') and v.get('data_name').find('percent') > -1:
        column_formats[v.get('display_name')] = 'percent'
    # elif v.get('data_name') == '_IGNORE_':
    #     column_formats[v.get('display_name')] = 'gray_background'
    else:
        column_formats[v.get('display_name')] = 'general'
    
    if v.get('data_name') and v.get('data_name') in all_data.columns:
        column_renames[v.get('data_name')] = v.get('display_name')
    else:
        all_data[v.get('display_name')] = None

all_data.rename(columns=column_renames, inplace=True)

all_data.info()


# %%

all_data['Course'] = all_data.apply(lambda x: exl_writer.format_excel_hyperlink(
                                                utils.build_canvas_url('courses',
                                                                       x['Course Id'])
                                            , x['Course']), axis=1)

# %%
for col, link_map in TOOL_LINK_MAP.items():
    all_data[col] = all_data.apply(lambda x: exl_writer.format_excel_hyperlink(
                                                utils.build_canvas_url('courses', 
                                                                    x['Course Id'], 
                                                                    link_map.get('content_type'),
                                                                    link_map.get('content_id')),
                                                link_map.get('content_name')
                                            ), axis=1)



# %%
#from utilities import excel_report_writer as exl_writer
SUB_NAME_TERMS = '_'.join(PARAMS.get('term_sis_list')) if len(PARAMS.get('term_sis_list')) <= 2 else 'Multiple'


REPORT_NAME = os.path.join(reports_dir, 'Course Summary - {sub_name} - {date}.xlsx')

exl_writer.save_custom_xls(REPORT_NAME.format(sub_name=SUB_NAME_TERMS, date=utils.current_date()),  #sub_name = f"v{version}"
                            list_dfs=[all_data[COLUMN_ORDER]],
                            list_nms=['Courses'],
                            column_formats=column_formats)







### ------------------------------------------------------------------------------------------------------
### ----------------------------------------------- KALTURA ----------------------------------------------
### ------------------------------------------------------------------------------------------------------







# all_broken_data = pd.merge(course_df[['course_id','course_canvas_id','course_name','term_name']],
#                             broken_content,
#                             on=['course_id'])



# %%

### ------------------------------------------------------------------------------------------------------
### -------------------------------------------- BROKEN LINKS --------------------------------------------
### ------------------------------------------------------------------------------------------------------

# %%
BROKEN_COLUMN_MAP = {
    0: { 'display_name': 'Course Id', 'data_name': 'course_canvas_id' },
    1: { 'display_name': 'Course', 'data_name': 'course_name' },
    2: { 'display_name': 'Term', 'data_name': 'term_name' },
    3: { 'display_name': 'Content Name', 'data_name': 'content_name' },
    4: { 'display_name': 'Content Type', 'data_name': 'content_type' },
    5: { 'display_name': 'Content Id', 'data_name': 'content_id' },
    6: { 'display_name': 'Content State', 'data_name': 'content_state' },

    7: { 'display_name': 'Content Created', 'data_name': 'content_created_at' },
    8: { 'display_name': 'Content Updated', 'data_name': 'content_updated_at' },

    9: { 'display_name': 'Total Broken Links', 'data_name': 'total_broken_links' },
    10: { 'display_name': 'Potential Broken Links', 'data_name': 'link_list' },
}
BROKEN_COLUMN_ORDER = [v.get('display_name') for k, v in BROKEN_COLUMN_MAP.items()]

# %%
all_broken_data = pd.merge(course_df[['course_id','course_canvas_id','course_name','term_name']],
                            broken_content,
                            on=['course_id'])


all_broken_data.info()


# %%
for col in ['content_created_at', 'content_updated_at']:
    all_broken_data[col] = all_broken_data[col].apply(lambda x: x if pd.isnull(x) else x.strftime('%Y-%m-%d'))


#all_broken_data.groupby(['course_canvas_id','joinable_content','content_id'], as_index=False).count()

# %%

column_formats = {}
column_renames = {}
for k, v in BROKEN_COLUMN_MAP.items():
    if v.get('display_name') in ['Course', 'Content Name'] or v.get('display_name') in TOOL_LINK_MAP.keys():
        column_formats[v.get('display_name')] = 'hyperlink'
    elif v.get('data_name') in ['link_list']:
        column_formats[v.get('display_name')] = 'general_multiline'
    elif v.get('data_name') and v.get('data_name').find('total') > -1:
        column_formats[v.get('display_name')] = 'black_integers'
    elif v.get('data_name') and v.get('data_name').find('percent') > -1:
        column_formats[v.get('display_name')] = 'percent'
    elif v.get('data_name') in dict_sort_content.keys(): # is column with link count per content type
        column_formats[v.get('display_name')] = 'black_integers'
    else:
        column_formats[v.get('display_name')] = 'general'
    
    if v.get('data_name') and v.get('data_name') in all_broken_data.columns:
        column_renames[v.get('data_name')] = v.get('display_name')
    else:
        all_broken_data[v.get('display_name')] = None

all_broken_data.rename(columns=column_renames, inplace=True)

all_broken_data.info()


# %%

all_broken_data['Course'] = all_broken_data.apply(lambda x: exl_writer.format_excel_hyperlink(
                                                    utils.build_canvas_url('courses',
                                                                        x['Course Id'])
                                                , x['Course']), axis=1)


all_broken_data['Content Name'] = all_broken_data.apply(lambda x: exl_writer.format_excel_hyperlink(
                                                    utils.build_canvas_url('courses',
                                                                        x['Course Id'],
                                                                        content_type=(x['Content Type'] if x['Content Type'].find('/')<0 else x['Content Type'][:x['Content Type'].find('/')]),
                                                                        content_id=x['Content Id']
                                                                        )
                                                , x['Content Name']), axis=1)


# %%
broken_version = 3

REPORT_NAME = os.path.join(reports_dir, 'Broken Links - {sub_name} - {date}.xlsx')

exl_writer.save_custom_xls(REPORT_NAME.format(sub_name=SUB_NAME_TERMS, date=utils.current_date()),
                         list_dfs=[all_broken_data[BROKEN_COLUMN_ORDER]],
                         list_nms=['Broken Links'],
                         column_formats=column_formats)

# %%



# %%
###------------------------------------- FOR FILES REPORT ----------------------------
# content_files = pd.DataFrame(content_df.files.tolist(),
#                              index=[content_df.course_id, 
#                                     content_df.link_to_content, 
#                                     content_df.content_type,
#                                     content_df.content_state,
#                                     content_df.content_updated_at,
#                                     content_df.content_id,
#                                     ]
#                             ).stack()

# def content_is_published(content_state):
#     PUBLISHED_STATES = ['published', 'active', 'visible',]
#     return content_state in PUBLISHED_STATES
    
    
# content_files = content_files.reset_index()[[0,'course_id','link_to_content','content_type','content_state','content_updated_at','content_id']] 
# content_files.columns = ['file_id','course_id','link_to_content','content_type','content_state','content_updated_at','content_id']

# content_files.drop_duplicates(inplace=True)
# content_files.info()

# content_files['content_is_published'] = content_files['content_state'].apply(lambda x: content_is_published(x))


# sort_columns = ['course_id','file_id','content_is_published','content_sort','content_updated_at','content_id']
# sort_order = [True, True, False, True, False, False]
# # Sort content items and filter to one content item per file id
# content_files['content_sort'] = content_files['content_type'].apply(lambda x:  dict_sort_content.get(x) )
# content_files.sort_values(by=sort_columns, ascending=sort_order, inplace=True, na_position='last')
# content_files = pd.DataFrame(content_files.groupby(['course_id','file_id']).head(1))
# content_files.reset_index(drop=True, inplace=True)

# %%


###------------------------------------- FOR CONTENT REPORT ----------------------------
# content_df['content_url'] = content_df.apply(lambda x: utils.build_canvas_url('courses', x['course_canvas_id'], 
#                                                                         x['content_type'], x['content_id']), axis=1)

# content_df['content_type'] = np.where((content_df['content_type']=='assignments') & (content_df['content_id']=='syllabus'),
#                                     'syllabus',
#                                     content_df['content_type'])

# content_df['link_to_content'] = content_df.apply(lambda x: utils.build_canvas_url('courses', x['course_canvas_id'], x['content_type'], x['content_id']), axis=1)

# content_df['content_name'] = content_df.apply(lambda x: exl_writer.format_excel_hyperlink(
#                                                         utils.build_canvas_url('courses',x['course_id'],x['content_type'],x['content_id'])
#                                                     ,x['content_name']), axis=1)

# %%

# %%

# %%

# %%

# %%

# %%


# %%
### ------------------------------------------------------------------------------------------------------
### ---------------------------------------------- ALL FILES ---------------------------------------------
### ------------------------------------------------------------------------------------------------------


# In[]:
FILES_COLUMN_MAP = {
    0: { 'display_name': 'Course Id', 'data_name': 'course_canvas_id' },
    1: { 'display_name': 'Course', 'data_name': 'course_name' },
    2: { 'display_name': 'Term', 'data_name': 'term_name' },
    3: { 'display_name': 'File Name', 'data_name': 'file_name' },
    4: { 'display_name': 'File Id', 'data_name': 'file_id' },
    5: { 'display_name': 'File State', 'data_name': 'file_state' },
    6: { 'display_name': 'File Type', 'data_name': 'file_type' },
    7: { 'display_name': 'File Extension', 'data_name': 'file_extension' },
    8: { 'display_name': 'File Created', 'data_name': 'file_created_at' },
    9: { 'display_name': 'File Updated', 'data_name': 'file_updated_at' },
    10: { 'display_name': 'File Linked', 'data_name': 'file_is_linked' },
    11: { 'display_name': 'Total Links to File', 'data_name': 'total_links_to_file' },
    12: { 'display_name': 'announcements', 'data_name': 'announcements' },
    13: { 'display_name': 'assignments', 'data_name': 'assignments' },
    14: { 'display_name': 'discussions', 'data_name': 'discussions' },
    15: { 'display_name': 'modules', 'data_name': 'modules' },
    16: { 'display_name': 'pages', 'data_name': 'pages' },
    17: { 'display_name': 'quizzes', 'data_name': 'quizzes' },
    18: { 'display_name': 'quizzes/answers', 'data_name': 'quizzes/answers' },
    19: { 'display_name': 'quizzes/questions', 'data_name': 'quizzes/questions' },
    20: { 'display_name': 'syllabus', 'data_name': 'syllabus' },
    21: { 'display_name': 'Links to Content', 'data_name': 'link_list' },
}
FILES_COLUMN_ORDER = [v.get('display_name') for k, v in FILES_COLUMN_MAP.items()]

# %%


files_with_link = file_content_links_df.loc[file_content_links_df['linked_file_id'].notnull()].copy(deep=True)

files_with_link = pd.merge(course_df[['course_id','course_canvas_id']],
                            files_with_link,
                            on=['course_id'])


files_with_link['joinable_content'] = files_with_link['content_type'].apply(lambda x: x[:x.find('/')] if x.find('/')>-1 else x)





files_with_link['content_link'] = files_with_link.apply(lambda x: utils.build_canvas_url(
                                                                        context_type= 'courses',
                                                                        context_id= x['course_canvas_id'], 
                                                                        content_type=x['joinable_content'], 
                                                                        content_id=x['content_id'],
                                                                        canvas_domain=None), axis=1)


# %%

merged_files_links = files_with_link.groupby(['course_id', 'linked_file_id'
                                                ], as_index=False).agg(link_list=('content_link',list))

merged_files_links.rename(columns={'linked_file_id': 'file_id'}, inplace=True)
merged_files_links.info()
# %%
all_files_data = pd.merge(merged_files_data,
                        merged_files_links,
                        how='left',
                        on=['course_id', 'file_id'])
all_files_data.info()
# %%

all_files_data = pd.merge(course_df[['course_id','course_canvas_id','course_name','term_name']],
                            all_files_data,
                            on=['course_id'])

all_files_data.info()
# %%
all_files_data['file_is_linked'] = all_files_data['total_links_to_file'].astype(bool)
# %%
for col in ['file_created_at', 'file_updated_at']:
    all_files_data[col] = all_files_data[col].apply(lambda x: x if pd.isnull(x) else x.strftime('%Y-%m-%d'))
# %%
all_files_data.head()

# %%


column_formats = {}
column_renames = {}
for k, v in FILES_COLUMN_MAP.items():
    if v.get('display_name') == 'Course' or v.get('display_name') in TOOL_LINK_MAP.keys():
        column_formats[v.get('display_name')] = 'hyperlink'
    elif v.get('data_name') in ['link_list']:
        column_formats[v.get('display_name')] = 'general_multiline'
    elif v.get('data_name') and v.get('data_name').find('total') > -1:
        column_formats[v.get('display_name')] = 'black_integers'
    elif v.get('data_name') and v.get('data_name').find('percent') > -1:
        column_formats[v.get('display_name')] = 'percent'
    elif v.get('data_name') in dict_sort_content.keys(): # is column with link count per content type
        column_formats[v.get('display_name')] = 'black_integers'
    else:
        column_formats[v.get('display_name')] = 'general'
    
    if v.get('data_name') and v.get('data_name') in all_files_data.columns:
        column_renames[v.get('data_name')] = v.get('display_name')
    else:
        all_files_data[v.get('display_name')] = None

all_files_data.rename(columns=column_renames, inplace=True)

all_files_data.info()


# %%

all_files_data['Course'] = all_files_data.apply(lambda x: exl_writer.format_excel_hyperlink(
                                                    utils.build_canvas_url('courses',
                                                                        x['Course Id'])
                                                , x['Course']), axis=1)

# %%
all_files_data.head()

# %%

REPORT_NAME = os.path.join(reports_dir, 'All Files - {sub_name} - {date}.xlsx')

exl_writer.save_custom_xls(REPORT_NAME.format(sub_name=SUB_NAME_TERMS, date=utils.current_date()),
                         list_dfs=[all_files_data[FILES_COLUMN_ORDER]],
                         list_nms=['Files'],
                         column_formats=column_formats)


# %%


# %%
WYSIWYG_COLUMN_MAP = {
    0: { 'display_name': 'Course Id', 'data_name': 'course_id'},
    1: { 'display_name': 'Course', 'data_name': 'course_name'},
    2: { 'display_name': 'Term', 'data_name': 'term_name'},
    3: { 'display_name': 'WYSIWYG Name', 'data_name': 'content_name'},
    4: { 'display_name': 'WYSIWYG Id', 'data_name': 'content_id'},
    5: { 'display_name': 'WYSIWYG Type', 'data_name': 'content_type'},
    6: { 'display_name': 'WYSIWYG Status', 'data_name': 'content_state'},
    7: { 'display_name': 'In Modules', 'data_name': 'in_modules'},
    8: { 'display_name': 'Has Content', 'data_name': 'has_content'},
    9: { 'display_name': 'Kaltura Video Links', 'data_name': 'kaltura_video_links'},
    10: { 'display_name': 'WYSIWYG Created', 'data_name': 'content_created_at'},
    11: { 'display_name': 'WYSIWYG Updated', 'data_name': 'content_updated_at'},
    12: { 'display_name': 'WYSIWYG Linked', 'data_name': 'content_is_linked'},
    13: { 'display_name': 'Total Links to WYSIWYG', 'data_name': 'total_links_to_content'},
    14: { 'display_name': 'Links to Content', 'data_name': 'link_to_content'},
}

# %%

# %%
