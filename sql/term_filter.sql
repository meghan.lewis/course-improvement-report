SELECT DISTINCT sis_source_id
    FROM enrollment_term_dim
    WHERE DATEADD(day, %(days_from_start)s, date_start) < GETDATE()  -- Term start date is within N days from now (or in the past)
        AND DATEADD(day, %(days_from_end)s, date_end) > GETDATE()  -- Term end date is not more than N daysfrom now
        AND sis_source_id ~ '20[[:digit:]][[:digit:]][234]0';
