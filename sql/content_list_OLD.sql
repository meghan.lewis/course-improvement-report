SELECT DISTINCT 
        course_id, 
        content_type, 
        content_id, 
        content_name, 
        content_updated_at,
        content_state, 
        content_text
    FROM (
--SYLLABUS
        SELECT DISTINCT courses.id course_id, 'syllabus' AS content_type, 'syllabus' AS content_id,
                nd.name AS content_name, d.created_at content_updated_at,
                nid.visible as content_state, -- '' AS sub_content_type, '' AS sub_content_name,
                d.syllabus_body AS content_text
        FROM courses
                JOIN course_dim AS d ON (courses.id = d.id)
                AND d.syllabus_body IS NOT NULL
                JOIN course_ui_navigation_item_fact AS nif ON (d.id = nif.course_id)
                JOIN course_ui_canvas_navigation_dim AS nd ON (nd.canvas_id = 1)
                        AND (nif.course_ui_canvas_navigation_id = nd.id)
                JOIN course_ui_navigation_item_dim AS nid ON (nif.course_ui_navigation_item_id = nid.id)
UNION -- PAGES
        SELECT DISTINCT courses.id course_id, 'pages' AS content_type, d.url AS content_id, d.title AS content_name,
                NVL(d.updated_at, d.created_at) content_updated_at, d.workflow_state as content_state,
                --'' AS sub_content_type, '' AS sub_content_name,
                d.body AS content_text
        FROM courses
                JOIN wiki_page_fact AS f ON (f.parent_course_id = courses.id)
                JOIN wiki_page_dim AS d ON (d.id = f.wiki_page_id)
                        AND (d.workflow_state <> 'deleted')
UNION --ASSIGNMENTS
        SELECT DISTINCT courses.id course_id, 'assignments' AS content_type, CAST(a.canvas_id AS VARCHAR) AS content_id,
                a.title AS content_name, NVL(a.updated_at, a.created_at) content_updated_at,
                a.workflow_state as content_state, --'' AS sub_content_type, '' AS sub_content_name,
                a.description AS content_text
        FROM courses
                JOIN assignment_dim AS a ON (a.course_id = courses.id)
                        AND (a.workflow_state <> 'deleted')
                        AND (a.submission_types NOT IN ('online_quiz','discussion_topic'))
UNION --DISCUSSIONS
        SELECT DISTINCT courses.id course_id, ISNULL(LOWER(d.type)+'s','discussions') AS content_type,
                CAST(d.canvas_id AS VARCHAR) AS content_id,
                d.title AS content_name, NVL(d.updated_at, d.created_at) content_updated_at,
                d.workflow_state as content_state,  --'' AS sub_content_type, '' AS sub_content_name,
                d.message AS content_text
        FROM courses
                JOIN discussion_topic_dim AS d ON (d.course_id = courses.id)
                        AND (d.workflow_state <> 'deleted')
UNION -- QUIZZES
        SELECT DISTINCT courses.id course_id, 'quizzes' AS content_type, CAST(q.canvas_id AS VARCHAR) AS content_id,
                q.name AS content_name, NVL(q.updated_at, q.created_at) content_updated_at,
                q.workflow_state as content_state,  --'' AS sub_content_type, '' AS sub_content_name,
                q.description AS content_text
        FROM courses
                JOIN quiz_dim AS q ON (q.course_id = courses.id)
                        AND (q.workflow_state <> 'deleted')
UNION -- MODULE ITEMS
        SELECT DISTINCT courses.id course_id, 'modules' AS content_type, CAST(m.canvas_id AS VARCHAR) AS content_id,
                m.name as content_name, NVL(mid.updated_at, mid.created_at) content_updated_at,
                CASE
                WHEN m.workflow_state = mid.workflow_state THEN m.workflow_state
                WHEN m.workflow_state = 'unpublished' THEN m.workflow_state
                ELSE mid.workflow_state 
                END as content_state,
                -- '' AS sub_content_type, '' AS sub_content_name,
                '<a href="' + NVL(mid.url, '/files/' + CAST(CAST(RIGHT(CAST(mid.file_id AS VARCHAR), LEN(CAST(mid.file_id AS VARCHAR))-4) AS INT) AS VARCHAR)) + '"></a>' AS content_text
        FROM courses 
                JOIN module_item_dim AS mid ON (mid.course_id = courses.id)
                        AND (mid.workflow_state <> 'deleted')
                        AND ((mid.file_id IS NOT NULL) OR (mid.url IS NOT NULL))
                JOIN module_dim AS m ON (mid.module_id = m.id)
) AS content;



