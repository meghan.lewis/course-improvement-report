SELECT DISTINCT 
        course_id, 
        content_type, 
        content_id, 
        content_name, 
        content_created_at,
        content_updated_at,
        content_state, 
        content_text
    FROM (
    --SYLLABUS
        SELECT DISTINCT courses.id course_id, 'syllabus' AS content_type, 'syllabus' AS content_id,
                nd.name AS content_name, d.created_at content_created_at, CAST( NULL AS TIMESTAMP ) content_updated_at,
                nid.visible as content_state, -- '' AS sub_content_type, '' AS sub_content_name,
                d.syllabus_body AS content_text
        FROM courses
                JOIN course_dim AS d ON (courses.id = d.id)
                        AND d.syllabus_body IS NOT NULL
                JOIN course_ui_navigation_item_fact AS nif ON (d.id = nif.course_id)
                JOIN course_ui_canvas_navigation_dim AS nd ON (nd.canvas_id = 1)
                        AND (nif.course_ui_canvas_navigation_id = nd.id)
                JOIN course_ui_navigation_item_dim AS nid ON (nif.course_ui_navigation_item_id = nid.id)
UNION -- PAGES
        SELECT DISTINCT courses.id course_id, 'pages' AS content_type, d.url AS content_id, d.title AS content_name,
                NVL(d.created_at, d.updated_at) content_created_at, NVL(d.updated_at, d.created_at) content_updated_at, 
                d.workflow_state as content_state,
                --'' AS sub_content_type, '' AS sub_content_name,
                d.body AS content_text
        FROM courses
                JOIN wiki_page_fact AS f ON (f.parent_course_id = courses.id)
                JOIN wiki_page_dim AS d ON (d.id = f.wiki_page_id)
                        AND (d.workflow_state <> 'deleted')
UNION --ASSIGNMENTS
        SELECT DISTINCT courses.id course_id, 'assignments' AS content_type, CAST(d.canvas_id AS VARCHAR) AS content_id,
                d.title AS content_name, NVL(d.created_at, d.updated_at) content_created_at, 
                NVL(d.updated_at, d.created_at) content_updated_at,
                d.workflow_state as content_state, --'' AS sub_content_type, '' AS sub_content_name,
                d.description AS content_text
        FROM courses
                JOIN assignment_dim AS d ON (d.course_id = courses.id)
                        AND (d.workflow_state <> 'deleted')
                        AND (d.submission_types NOT IN ('online_quiz','discussion_topic'))
UNION --DISCUSSIONS
        SELECT DISTINCT courses.id course_id, ISNULL(LOWER(d.type)+'s','discussions') AS content_type,
                CAST(d.canvas_id AS VARCHAR) AS content_id, d.title AS content_name, 
                NVL(d.created_at, d.updated_at) content_created_at, NVL(d.updated_at, d.created_at) content_updated_at,
                d.workflow_state as content_state,  --'' AS sub_content_type, '' AS sub_content_name,
                d.message AS content_text
        FROM courses
                JOIN discussion_topic_dim AS d ON (d.course_id = courses.id)
                        AND (d.workflow_state <> 'deleted')
UNION -- QUIZZES
        SELECT DISTINCT courses.id course_id, 'quizzes' AS content_type, CAST(d.canvas_id AS VARCHAR) AS content_id,
                d.name AS content_name, NVL(d.created_at, d.updated_at) content_created_at, NVL(d.updated_at, d.created_at) content_updated_at,
                d.workflow_state as content_state,  --'' AS sub_content_type, '' AS sub_content_name,
                --d.description AS content_text
                d.description || CASE WHEN (f.question_count+f.unpublished_question_count)>0 THEN '   [ ' + CAST(f.question_count AS VARCHAR) + '/' + CAST(f.unpublished_question_count AS VARCHAR) + ' question(s) ]' END AS content_text
        FROM courses
                JOIN quiz_dim AS d ON (d.course_id = courses.id)
                        AND (d.workflow_state <> 'deleted')
                JOIN quiz_fact AS f ON (d.id = f.quiz_id)
UNION -- MODULE ITEMS
        SELECT course_id, content_type, content_id, content_name, content_created_at, content_updated_at, content_state,
                ( '<p>' || 
                        '<a id="modules_items-' || 
                        NVL(content_id, '') || 
                        '" href="' || 
                        NVL(item_url, 
                                ('/courses/' || NVL(course_canvas_id,'') || '/' ||  NVL(item_type,'') ||  '/' ||  NVL(item_id,''))
                          )  || 
                        '">' || 
                        NVL(content_name,'') || 
                        '</a>' || 
                        '</p>'
                ) content_text
                FROM (
                        SELECT DISTINCT courses.id course_id, CAST(courses.canvas_id AS VARCHAR) course_canvas_id,
                                'modules' AS content_type, CAST(m.canvas_id AS VARCHAR) AS content_id,
                                m.name as content_name, 
                                NVL(d.created_at, d.updated_at) content_created_at,
                                NVL(d.updated_at, d.created_at) content_updated_at,
                                CASE
                                        WHEN m.workflow_state = d.workflow_state THEN m.workflow_state
                                        WHEN m.workflow_state = 'unpublished' THEN m.workflow_state
                                        ELSE d.workflow_state 
                                END as content_state,
                                -- '' AS sub_content_type, '' AS sub_content_name,
                                CAST(d.canvas_id AS VARCHAR) as module_item_id,
                                d.title as module_item_name,
                                CAST(CASE LOWER(d.content_type)
                                        WHEN 'assignment' THEN 'assignments'
                                        WHEN 'attachment' THEN 'files'
                                        WHEN 'contextexternaltool' THEN 'external_tools' -----
                                        WHEN 'discussiontopic' THEN 'discussion_topics'
                                        WHEN 'quiz' THEN 'quizzes'
                                        WHEN 'wikipage' THEN 'pages'
                                END AS VARCHAR) item_type,
                                CAST(CASE LOWER(d.content_type)
                                        WHEN 'assignment' THEN  CAST(CAST(RIGHT(CAST(d.assignment_id AS VARCHAR), LEN(CAST(d.assignment_id AS VARCHAR))-4) AS BIGINT) AS VARCHAR)
                                        WHEN 'attachment' THEN  CAST(CAST(RIGHT(CAST(d.file_id AS VARCHAR), LEN(CAST(d.file_id AS VARCHAR))-4) AS BIGINT) AS VARCHAR)
                                        WHEN 'discussiontopic' THEN  CAST(CAST(RIGHT(CAST(d.discussion_topic_id AS VARCHAR), LEN(CAST(d.discussion_topic_id AS VARCHAR))-4) AS BIGINT) AS VARCHAR)
                                        WHEN 'quiz' THEN  CAST(CAST(RIGHT(CAST(d.quiz_id AS VARCHAR), LEN(CAST(d.quiz_id AS VARCHAR))-4) AS BIGINT) AS VARCHAR)
                                        WHEN 'wikipage' THEN p.url
                                END AS VARCHAR) item_id,
                                d.url as item_url
                        FROM courses 
                                JOIN module_item_dim AS d ON (d.course_id = courses.id)
                                        AND (d.workflow_state <> 'deleted')
                                JOIN module_dim AS m ON (d.module_id = m.id)
                                LEFT JOIN wiki_page_dim AS p ON (d.wiki_page_id = p.id)
                ) AS x
) AS content;



