SELECT DISTINCT 
    f.course_id, 
    d.canvas_id AS file_id,
    NVL(d.display_name,'') as file_name, 
    d.file_state,
    NVL(d.content_type,'') as file_type,
   -- REGEXP_SUBSTR(d.display_name, '[\.][^\.]{1,5}$' , LEN(d.display_name)-6) file_extension,
    d.created_at as file_created_at,
    d.updated_at as file_updated_at,
    d.replacement_file_id
FROM courses
    JOIN file_fact as f ON (f.course_id = courses.id)
    JOIN file_dim as d on (f.file_id = d.id)
        AND (d.owner_entity_type in ('course'))
        AND (d.file_state <> 'deleted')