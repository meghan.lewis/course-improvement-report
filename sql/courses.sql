SELECT DISTINCT 
        t.id term_id,
        t.canvas_id term_canvas_id,
        t.sis_source_id term_sis_id,
        t.name term_name,
        t.date_start term_start_date,
        t.date_end term_end_date,

        pa.id parent_account_id,
        pa.canvas_id parent_account_canvas_id,
        pa.sis_source_id parent_account_sis_id,
        pa.name parent_account_name,

        a.id account_id,
        a.canvas_id account_canvas_id,
        a.sis_source_id account_sis_id,
        a.name account_name,

        c.id course_id,
        c.canvas_id course_canvas_id,
        CASE
            WHEN c.sis_source_id ~ 'XXLS\\w{2}\\d{6}' THEN regexp_substr(c.sis_source_id, '(\\w{2}\\d{6})$')
            WHEN c.sis_source_id ~ '^\\w{4}\\d{6}$' THEN regexp_substr(c.sis_source_id, '(\\w{4}\\d{6})$')
            WHEN c.sis_source_id ~ 'CCRS\\w{2,4}\-\\d{4}\-\\d{5}.\\d{6}$' THEN regexp_substr(c.sis_source_id, '(\\d{5}.\\d{6})$')
            ELSE c.sis_source_id
        END course_sis_id,
        c.name course_name,
        c.workflow_state course_state,
        CASE WHEN c.workflow_state IN ('available','completed') THEN TRUE ELSE FALSE END course_is_published,
        c.created_at course_created_at,

        c.start_at course_start_date,
        c.conclude_at course_end_date
    FROM courses 
        JOIN course_dim AS c ON (courses.id = c.id)
        JOIN enrollment_term_dim AS t ON (c.enrollment_term_id = t.id)      
        JOIN account_dim AS a ON (c.account_id = a.id)
        LEFT JOIN account_dim AS pa ON (a.parent_account_id = pa.id)