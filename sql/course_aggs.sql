, instructor_emails AS (
   SELECT course_id,
          LISTAGG(DISTINCT email_address,'; ') instructors
    FROM (
        SELECT DISTINCT 
            ed.course_id,
            e.address as email_address,
            RANK () OVER( PARTITION BY courses.id, ed.user_id ORDER BY e.position) as email_rank
          FROM courses 
            JOIN enrollment_dim as ed on (courses.id = ed.course_id)
                AND ed.type IN ('TeacherEnrollment')
                AND ed.workflow_state IN ('active','completed')
            JOIN communication_channel_dim AS e ON (ed.user_id = e.user_id)
                AND (e.type = 'email') 
                AND (e.workflow_state IN ('active'))
                AND (e.address NOT IN ('tstaff@example.com','schedule@usu.edu'))
       ) AS x
   WHERE email_rank <= 1
   GROUP BY course_id
)
, instructor_enrollments AS (
    SELECT DISTINCT
        ed.course_id,
        ed.user_id,
        ed.type as enrollment_type,
        ed.workflow_state as enrollment_state
	FROM courses
        JOIN enrollment_dim as ed on (courses.id = ed.course_id)
                AND ed.type IN ('TeacherEnrollment')
                AND ed.workflow_state IN ('active','completed')
)
, students_enrollments AS (
    SELECT DISTINCT
        erd.course_id,
        erd.user_id,
        ed.type as enrollment_type,
        ed.workflow_state as enrollment_state
	FROM courses
        JOIN enrollment_rollup_dim AS erd ON (courses.id = erd.course_id) 
            AND erd.most_privileged_role IN ('StudentEnrollment')
        JOIN enrollment_dim AS ed ON (ed.id = erd.id) 
            AND ed.workflow_state NOT IN ('creation_pending','invited','rejected','deleted')
            AND ed.type = erd.most_privileged_role --- eliminates "StudentViewEnrollment" types which show as erd.most_privileged_role = 'StudentEnrollment'
)
, course_navigation AS (
    SELECT DISTINCT 
        f.course_id,
        f.course_ui_canvas_navigation_id navigation_menu_id,
        cd.name AS navigation_menu_name,
        cd."default" as navigation_default,
        f.course_ui_navigation_item_id course_navigation_id,
        nd.position course_navigation_position,
        nd.visible AS course_navigation_visible
    FROM courses
        JOIN course_ui_navigation_item_fact AS f ON (courses.id = f.course_id)
        JOIN course_ui_canvas_navigation_dim AS cd ON (cd.id = f.course_ui_canvas_navigation_id)
        JOIN course_ui_navigation_item_dim AS nd ON (nd.id = f.course_ui_navigation_item_id)
)

SELECT 
    courses.id AS course_id,
    i.instructors,
    COUNT(DISTINCT ie.user_id) as total_instructors,
    COUNT(DISTINCT e.user_id) as total_students,
    COUNT(DISTINCT CASE WHEN LOWER(n.course_navigation_visible) = 'visible' THEN course_navigation_id END) total_navigation_items,
    --COUNT(DISTINCT CASE WHEN LOWER(n.navigation_default) = 'default' AND LOWER(n.course_navigation_visible) = 'visible' THEN course_navigation_id END) default_visible_nav,
    --COUNT(DISTINCT CASE WHEN LOWER(n.navigation_default) != 'default' AND LOWER(n.course_navigation_visible) = 'visible' THEN course_navigation_id END) non_default_visible_nav,
    CASE WHEN MAX(CASE WHEN n.navigation_menu_name != 'Files' THEN NULL WHEN n.course_navigation_visible = 'visible' THEN 1 ELSE 0 END) = 1 THEN 'visible' ELSE 'hidden' END files_navigation
FROM courses
    LEFT JOIN students_enrollments AS e ON (courses.id = e.course_id)
    LEFT JOIN course_navigation AS n ON (courses.id = n.course_id)
    LEFT JOIN instructor_enrollments AS ie ON (courses.id = ie.course_id)
    LEFT JOIN instructor_emails AS i ON (courses.id = i.course_id)
GROUP BY courses.id, i.instructors;