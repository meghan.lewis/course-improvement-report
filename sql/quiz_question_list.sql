, quiz_questions AS (
	SELECT DISTINCT
		q.course_id,
		q.id as quiz_id,
		q.canvas_id AS quiz_canvas_id,
		q.name AS quiz_name,
		qq.id as quiz_question_id,
		qq.name AS question_name,
		TRIM(q.name) + NVL(CONCAT(' | ', TRIM(qq.name)),'') as quiz_and_question_name,
		NVL(qq.created_at, q.created_at) created_at,
		GREATEST(NVL(q.updated_at, q.created_at),
				NVL(qq.updated_at, qq.created_at),
				NVL(qg.updated_at, qg.created_at)
			) updated_at,
		q.workflow_state,
		TRIM(qq.question_text) question_text,
		length(NVL(qq.question_text,'')) as length_questions,
		TRIM(qq.correct_comments) correct_comments,
		TRIM(qq.incorrect_comments) incorrect_comments,
		TRIM(qq.neutral_comments) neutral_comments,
		length(NVL(qq.correct_comments,''))+length(NVL(qq.incorrect_comments,''))+length(NVL(qq.neutral_comments,'')) as length_questions_comments
	FROM courses
		JOIN quiz_dim AS q ON (q.course_id = courses.id)
			AND (q.workflow_state <> 'deleted')
		JOIN quiz_question_dim AS qq ON (q.id = qq.quiz_id)
			AND (qq.workflow_state <> 'deleted')
		LEFT JOIN quiz_question_group_dim AS qg ON (qq.quiz_question_group_id = qg.id)
), quiz_answers AS (
	SELECT DISTINCT
			qq.course_id,
			qq.quiz_canvas_id,
			qq.quiz_name,
			qq.quiz_and_question_name,
			qq.created_at,
			qq.updated_at,
			qq.workflow_state,
			TRIM(qa.text) text,
			TRIM(qa.html) html,
			TRIM(qa.blank_id) blank_id,
			--length(NVL(qa."text",''))+length(NVL(qa.html,''))+length(NVL(qa.blank_id,'')) as length_answers_text,
			TRIM(qa.answer_match_left) answer_match_left,
			TRIM(qa.answer_match_right) answer_match_right,
			TRIM(qa.matching_answer_incorrect_matches) matching_answer_incorrect_matches,
			(length(NVL(qa."text",''))
				+length(NVL(qa.html,''))
				+length(NVL(qa.blank_id,''))
				+length(NVL(qa.answer_match_left,''))
				+length(NVL(qa.answer_match_right,''))
				+length(NVL(qa.matching_answer_incorrect_matches,''))) as length_answers,
			TRIM(qa.text_after_answers) text_after_answers,
			TRIM(qa.comments) comments,
			length(NVL(qa.text_after_answers,''))+length(NVL(qa.comments,'')) as length_answers_comments
	FROM quiz_questions AS qq
		JOIN quiz_question_answer_dim AS qa ON (qq.quiz_question_id = qa.quiz_question_id)
)


SELECT course_id,
        content_type,
        content_id,
        content_name,
        content_created_at,
        content_updated_at,
        content_state,
        LISTAGG(DISTINCT content_text,' | ') within group (order by content_text) content_text
	FROM
	(      
	--QUIZ QUESTIONS
			SELECT DISTINCT
				course_id,
				'quizzes/questions' AS content_type,
				CAST(quiz_canvas_id AS VARCHAR) AS content_id,
				quiz_and_question_name AS content_name,
				created_at content_created_at,
				updated_at content_updated_at,
				workflow_state as content_state,
				question_text AS content_text
			FROM quiz_questions
			WHERE length_questions > 0
	 --QUIZ QUESTION COMMENTS
		UNION
			SELECT DISTINCT
				course_id,
				'quizzes/questions_comments' AS content_type,
				CAST(quiz_canvas_id AS VARCHAR) AS content_id,
				quiz_and_question_name AS content_name,
				created_at content_created_at,
				updated_at content_updated_at,
				workflow_state as content_state,
				BTRIM(  NVL(CONCAT(correct_comments,' ~ '), '') ||
							NVL(CONCAT(incorrect_comments,' ~ '), '') ||
							NVL(CONCAT(neutral_comments,' ~ '), '')
					, ' ~ ') AS content_text
			FROM quiz_questions
			WHERE length_questions_comments > 0
	--QUIZ ANSWERS
		UNION 
			SELECT DISTINCT
				course_id,
				'quizzes/answers' AS content_type, --'quizzes/answers_text' AS content_type,
				CAST(quiz_canvas_id AS VARCHAR) AS content_id,
				quiz_and_question_name AS content_name,
				created_at content_created_at,
				updated_at content_updated_at,
				workflow_state as content_state,
				BTRIM(  NVL(CONCAT("text", ' ~ '), '') ||
							NVL(CONCAT(html, ' ~ '), '') ||
							NVL(CONCAT(blank_id, ' ~ '), '') ||
							NVL(CONCAT(answer_match_left, ' ~ '), '') ||
							NVL(CONCAT(answer_match_right, ' ~ '), '') ||
							NVL(CONCAT(matching_answer_incorrect_matches, ' ~ '), '')
					, ' ~ ') AS content_text
			FROM quiz_answers
			WHERE length_answers > 0
	-- QUIZ ANSWER COMMENTS
		UNION 
			SELECT DISTINCT
				course_id,
				'quizzes/answers_comments' AS content_type,
				CAST(quiz_canvas_id AS VARCHAR) AS content_id,
				quiz_and_question_name AS content_name,
				created_at content_created_at,
				updated_at content_updated_at,
				workflow_state as content_state,
				BTRIM(  NVL(CONCAT(text_after_answers, ' ~ '), '') ||
							NVL(CONCAT(comments, ' ~ '), '')
					, ' ~ ') AS content_text
			FROM quiz_answers
			WHERE length_answers_comments > 0 
	) x
GROUP BY 
    course_id,
    content_type,
    content_id,
    content_name,
    content_created_at,
    content_updated_at,
    content_state