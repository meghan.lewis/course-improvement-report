WITH courses AS (
  SELECT DISTINCT
      c.id,
      c.canvas_id,
      c.sis_source_id,
      c.name,
      c.workflow_state
   FROM course_dim AS c
        JOIN enrollment_term_dim AS t ON (c.enrollment_term_id = t.id)
            AND (t.sis_source_id = ANY(%(term_sis_list)s))
            AND (c.workflow_state <> 'deleted')
)
