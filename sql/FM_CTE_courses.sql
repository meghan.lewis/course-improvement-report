WITH courses AS (
        SELECT DISTINCT t.sis_source_id term_id, t.name term_name,
            c.id, CAST(c.canvas_id AS VARCHAR) course_id,
            c.name as course_name, c.workflow_state AS course_state,
            CASE WHEN nd.visible='visible' THEN TRUE ELSE FALSE END as files_navigation
          FROM course_dim AS c
            JOIN (SELECT id, canvas_id, sis_source_id, name
                     FROM enrollment_term_dim
                     WHERE canvas_id IN (15,82)
	                        	OR ((sis_source_id ~ '20[[:digit:]][[:digit:]][234]0')
        		                    		AND (date_end > DATEADD(month, -6, date_trunc('day', GETDATE()))))
		              ) AS t ON (c.enrollment_term_id = t.id)
		                    AND (c.workflow_state <> 'deleted')
		                    AND NOT(t.canvas_id IN (15,82)
    		                    			AND (lower(c.name) ~ '(portfolio)'
		                    					OR (c.created_at < DATEADD(month, -36, date_trunc('day', GETDATE())))))
           JOIN course_ui_navigation_item_fact AS f ON (c.id = f.course_id)
           JOIN course_ui_canvas_navigation_dim AS cd ON (cd.id = f.course_ui_canvas_navigation_id)
		             AND (cd.name = 'Files')
            JOIN course_ui_navigation_item_dim AS nd ON (nd.id = f.course_ui_navigation_item_id)
    )

