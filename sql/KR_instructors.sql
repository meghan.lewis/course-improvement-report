SELECT DISTINCT course_id, LISTAGG(address,'; ')OVER(PARTITION BY course_id) instructors
    FROM (
        SELECT DISTINCT ed.course_id, cc.address, RANK() OVER(PARTITION BY ed.user_id ORDER BY cc.position)
            FROM courses AS c
                JOIN enrollment_dim AS ed ON (c.id = ed.course_id)
                    AND (ed.type = 'TeacherEnrollment')
                    AND (ed.workflow_state IN ('active'))
                JOIN communication_channel_dim AS cc ON (ed.user_id = cc.user_id)
                    AND (cc.type = 'email')
                    AND (cc.workflow_state IN ('active'))
                    AND (cc.address NOT IN ('tstaff@example.com','schedule@usu.edu'))
        ) AS x
    WHERE rank=1
