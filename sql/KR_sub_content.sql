-- content that has "sub content" items
-- links to a parent item (i.e. quiz) but reference is to a sub-content item (i.e. quiz question)



--QUIZ QUESTIONS
    SELECT DISTINCT c.id AS course_id,
            'quizzes' AS content_type, CAST(q.canvas_id AS VARCHAR) AS content_id,
            (NVL(qq.name,'') +
                CASE WHEN qq.position IS NOT NULL OR qg.name IS NOT NULL
                    THEN '    [' +
                                    NVL(CAST(qq.position AS VARCHAR),'')
                                    + CASE WHEN qq.position IS NOT NULL AND qg.name IS NOT NULL THEN ' / '  ELSE '' END +
                                    NVL(qg.name, '')
                                + ']'
                    ELSE ''	END
            ) AS content_name,
        CASE WHEN q.workflow_state NOT IN ('deleted','unpublished') THEN 'published' ELSE q.workflow_state END as content_state,
        'quiz_questions' as sub_content_type,
        CAST(qq.canvas_id AS VARCHAR) + '_question' AS sub_content_id,
        qq.question_text AS content_text
    FROM courses AS c
            JOIN quiz_dim AS q ON (q.course_id = c.id)
                AND (q.workflow_state <> 'deleted')
            JOIN quiz_question_dim AS qq ON (q.id = qq.quiz_id)
                AND (qq.workflow_state <> 'deleted')
                AND lower(qq.question_text) ~ %(search_string)s
            LEFT JOIN quiz_question_group_dim AS qg ON (qq.quiz_question_group_id = qg.id)
UNION
--QUIZ QUESTION COMMENTS
    SELECT DISTINCT c.id AS course_id,
            'quizzes' AS content_type, CAST(q.canvas_id AS VARCHAR) AS content_id,
            (NVL(qq.name,'') +
                    CASE WHEN qq.position IS NOT NULL OR qg.name IS NOT NULL
                    THEN '    [' +
                                    NVL(CAST(qq.position AS VARCHAR),'')
                                    + CASE WHEN qq.position IS NOT NULL AND qg.name IS NOT NULL THEN ' / '  ELSE '' END +
                                    NVL(qg.name, '')
                                + ']'
                    ELSE ''	END
            ) AS content_name,
        CASE WHEN q.workflow_state NOT IN ('deleted','unpublished') THEN 'published' ELSE q.workflow_state END as content_state,
        'quiz_question_comments' as sub_content_type,
        CAST(qq.canvas_id AS VARCHAR) + '_comment' AS sub_content_id,
        (CASE WHEN lower(qq.correct_comments) ~ %(search_string)s THEN ' {{correct comments}} ' + qq.correct_comments ELSE '' END +
        CASE WHEN lower(qq.incorrect_comments) ~ %(search_string)s THEN ' {{incorrect comments}} ' + qq.incorrect_comments ELSE '' END +
        CASE WHEN lower(qq.neutral_comments) ~ %(search_string)s THEN ' {{neutral comments}} ' + qq.neutral_comments ELSE '' END
        ) AS content_text
    FROM courses AS c
            JOIN quiz_dim AS q ON (q.course_id = c.id)
                AND (q.workflow_state <> 'deleted')
            JOIN quiz_question_dim AS qq ON (q.id = qq.quiz_id)
                AND (qq.workflow_state <> 'deleted')
                AND (lower(qq.correct_comments) ~ %(search_string)s
                        OR lower(qq.incorrect_comments) ~ %(search_string)s
                        OR lower(qq.neutral_comments) ~ %(search_string)s)
            LEFT JOIN quiz_question_group_dim AS qg ON (qq.quiz_question_group_id = qg.id)
UNION
--QUIZ ANSWERS
    SELECT DISTINCT c.id AS course_id,
            'quizzes' AS content_type, CAST(q.canvas_id AS VARCHAR) AS content_id,
                (NVL(qq.name,'') +
                        CASE WHEN qq.position IS NOT NULL OR qg.name IS NOT NULL
                        THEN '    [' +
                                        NVL(CAST(qq.position AS VARCHAR),'')
                                        + CASE WHEN qq.position IS NOT NULL AND qg.name IS NOT NULL THEN ' / '  ELSE '' END +
                                        NVL(qg.name, '')
                                    + ']'
                        ELSE ''	END
                ) as content_name,
            CASE WHEN q.workflow_state NOT IN ('deleted','unpublished') THEN 'published' ELSE q.workflow_state END as content_state,
            'quiz_answers' as sub_content_type,
            CAST(qq.canvas_id AS VARCHAR) + '_answer' AS sub_content_id,
            (CASE WHEN lower(qa.text) ~ %(search_string)s THEN ' {{text}} ' + qa.text ELSE '' END +
            CASE WHEN lower(qa.html) ~ %(search_string)s THEN ' {{html}} ' + qa.html ELSE '' END +
            CASE WHEN lower(qa.blank_id) ~ %(search_string)s THEN ' {{blank_id}} ' + qa.blank_id ELSE '' END +
            CASE WHEN lower(qa.answer_match_left) ~ %(search_string)s THEN ' {{answer_match_left}} ' + qa.answer_match_left ELSE '' END +
            CASE WHEN lower(qa.answer_match_right) ~ %(search_string)s THEN ' {{answer_match_right}} ' + qa.answer_match_right ELSE '' END +
            CASE WHEN lower(qa.matching_answer_incorrect_matches) ~ %(search_string)s THEN ' {{matching_answer_incorrect_matches}} ' + qa.matching_answer_incorrect_matches ELSE '' END
            ) AS content_text
    FROM courses AS c
            JOIN quiz_dim AS q ON (q.course_id = c.id)
                AND (q.workflow_state <> 'deleted')
            JOIN quiz_question_dim AS qq ON (q.id = qq.quiz_id)
                AND (qq.workflow_state <> 'deleted')
            JOIN quiz_question_answer_dim AS qa ON (qq.id = qa.quiz_question_id)
                AND (lower(qa.text) ~ %(search_string)s
                        OR lower(qa.html) ~ %(search_string)s
                        OR lower(qa.blank_id) ~ %(search_string)s
                        OR lower(qa.answer_match_left) ~ %(search_string)s
                        OR lower(qa.answer_match_right) ~ %(search_string)s
                        OR lower(qa.matching_answer_incorrect_matches) ~ %(search_string)s)
            LEFT JOIN quiz_question_group_dim AS qg ON (qq.quiz_question_group_id = qg.id)
UNION
    --QUIZ ANSWERS  (MATCHING)
    SELECT DISTINCT c.id AS course_id,
        'quizzes' AS content_type, CAST(q.canvas_id AS VARCHAR) AS content_id,
            (NVL(qq.name,'') +
                CASE WHEN qq.position IS NOT NULL OR qg.name IS NOT NULL
                THEN '    [' +
                                NVL(CAST(qq.position AS VARCHAR),'')
                                + CASE WHEN qq.position IS NOT NULL AND qg.name IS NOT NULL THEN ' / '  ELSE '' END +
                                NVL(qg.name, '')
                            + ']'
                ELSE ''	END
        ) as content_name,
        CASE WHEN q.workflow_state NOT IN ('deleted','unpublished') THEN 'published' ELSE q.workflow_state END as content_state,
        'quiz_answers' as sub_content_type,
        CAST(qq.canvas_id AS VARCHAR) + '_answer' AS sub_content_id,
        (
        CASE WHEN lower(qa.answer_match_left) ~ %(search_string)s THEN ' {{answer_match_left}} ' + qa.answer_match_left ELSE '' END +
        CASE WHEN lower(qa.answer_match_right) ~ %(search_string)s THEN ' {{answer_match_right}} ' + qa.answer_match_right ELSE '' END +
        CASE WHEN lower(qa.matching_answer_incorrect_matches) ~ %(search_string)s THEN ' {{matching_answer_incorrect_matches}} ' + qa.matching_answer_incorrect_matches ELSE '' END
        ) AS content_text
    FROM courses AS c
            JOIN quiz_dim AS q ON (q.course_id = c.id)
                AND (q.workflow_state <> 'deleted')
            JOIN quiz_question_dim AS qq ON (q.id = qq.quiz_id)
                AND (qq.workflow_state <> 'deleted')
            JOIN quiz_question_answer_dim AS qa ON (qq.id = qa.quiz_question_id)
                    AND (lower(qa.answer_match_left) ~ %(search_string)s
                        OR lower(qa.answer_match_right) ~ %(search_string)s
                        OR lower(qa.matching_answer_incorrect_matches) ~ %(search_string)s)
            LEFT JOIN quiz_question_group_dim AS qg ON (qq.quiz_question_group_id = qg.id)

UNION
-- QUIZ ANSWER COMMENTS
    SELECT DISTINCT c.id AS course_id,
            'quizzes' AS content_type, CAST(q.canvas_id AS VARCHAR) AS content_id,
                 (NVL(qq.name,'') +
                    CASE WHEN qq.position IS NOT NULL OR qg.name IS NOT NULL
                    THEN '    [' +
                                    NVL(CAST(qq.position AS VARCHAR),'')
                                    + CASE WHEN qq.position IS NOT NULL AND qg.name IS NOT NULL THEN ' / '  ELSE '' END +
                                    NVL(qg.name, '')
                                + ']'
                    ELSE ''	END
            ) as content_name,
        CASE WHEN q.workflow_state NOT IN ('deleted','unpublished') THEN 'published' ELSE q.workflow_state END as content_state,
        'quiz_answer_comments' as sub_content_type,
        CAST(qq.canvas_id AS VARCHAR) + '_comment' AS sub_content_id,
        (CASE WHEN lower(qa.text_after_answers) ~ %(search_string)s THEN ' {{text_after_answers}} ' + qa.text_after_answers ELSE '' END +
        CASE WHEN lower(qa.comments) ~ %(search_string)s THEN ' {{comments}} ' + qa.comments ELSE '' END
        ) AS content_text
    FROM courses AS c
            JOIN quiz_dim AS q ON (q.course_id = c.id)
                AND (q.workflow_state <> 'deleted')
            JOIN quiz_question_dim AS qq ON (q.id = qq.quiz_id)
                AND (qq.workflow_state <> 'deleted')
            JOIN quiz_question_answer_dim AS qa ON (qq.id = qa.quiz_question_id)
                        AND (lower(qa.text_after_answers) ~ %(search_string)s
                                OR lower(qa.comments) ~ %(search_string)s)
            LEFT JOIN quiz_question_group_dim AS qg ON (qq.quiz_question_group_id = qg.id)

;