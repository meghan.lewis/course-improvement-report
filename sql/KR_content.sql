    SELECT DISTINCT c.id AS course_id,
        'assignments' AS content_type, 'syllabus' AS content_id, nd.name AS content_name,
            CASE WHEN nid.visible NOT IN ('hidden') THEN 'published'
            ELSE 'unpublished' END as content_state,
        d.syllabus_body AS content_text
    FROM course_dim AS d
        JOIN courses AS c ON (d.id = c.id)
                AND (lower(d.syllabus_body) ~ %(search_string)s)
        JOIN course_ui_navigation_item_fact AS nif ON (d.id = nif.course_id)
        JOIN course_ui_canvas_navigation_dim AS nd ON (nd.canvas_id = 1)
                AND (nif.course_ui_canvas_navigation_id = nd.id)
        JOIN course_ui_navigation_item_dim AS nid ON (nif.course_ui_navigation_item_id = nid.id)
UNION
    SELECT DISTINCT c.id AS course_id,
        'pages' AS content_type, d.url as content_id, d.title as content_name,
        CASE WHEN d.workflow_state NOT IN ('deleted','unpublished') THEN 'published'
            ELSE d.workflow_state END as content_state,
        body AS content_text
    FROM courses AS C
        JOIN wiki_page_fact AS f ON (f.parent_course_id = c.id)
        JOIN wiki_page_dim AS d ON (d.id = f.wiki_page_id)
                AND (d.workflow_state <> 'deleted')
                AND (lower(d.body) ~ %(search_string)s)
UNION
    SELECT DISTINCT c.id AS course_id,
            ISNULL(LOWER(d.type)+'s','discussion_topics') AS content_type,
            CAST(d.canvas_id AS VARCHAR) AS content_id, title AS content_name,
            CASE WHEN d.workflow_state NOT IN ('deleted','unpublished') THEN 'published'
                ELSE d.workflow_state END as content_state,
            message AS content_text
        FROM courses AS C
            JOIN discussion_topic_dim AS d ON (d.course_id = c.id)
                    AND (d.workflow_state <> 'deleted')
                    AND  (lower(d.message) ~ %(search_string)s)
UNION
    SELECT DISTINCT c.id AS course_id,
            'quizzes' AS content_type, CAST(q.canvas_id AS VARCHAR) AS content_id,
            q.name AS content_name,
            CASE WHEN q.workflow_state NOT IN ('deleted','unpublished') THEN 'published'
                ELSE q.workflow_state END as content_state,
            q.description AS content_text
        FROM courses AS c
                JOIN quiz_dim AS q ON (q.course_id = c.id)
                    AND  (lower(q.description) ~ %(search_string)s)
                    AND (q.workflow_state <> 'deleted')
UNION
    SELECT DISTINCT c.id AS course_id,
            'assignments' AS content_type, CAST(a.canvas_id AS VARCHAR) AS content_id,
            a.title AS content_name,
            CASE WHEN a.workflow_state NOT IN ('deleted','unpublished') THEN 'published'
                ELSE a.workflow_state END as content_state,
            a.description AS content_text
        FROM courses AS c
                JOIN assignment_dim AS a ON (a.course_id = c.id)
                    AND (lower(a.description) ~ %(search_string)s)
                    AND (a.workflow_state <> 'deleted')
                    AND (a.submission_types NOT IN ('online_quiz','discussion_topic'))  ---------- ADDED 2021-05-12
;