<p align="center">
  <a href="" rel="noopener">
 <img width=200px height=200px src="https://i.imgur.com/6wj0hh6.jpg" alt="Project logo"></a>
</p>

<h3 align="center">Course Improvement Report</h3>
<h4 align="center">Docker/PySpark version</h4>
<div align="center">
<!-- 
[![Status](https://img.shields.io/badge/status-active-success.svg)]()
[![GitHub Issues](https://img.shields.io/github/issues/kylelobo/The-Documentation-Compendium.svg)](https://github.com/kylelobo/The-Documentation-Compendium/issues)
[![GitHub Pull Requests](https://img.shields.io/github/issues-pr/kylelobo/The-Documentation-Compendium.svg)](https://github.com/kylelobo/The-Documentation-Compendium/pulls)
[![License](https://img.shields.io/badge/license-MIT-blue.svg)](/LICENSE) -->

</div>

---

<p align="center"> Course improvement report builder. 
    <br> 
</p>

## 📝 Table of Contents

- [About](#about)
- [Getting Started](#getting_started)
- [Deployment](#deployment)
- [Usage](#usage)
- [Built Using](#built_using)
- [TODO](../TODO.md)
- [Contributing](../CONTRIBUTING.md)
- [Authors](#authors)
- [Acknowledgments](#acknowledgement)

## 🧐 About <a name = "about"></a>

Write about 1-2 paragraphs describing the purpose of your project.

## 🏁 Getting Started <a name = "getting_started"></a>

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See [deployment](#deployment) for notes on how to deploy the project on a live system.


#### NOTE:
VM IP --- 129.123.41.30


### Prerequisites

Python 3.8
git

```

```

### Installing

Clone project from gitlab

```
git clone git@gitlab.com:meghan.lewis/course-improvement-report.git
```


```
Create files:
- course_improvement_report/config/config.yaml
- course_improvement_report/.env
```


Create and activate a virtual environment

```
python3 -m venv venv
. ./venv/bin/activate 
```

Install python requirements from requirements.txt

```
pip install -r course_improvement_report/requirements.txt
```


# NO
Install python requirements from setup.py

```
cd course_improvement_report
pip install -e .
```



cd projects
. ./venv/bin/activate 
python course_improvement_report/run.py









## NEW
  pip install --upgrade google-api-python-client google-auth-httplib2 google-auth-oauthlib