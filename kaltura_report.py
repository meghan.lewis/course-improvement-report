
# coding: utf-8


#############################
####   USE THIS REPORT   ####
#############################



# # In[1]:

# import os
# #import sys
# import logging
# import re
# import datetime
# import pandas as pd
# import numpy as np
# from bs4 import BeautifulSoup
# import urllib

# ## Custom Modules
# import connmanager_two as connman
# import utils

# In[]:
# #current_dir = os.path.join(os.path.realpath('__file__'))▐
# current_dir = os.getcwd()
# # In[]:
# ## Path to SQL scripts
# SQL_LOC = 'sql/%s.sql'
# SQL_FILE_PATH = os.path.join(current_dir, SQL_LOC)

# In[]:
# #QUERY_NAME = 'KR_kaltura_content_query'

# ## Path to write report
# REPORT_LOC = 'reports/'
# REPORT_FILE_PATH = os.path.join(current_dir, REPORT_LOC)

# REPORT_NAME = os.path.join(REPORT_FILE_PATH, 'Kaltura Players - {sub_name} - {date}.xlsx')

# ## Path to log file
# LOG_FILE_NAME = 'logs/log_kaltura_report.txt'  # LOG FILE

# In[]:
# logging_config = dict(version = 1,
# 						disable_existing_loggers = False,
# 						formatters = {'f': {'format':'%(asctime)s %(name)-12s %(levelname)-8s %(message)s'}	},
# 						handlers = {'console': {'class': 'logging.StreamHandler',
# 											'formatter': 'f',
# 											'level': logging.DEBUG},
# 									'file':{'class': 'logging.handlers.RotatingFileHandler',
# 											'filename': LOG_FILE_NAME,
# 											'formatter': 'f',
# 											'level': logging.INFO}
# 									}
# 						,root = {'handlers': ['file','console'],'level': logging.INFO,})
# logging.config.dictConfig(logging_config)
# logger = logging.getLogger(__name__)
#print(os.path.abspath(__file__))

# # In[12]:
# data = pd.DataFrame()

# for pre in QUERY_PREFIX_LIST:
#     QUERY_NAME = pre + '_' + QUERY_SUFFIX
#     logger.info("Execute Query: '%s'..." % QUERY_NAME)
#     QUERY = utils.get_file_text(SQL_FILE_PATH % QUERY_NAME)
#     temp_data = pd.DataFrame()
#     try:
#         PARAMS = {'search_string': QUERY_SEARCH_STRING}
#         if DEFAULT_TERM.upper()=='Y':
#             temp_data = connman.db_query(SOURCE_DB_NAME, QUERY, PARAMS)
#             #temp_data = pd.read_sql(QUERY, con=SOURCE_DB, params=PARAMS)
#         else:
#             TERMS_PARAM = {'term_sis_list': TERM_LIST}
#             temp_data = connman.db_query(SOURCE_DB_NAME, QUERY, {**PARAMS, **TERMS_PARAM})
#             #temp_data = pd.read_sql(QUERY, con=SOURCE_DB, params={**PARAMS, **TERMS_PARAM})

#         logger.info("\t (%d total content items retrieved)" % len(temp_data))
#     except Exception:
#         logger.exception("Error querying database", exc_info=True)

# ## Read test data from file instead
# ####data = pd.read_csv("kaltura_raw_data_{term_id}.txt".format(term_id=TERM_ID), sep='\t')
#     try:
#         logger.info("Clean and Format Data...")
#         temp_data['content_text'] = temp_data['content_text'].apply(lambda x: utils.clean_html_content(x))
#         temp_data['content_name'] = temp_data['content_name'].apply(lambda x: utils.clean_ascii_string(x))
#         temp_data['content_name'] = temp_data.apply(lambda x: utils.format_excel_hyperlink(
#                                                         utils.build_canvas_url('courses',
#                                                                                x['course_id'],
#                                                                                x['content_type'],
#                                                                                x['content_id'])
#                                                     , x['content_name']), axis=1)
#         temp_data['content_type'] = np.where((temp_data['content_type']=='assignments') & (temp_data['content_id']=='syllabus'),
#                                         'syllabus',
#                                         temp_data['content_type'])
#         if 'sub_content_type' in temp_data.columns:
#             temp_data['content_type'] = np.where((temp_data['sub_content_type'].notnull()),
#                                             temp_data['sub_content_type'],
#                                             temp_data['content_type'])
#             temp_data['content_id'] = np.where((temp_data['sub_content_id'].notnull()),
#                                             temp_data['sub_content_id'],
#                                             temp_data['content_id'])

#             del temp_data['sub_content_type'], temp_data['sub_content_id']

#     except Exception:
#         logger.exception("Error cleaning content data", exc_info=True)

#     if not temp_data.empty and len(temp_data)>0:
#         data = data.append(temp_data, ignore_index=True)
# In[]:


# # In[3]:
# # Columns to include on export
# REPORT_COLUMNS = ['term_sis_id', 'term_name','course_id', 'course_sis_id',
#             'course_name','course_state', 'instructors',
#             'content_type','content_name', 'content_state',
#             'total_kaltura_links','links']  # +Player IDS
# # In[26]:
# EXCEL_FORMATS = {
#     'general': {
#             'align': 'left',
#             'font_size': 10
#             },
#     'hyperlink': {
#             'font_color': 'blue',
#             'font_size': 10,
#             'underline': 1
#             },
#     'blue_integers': {
#             'align': 'center',
#             'font_size': 10,
#             'num_format': '[Blue]#,##0;(#,##0);_(* "-"_);_(@_)'
#             },
#     'red_integers': {
#             'align': 'center',
#             'font_size': 10,
#             'num_format': '[Red]#,##0;(#,##0);_(* "-"_);_(@_)'
#             },
#     'total_integers': {
#             'align': 'center',
#             'bg_color': '#FFFDF1',
#             'bold': True,
#             'font_size': 11,
#             'num_format': '#,##0'
#             }
#     }

# In[27]:




# In[5]:
#numeric_types = ['int16', 'int32', 'int64', 'float16', 'float32', 'float64']



# In[]:
import datetime
import re
import pandas as pd
import numpy as np
from pathlib import Path
from bs4 import UnicodeDammit

from urllib.parse import urlparse
import os
from bs4 import BeautifulSoup
# from pandas import ExcelWriter
# from xlsxwriter.utility import xl_range

## Custom Modules
# import connmanager_two as connman
from utilities import utils
from utilities.custom_data import course_processor
from utilities.custom_data import canvas_helpers
from utilities import excel_report_writer as exl_writer

from utilities.database_access import OLD_database_access as database_access
from utilities.create_logger import initialize_logger


# In[]:
pd.set_option('display.max_colwidth', None)
pd.set_option('display.max_rows', 999)

# In[]:

current_dir = os.getcwd()
print(current_dir)
# In[]:
###
parent_dir = os.path.split(current_dir)[0]
# config_dir = os.path.join(current_dir, 'config')
# data_dir = os.path.join(current_dir, 'data')
sql_dir = os.path.join(current_dir, 'sql')
reports_dir = os.path.join(current_dir, 'reports')


# In[]:
logger = initialize_logger(location=['STDOUT', current_dir+'/'], script_name=Path(__file__).stem)

# In[]:
# from dotenv import load_dotenv   #pip install python-dotenv
# load_dotenv()
#####  obZYbHC10Z

# In[]:
# create DatabaseAccess object from custom module
dba = database_access.DatabaseAccess()
ssh_server, db_conn = dba.create_db_connection(username="mlewis", password="obZYbHC10Z")

# In[]:
def get_sql_query_from_file(dir, name):
    sql_file_path = os.path.join(dir, f"{name}.sql")
    if not utils.check_file_exists(sql_file_path):
        logger.info(f"SQL file does not exist: {sql_file_path}")
        return None
    return utils.get_file_text(sql_file_path)

def get_db_data(conn, query, parameters={}):
    df = pd.DataFrame()
    #logger.info(f"Executing Database Query")
    try:
        df = pd.read_sql_query(query, db_conn, params=parameters)
        logger.info(f" {len(df)} data rows retrieved")
    except Exception:
        logger.exception("Error querying database", exc_info=True)
    return df
# In[]:
# In[7]:
def parse_kaltura_links(html_text_content):
    """
        Parses links to kaltura videos and IDs of kaltura player objects
        from the HTML of Canvas content items

        Returns:
            - Total # of kaltura links found in the content text
            - List of kaltura links
            - List of player IDs
    """
    REGEX_LIST_kaltura_players = [
        r'/uiconf[\_]?id/([0-9]+)[^0-9]?',
        r'/playerskin/([0-9]+)',
        r'kaltura.com/(tiny)',
        r'=(kalturaviewer)',
        r'(mediaspace)\.kaltura.com'
    ]
    #REGEX_js_kaltura_settings = '<script>.*kaltura.*</script>'

    links,players=[],[]
    try:
        # Parse HTML content
        parsed_html = BeautifulSoup(html_text_content, "html.parser")

        # Search all hyperlinks and iframe links for 'kaltura'
        for a in parsed_html.findAll('a', href=True):
            if re.search(r'kaltura', a['href'], re.IGNORECASE):
                links.append(urlparse.unquote(a['href']))
        for i in parsed_html.findAll('iframe', src=True):
            if re.search(r'kaltura', i['src'], re.IGNORECASE):
                links.append(urlparse.unquote(i['src']))

        # For every 'kaltura' link found, attempt to parse a player ID
        for link in links:
            found = False
           # link = link.lower()
            for REGEX in REGEX_LIST_kaltura_players:
                if re.search(REGEX, link, re.IGNORECASE):
                    players.append(re.search(REGEX, link, re.IGNORECASE).group(1))
                    found = True
                    break
            if not found:
                print(f"NONE: {link}")
    except Exception:
        logger.exception("Error parsing links from content", exc_info=True)

    return pd.Series([len(links), links, players])
# In[7]:

# In[7]:

COURSES_CTE_NAME = "CTE_courses"

QUERIES = {
    'courses': 'courses',
    'instructors': 'KR_instructors',
    'content': ['KR_content','KR_sub_content']
}

PARAMS = {
    'term_sis_list': ['202140']
}

QUERY_SPECIFIC_PARAMS = {
    'content': {'search_string': 'kaltura'}
}
# In[]:

# In[]:
COURSES_CTE = get_sql_query_from_file(sql_dir, COURSES_CTE_NAME)

# In[]:
QUERY_NAME = QUERIES.get('courses')
logger.info(f"Get data for: {QUERY_NAME}")
query_text = COURSES_CTE + get_sql_query_from_file(sql_dir, QUERY_NAME)
query_params = PARAMS
course_df = get_db_data(db_conn, query_text, parameters=query_params)

# In[25]:
## PROCESS COURSE DATA
course_df['course_id'] = course_df['course_id'].apply(lambda x: None if pd.isnull(x) else str(int(x)))
course_df['course_name'] = course_df['course_name'].apply(lambda x: utils.clean_ascii_string(x))
course_df['delivery_method'] = course_df['course_name'].apply(lambda x: course_processor.CourseProcessor(course_name=x).get_delivery_method())
course_df.head()

# In[]:
## INSTRUCTOR DATA
QUERY_NAME = QUERIES.get('instructors')
logger.info(f"Get data for: {QUERY_NAME}")
query_text = COURSES_CTE + get_sql_query_from_file(sql_dir, QUERY_NAME)
query_params = PARAMS
instructor_df = get_db_data(db_conn, query_text, parameters=query_params)
instructor_df.info()

# In[11]:
## PROCESS INSTRUCTOR DATA
instructor_df['course_id'] = instructor_df['course_id'].apply(lambda x: None if pd.isnull(x) else str(int(x)))
instructor_df.head()

# In[]:
# %%
## GET CONTENT DATA

content_df_list = [] 
for QUERY_NAME in QUERIES.get('content'): 
    logger.info(f"Get data for: {QUERY_NAME}")
    query_text = COURSES_CTE + get_sql_query_from_file(sql_dir, QUERY_NAME)
    query_params = PARAMS
    if QUERY_SPECIFIC_PARAMS.get('content'):
        query_params = {**query_params, **QUERY_SPECIFIC_PARAMS.get('content')}
    df = get_db_data(db_conn, query_text, parameters=query_params)
    content_df_list.append(df)

content_df = pd.concat(content_df_list).reset_index(drop=True)
content_df.info()

# %%
## PROCESS CONTENT DATA
content_df['course_id'] = content_df['course_id'].apply(lambda x: None if pd.isnull(x) else str(int(x)))
content_df['content_text'] = content_df['content_text'].apply(lambda x: canvas_helpers.clean_canvas_html_content(x))
content_df['content_name'] = content_df['content_name'].apply(lambda x: utils.clean_ascii_string(x))

content_df.head()


# In[]:
logger.info("Parse Kaltura Players from Content...")
## Parse lists of kaltura links and player IDs from HTML content
content_df[['total_kaltura_links','links','players']] = content_df['content_text'].apply(lambda x: parse_kaltura_links(x))

## Many content items have multiple players -- unstack data to separate rows
content_players = utils.split_dataframe_list(content_df[['course_id','content_id','players']],'players','array')
logger.info("\t (%d player references found)" % len(content_players))

## Pivot 'content_players' DataFrame, to set player ID's as columns headers,
## rather than row values AND count number of each player ID per content item
pivot_players = pd.pivot_table(content_players,
                               index=['course_id','content_id'],
                               columns=['players'],
                               aggfunc=len,
                               fill_value=0)

# Create list with each player ID (later used for column headers & formatting) 
PLAYER_LIST = []
for col in pivot_players.columns:
    PLAYER_LIST.append(col)

pivot_players.columns.name = None
pivot_players.reset_index(inplace=True)


# In[]:

# %%

courses = pd.merge(course_df, 
                    instructor_df, 
                    on='course_id', 
                    how='left')
# %%
for col in instructor_df.select_dtypes(include=['int64', 'float64']).columns:
    print(col)
    courses[col] = courses[col].fillna(0)



# In[]:

logger.info("Merge Content & Players Data...")
# Merge kaltura players data with original content data
content = pd.merge(content_df, 
                    pivot_players, 
                    how='left', 
                    on=['course_id','content_id'])

# Cleanup/format data for report
for col in PLAYER_LIST:
    content[col].fillna(0, inplace=True)
    content[col] = content[col].astype(int)



# In[]:

logger.info("Merge Courses & Content Data...")
all_data = pd.merge(courses, 
                    content, 
                    on=['course_id'])

all_data.reset_index(inplace=True, drop=True)
all_data.sort_values(by=['term_sis_id','course_sis_id','course_id','content_type','content_name'],
                    na_position='first', inplace=True)



## ------------------------------- HYPERLINKS ETC ---------------------
        # temp_data['content_name'] = temp_data.apply(lambda x: utils.format_excel_hyperlink(
        #                                                 utils.build_canvas_url('courses',
        #                                                                        x['course_id'],
        #                                                                        x['content_type'],
        #                                                                        x['content_id'])
        #                                             , x['content_name']), axis=1)
        # temp_data['content_type'] = np.where((temp_data['content_type']=='assignments') & (temp_data['content_id']=='syllabus'),
        #                                 'syllabus',
        #                                 temp_data['content_type'])
        # if 'sub_content_type' in temp_data.columns:
        #     temp_data['content_type'] = np.where((temp_data['sub_content_type'].notnull()),
        #                                     temp_data['sub_content_type'],
        #                                     temp_data['content_type'])
        #     temp_data['content_id'] = np.where((temp_data['sub_content_id'].notnull()),
        #                                     temp_data['sub_content_id'],
        #                                     temp_data['content_id'])

        #     del temp_data['sub_content_type'], temp_data['sub_content_id']









# In[26]:
logger.info("Prepare Report & Write to Excel...")

## Create dictionary object with columns and Excel format parameters
custom_column_formats = {}

for col in REPORT_COLUMNS:
    if col in ['content_name']:
        custom_column_formats[col] = EXCEL_FORMATS.get('hyperlink')
    elif col in ['total_kaltura_links']:
        custom_column_formats[col] = EXCEL_FORMATS.get('total_integers')
    else:
        custom_column_formats[col] = EXCEL_FORMATS.get('general')

for player in PLAYER_LIST:
    if re.match('^\d+$', player):
        custom_column_formats[player] = EXCEL_FORMATS.get('blue_integers')
    else:
        custom_column_formats[player] = EXCEL_FORMATS.get('red_integers')


DATE_TODAY = datetime.date.today()
DATE_STR = DATE_TODAY.strftime("%Y-%m-%d")



NAME_TERMS = '_'.join(TERM_LIST) if len(TERM_LIST) <= 2 else 'Multiple'

# Format report name
REPORT_NAME = REPORT_NAME.format(sub_name=NAME_TERMS, date=DATE_STR)
# Append list of player columns to report columns
REPORT_COLUMNS = REPORT_COLUMNS + PLAYER_LIST



utils.save_custom_xls(REPORT_NAME,
                     list_dfs=[data[REPORT_COLUMNS]],
                     list_nms=['Kaltura Players'],
                     column_formats=custom_column_formats)


logger.info("%s -- Complete" % utils.current_time())

# In[ ]:


























